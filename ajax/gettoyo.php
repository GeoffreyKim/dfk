<?php
    include_once('../config/connection.php');
    $page = $_GET['page'];
    logs_by_current_page($page);
    function logs_by_current_page($page){
        global $dbc;
        $item_per_page = 20;
            if($page != ""){
                $page_number = $page;
                if(!is_numeric($page_number)){
                    $page_number = 1;
                }
            }else{
                $page_number = 1;
            }
            $results = "SELECT id FROM products Where id !='' ";
            $results = mysqli_query($dbc, $results);
            $get_total_rows = mysqli_num_rows($results);
            $total_pages = ceil($get_total_rows/$item_per_page);
    
            $page_position = (($page_number-1) * $item_per_page);
    
            $q = "SELECT * FROM products ORDER BY brand DESC LIMIT $page_position, $item_per_page";
            $r = mysqli_query($dbc, $q);

            while( $obj = mysqli_fetch_assoc( $r )) {
                $json[] = $obj;
            }
            $pagehtml = paginate_function($item_per_page, $page_number, $get_total_rows, $total_pages);

            $final = array('data'=>$json, 'pages'=>$pagehtml, 'ctpage'=>$page);

            header('Content-type: application/json');
            echo json_encode($final, JSON_PRETTY_PRINT);
    }





    function paginate_function($item_per_page, $current_page, $total_records, $total_pages)
    {
        $pagination = '';
        if($total_pages > 0 && $total_pages != 1 && $current_page <= $total_pages){ //verify total pages and current page number
            $pagination .= '<ul class="pagination pagination-info">';

            $right_links    = $current_page + 3;
            $previous       = $current_page - 1; //previous link
            $next           = $current_page + 1; //next link
            $first_link     = true; //boolean var to decide our first link

            if($current_page > 1){
                $previous_link = ($previous==0)? 1: $previous;
                //first link
                $pagination .= '<li class="page-item next pagedata" id="1"><a class="page-link">First</a></li>';
                //previous link
                $pagination .= '<li class="page-item pagedata" id='.$previous_link.'><a class="page-link">‹</a></li>';

                    for($i = ($current_page-2); $i < $current_page; $i++){ //Create left-hand side links
                        if($i > 0){
                            $pagination .= '<li class="page-item pagedata" id="'.$i.'"><a class="page-link">'.$i.'</a></li>';
                        }
                    }
                $first_link = false; //set first link to false
            }

            if($first_link){ //if current active page is first link
                $pagination .= '<li id='.$first_link.' class="page-item disabled" aria-controls="datatable-buttons" tabindex="0"><a class="page-link">«</a></li>
                ';
            }elseif($current_page == $total_pages){ //if it's the last active link
                $pagination .= '<li id="tdata" class="page-item next disabled" aria-controls="datatable-buttons" tabindex="0"><a class="page-link">End</a></li>';
            }else{ //regular current link
                $pagination .= '<li id="tdata" class="page-item active" aria-controls="datatable-buttons" tabindex="0"><a class="page-link">'.$current_page.'</a></li>
                ';
            }

            for($i = $current_page+1; $i < $right_links ; $i++){ //create right-hand side links
                if($i<=$total_pages){
                    $pagination .= '<li id="'.$i.'" class="page-item pagedata" aria-controls="datatable-buttons"><a class="page-link" >'.$i.'</a></li>';
                }
            }

            if($current_page < $total_pages){
                    $next_link = ($next > $total_pages)? $total_pages: $next;
                    //next link
                    $pagination .= '<li id='.$next_link.' class="page-item pagedata" aria-controls="datatable-buttons"><a class="page-link" >›</a></li>';
                    //last link
                    $pagination .= '<li id='.$total_pages.' class="page-item next pagedata"><a class="page-link" >Last</a></li>';
            }

            $pagination .= '</ul>';
        }
        return $pagination; //return pagination links
    }
?>