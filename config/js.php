<!-- JS Global Compulsory -->
<script src="/assets/vendor/jquery/dist/jquery.min.js"></script>
<script src="/assets/vendor/jquery-migrate/dist/jquery-migrate.min.js"></script>
<script src="/assets/vendor/popper.js/dist/umd/popper.min.js"></script>
<script src="/assets/vendor/bootstrap/bootstrap.min.js"></script>

<!-- JS Implementing Plugins -->
<script src="/assets/vendor/hs-megamenu/src/hs.megamenu.js"></script>
<script src="/assets/vendor/svg-injector/dist/svg-injector.min.js"></script>
<script src="/assets/vendor/fancybox/jquery.fancybox.min.js"></script>
<script src="/assets/vendor/slick-carousel/slick/slick.js"></script>
<script src="/assets/vendor/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="/assets/vendor/cubeportfolio/js/jquery.cubeportfolio.min.js"></script>

<!-- JS Front -->
<script src="/assets/js/hs.core.js"></script>
<script src="/assets/js/components/hs.header.js"></script>
<script src="/assets/js/components/hs.unfold.js"></script>
<script src="/assets/js/components/hs.fancybox.js"></script>
<script src="/assets/js/components/hs.slick-carousel.js"></script>
<script src="/assets/js/components/hs.validation.js"></script>
<script src="/assets/js/components/hs.focus-state.js"></script>
<script src="/assets/js/components/hs.cubeportfolio.js"></script>
<script src="/assets/js/components/hs.svg-injector.js"></script>
<script src="/assets/js/components/hs.go-to.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<!-- JS Plugins Init. -->
<script>
$(window).on('load', function () {
    // initialization of HSMegaMenu component
    $('.js-mega-menu').HSMegaMenu({
    event: 'hover',
    pageContainer: $('.container'),
    breakpoint: 767.98,
    hideTimeOut: 0
    });

    // initialization of svg injector module
    $.HSCore.components.HSSVGIngector.init('.js-svg-injector');
});

$(document).on('ready', function () {
    // initialization of header
    $.HSCore.components.HSHeader.init($('#header'));

    // initialization of unfold component
    $.HSCore.components.HSUnfold.init($('[data-unfold-target]'));

    // initialization of fancybox
    $.HSCore.components.HSFancyBox.init('.js-fancybox');

    // initialization of slick carousel
    $.HSCore.components.HSSlickCarousel.init('.js-slick-carousel');

    // initialization of form validation
    $.HSCore.components.HSValidation.init('.js-validate');

    // initialization of forms
    $.HSCore.components.HSFocusState.init();

    // initialization of cubeportfolio
    $.HSCore.components.HSCubeportfolio.init('.cbp');

    // initialization of go to
    $.HSCore.components.HSGoTo.init('.js-go-to');
});


$(document).ready(function() {
    var submit = $('.submit-btn');
    // var alert = $('.alert-msg');

    function redirecting() {
        window.location.href = 'http://applesandsense.com/';
    };
    
    $('#sendingmessage').on('submit', (function (e) {
        console.log('Sending messahe')
        // $('.showing').hide();
        submit.html('<i class="fa fa-spinner fa-spin"></i> Sending....');
        submit.prop('disabled', true);
        e.preventDefault();
        
        $('#nameError').html('');
        $('#emailError').html('');
        $('#messageError').html('');

        $.ajax({
            url: "/ajax/sendrequest.php",
            type: "POST", // Request type either a post, get, put, putch, delete
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (res) {
                if (res.Sent) {
                    $('#name').val('');
                    $('#email').val('');
                    $('#message').val('');
                    $('#phone').val('');
                    // $('.alert-msg').html('Your email has been sent, Thanks.');
                    submit.html('Send');
                    swal({
                        title: "Great!!!",
                        text: res.Sent,
                        icon: "success",
                        button: "Ok.",
                    }).then((value) => {
                        // redirecting();
                    });
                }
                else if (res.server) {
                    swal({
                        title: "Error!",
                        text: res.server,
                        icon: "error",
                    })
                }else{
                    $('#nameError').html(`${res.Name}`);
                    $('#messageError').html(`${res.Message}`);
                    $('#emailError').html(`${res.Email}`);
                }
                submit.prop('disabled', false);
                submit.html('Send');
            }
        });
    }));
});



</script>