<!-- <link href="/assets/plugins/animate.less/animate.min.css" rel="stylesheet" type="text/css" /> -->
<meta charset="utf-8">
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<link rel="icon" type="image/x-icon" href="<?php echo SAR; ?>/assets/img/logos/dreamlogo.png"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>The Dream Factory</title>
<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
<meta property="og:locale" content="en_GB" />
<meta property="og:type" content="article" />

<meta property="og:title" content="Bobmil Industries | Our mission is to make our products and services the preferred choice for all | Bobmil" />

<meta property="og:description" content="Our mission is to make our products and services the preferred choice for all" />
<meta property="og:url" content="http://www.bobmil.com/" />
<meta property="og:site_name" content="zalego Institute" />
<meta property="og:image" content="https://www.bobmil.com/images/icons/favicon.png" />
<meta property="og:image:secure_url" content="https://www.bobmil.com/images/icons/favicon.png" />
<meta property="og:image:width" content="222" />
<meta property="og:image:height" content="192" />


<!-- Favicons -->
<!-- Extra details for Live View on GitHub Pages -->
<!-- Canonical SEO -->
<link rel="canonical" href="https://www.bobmil.com/home" />
<!--  Social tags      -->
<meta name="keywords" content="Bobmil Industries, Bobmil, Industries, maharaja industries, Welcome to Bobmil, Mattress Manufacturing Industries, Mattress">
<meta name="description" content="Our mission is to make our products and services the preferred choice for all">
<!-- Schema.org markup for Google+ -->
<meta itemprop="name" content="Bobmil Industries | Our mission is to make our products and services the preferred choice for all | Bobmil">
<meta itemprop="description" content="Our mission is to make our products and services the preferred choice for all">
<meta itemprop="image" content="https://www.bobmil.com/images/logo.png">
<!-- Twitter Card data -->
<meta name="twitter:card" content="bobmilindustries">
<meta name="twitter:site" content="@bobmilindustries">
<meta name="twitter:title" content="Software development academy">
<meta name="twitter:description" content="Our mission is to make our products and services the preferred choice for all">
<meta name="twitter:creator" content="@bobmilindustries">
<meta name="twitter:image" content="https://pbs.twimg.com/profile_images/854675835202273281/D6xISw9o_400x400.jpg">
<meta name="twitter:data1" content="Bobmil Industries">
<!-- Open Graph data -->
<meta property="og:description" content="Our mission is to make our products and services the preferred choice for all" />
<meta property="og:site_name" content="Bobmil Industries" />
<!--     Fonts and icons     -->
<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">

<!-- CSS Implementing Plugins -->
<link rel="stylesheet" href="/assets/vendor/font-awesome/css/fontawesome-all.min.css">
<link rel="stylesheet" href="/assets/vendor/animate.css/animate.min.css">
<link rel="stylesheet" href="/assets/vendor/hs-megamenu/src/hs.megamenu.css">
<link rel="stylesheet" href="/assets/vendor/fancybox/jquery.fancybox.css">
<link rel="stylesheet" href="/assets/vendor/slick-carousel/slick/slick.css">
<link rel="stylesheet" href="/assets/vendor/cubeportfolio/css/cubeportfolio.min.css">

<!-- CSS Front Template -->
<link rel="stylesheet" href="/assets/css/theme.css">

<!-- iframe removal -->
<script type="text/javascript">
    if (document.readyState === 'complete') {
        if (window.location != window.parent.location) {
            const elements = document.getElementsByClassName("iframe-extern");
            while (elemnts.lenght > 0) elements[0].remove();
            // $(".iframe-extern").remove();
        }
    };
</script>
<!-- Slider section -->
<link rel="stylesheet" type="text/css" href="/assets/engine1/style.css" />
<!-- <script type="text/javascript" src="/assets/engine1/jquery.js"></script> -->
<!-- End slider section -->

<!-- <link href="assets/plugins/fullcalendar/dist/fullcalendar.css" rel="stylesheet" /> -->
<!-- <link href="assets/plugins/select2/select2.css" rel="stylesheet" type="text/css" /> -->
<link rel="stylesheet" type="text/css" href="/assets/css/default.css" />
<link rel="stylesheet" type="text/css" href="/assets/css/component.css" />

<script src="/assets/js/modernizr.custom.js"></script>
<style>
    .whatsapplink{
        background-color: rgb(25, 194, 104);
        border-radius: 50%;
        width: 50px;
        height: 50px;
        padding: .6rem;
        position: fixed;
        bottom: 10px;
        right: 10px;
        z-index: 9999;
    }
    .owl-item{
        /* margin: 0 22px; */
    }
    .logo-item{
        width: 200px;
    }
    .text-toyo{
        color: #136AB6 !important;
    }
    .nav-pills-toyo{
        background-color: #136AB6 !important;
    }
    .nav-item:hover{
        color: #FDA13D;
    }
    .faster{
        -webkit-animation-duration: .5s !important;
        animation-duration: .5s !important;
    }
    .nav-pills li .nav-link{
        text-transform: none !important;
    }
    .info-title>.locationlinks:hover, .info-title>.locationlinks:focus{
        color: #FDA13D !important;
    }
</style>
<?php
    if ($page == 'toyotyres' || $page == 'continental' || $page == 'hankook' || $page == 'zeta' || $page == 'general' || $page == 'sailun' || $page == 'search') {
    ?>
    <style>
        .card-pricing ul li{
            font-weight: 400;
            font-size: 16px;
        }
        .footer-big .social-feed p{
            max-width: none !important;
        }
        .card-profile .card-avatar img, .card-testimonial .card-avatar img{
            height: auto !important;
        }
        /* .card-description, .description, .footer-big p{
            color: #3c4858;
        } */
    </style>
    <?php
    }
?>

<?php
    if ($page == 'storelocator') {
    ?>
    <style>
        @media (min-width: 1200px){
            .container {
                max-width: 1240px;
            }
        }
    </style>
    <?php
    }
?>