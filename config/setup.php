<?php
// Setup File:

//error_reporting();

# Database Connection:
include('config/connection.php');
include_once('config/global.php');
# Constants:
DEFINE('D_TEMPLATE', 'template');
DEFINE('SAR', '//'. $_SERVER['HTTP_HOST']);

# Functions:
include('functions/data.php');
// include('functions/template.php');
include('functions/sandbox.php');
# Site Setup:
$debug = data_setting_value($dbc, 'debug-status');
$site_title = data_setting_value($dbc, 'site-title');
$help_notifications = data_setting_value($dbc, 'help-status');

# User Setup:
if(isset($_SESSION['username'])){

	$user = data_user($_SESSION['username']);
	$user_type = $user['type_id'];

}

# Page Setup:

$path = data_path();

if(!isset($path['call_parts'][0]) || $path['call_parts'][0] == ''){
	header("Location: /home"); // Set default location = day.
} else {
	$page = $path['call_parts'][0]; // Set $pageid to equal the value given in the URL
}

if(isset($path['call_parts'][1])) {
	$mode = $path['call_parts'][1]; // Set $mode to equal the value given in the URL
}

if(isset($path['call_parts'][2])) {
	$id = $path['call_parts'][2]; // Set $tab to equal the value given in the URL
}

if(isset($_GET['error'])) {
	$errorCode = $_GET['error']; // Set $error to equal the value given in the URL
}

# Page Queries:

include('config/queries.php');

?>
