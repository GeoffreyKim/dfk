<?php
// $account = $user['account_id'];
$user_id = $user['id'];

	switch ($page) {

		case 'users':

			if(isset($_POST['submitted']) == 1) {

				$first = mysqli_real_escape_string($dbc, $_POST['firstname']);
				$last = mysqli_real_escape_string($dbc, $_POST['lastname']);

				if($_POST['password'] != '') {

					if($_POST['password'] == $_POST['passwordv']) {

						$password = " password = SHA1('$_POST[password]'),";
						$verify = true;

					} else {

						$verify = false;

					}

				} else {

					$verify = false;

				}

				if(isset($_POST['id']) != '') {

					$action = 'updated';
					$q = "UPDATE users SET firstname = '$first', lastname = '$last', email = '$_POST[email]', type_id = '$_POST[type_id]', $password status = $_POST[status] WHERE id = $_GET[id]";
					$r = mysqli_query($dbc, $q);

				} else {

					$action = 'added';

					$q = "INSERT INTO users (firstname, lastname, email, password, type_id, status) VALUES ('$first', '$last', '$_POST[email]', SHA1('$_POST[password]'), '$_POST[type_id]', '$_POST[status]')";

					if($verify == true) {
						$r = mysqli_query($dbc, $q);
					}

				}

				if($r){

					$message = '<p class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>User was '.$action.'!</p>';

				} else {

					$message = '<p class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>User could not be '.$action.' because: '.mysqli_error($dbc);
					if($verify == false) {
						$message .= '<p class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>Password fields empty and/or do not match.</p>';
					}
					$message .= '<p class="alert alert-warning alert-dismissable"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>Query: '.$q.'</p>';

				}

			}

			if(isset($id)) { $opened = data_user( $id); }

		break;

		case 'login':
			if(isset($_POST['submitted']) == 2) {
				if (empty($_POST['email']) === true && empty($_POST['password']) === true) {
					$reply = '<span style="margin: 0 auto !important;" type="button" class="btn btn-danger waves-effect waves-light">Please fill the blanks.</span>';
				}
				else {
					$q = "SELECT * FROM users WHERE email = '$_POST[email]' AND password = SHA1('$_POST[password]')";
					$r = mysqli_query($dbc, $q);
					if(mysqli_num_rows($r) == 1) {
						$datetime = date("Y-m-d h:i:s");
						$rows = get_user_data($_POST['email']);
						while( $row = mysqli_fetch_assoc($rows) ):
							if ($row['password_recover'] == 1) {
								header('Location: /new_password.php?date='.$row['id'].'&time='.$row['type_id']);
							}else{
								if ($row['status'] == 2) {
									$reply = '<span style="margin: 0 auto !important;" type="button" class="btn btn-danger waves-effect waves-light">Account deactivated</span>';
								}else{
									$_SESSION['username'] = $_POST['email'];
									$_SESSION['center'] = $row['center'];
									$_SESSION['type_id'] = $row['type_id'];
									$_SESSION['id'] = $row['id'];
									$code = 4;
									data_log_system_event($row['id'], 'Logged In', $code);
								}
							}
						endwhile;
	 				}
	 				else {
	 					$reply = '<span style="margin: 0 auto !important;" type="button" class="btn btn-danger waves-effect waves-light">Incorrect credentials</span>';
	 				}
				}
			}
		break;

		case 'lock-screen':
			if(isset($_POST['submitted']) == 87) {
				if (empty($_POST['account']) === true && empty($_POST['password']) === true && empty($_POST['user_id']) === true) {
					$reply = '<span style="margin: 0 auto !important;" type="button" class="btn btn-danger waves-effect waves-light">Please fill the blanks.</span>';
				}
				else {
					$q = "SELECT * FROM users WHERE email = '$_POST[account]' AND password = SHA1('$_POST[password]')";
					$r = mysqli_query($dbc, $q);
					if(mysqli_num_rows($r) == 1) {
						state_updater($_POST['user_id']);
	 				}
	 				else {
	 					$reply = '<span style="margin: 0 auto !important;" type="button" class="btn btn-danger waves-effect waves-light">Incorrect credentials</span>';
	 				}
				}
			}
		break;

		case 'logout':
			$code = 4;
			data_log_system_event($user_id, 'Logged Out', $code);
		break;

		case 'day':
			if(isset($id)) { $opened = data_vhl( $id); }
		break;

		case 'issuance':
			if(isset($id)) { $opened = data_issuance( $id); }
		break;

		case 'drivers':
			if(isset($id)) { $opened = data_driver( $id); }
		break;

		case 'accident':
			if(isset($id)) { $opened = data_accident( $id); }
		break;

		case 'violation':
			if(isset($id)) { $opened = data_violation( $id); }
		break;

		case 'jobcard':
			if(isset($id)) { $opened = data_jobcard($id); }
		break;

		case 'workticket':
			if(isset($id)) { $opened = data_workticket( $id); }
		break;

		case 'costing':
			if(isset($id)) { $opened = data_costing( $id); }
		break;

		case 'distance':
			if(isset($id)) { $opened = data_distance( $id); }
		break;

		case 'location':
			if(isset($id)) { $opened = data_Location( $id); }
		break;

		case 'base':
			if(isset($id)) { $opened = data_base( $id); }
		break;

		case 'Contracted':
			if(isset($id)) { $opened = data_contracted( $id); }
		break;

		case 'Claim':
			if(isset($id)) { $opened = data_claim( $id); }
		break;

		case 'rental':
			if(isset($id)) { $opened = data_rental( $id); }
		break;

		case 'Return':
			if(isset($id)) { $opened = data_returns( $id); }
		break;

		case 'stockreg':
			if(isset($id)) { $opened = data_stock( $id); }
		break;

		case 'stockreci':
			if(isset($id)) { $opened = data_Order($id); }
		break;

		case 'mechanics':
			if(isset($id)) { $opened = data_Mechanics($id); }
		break;

		case 'DoneTasks':
			if(isset($id)) { $opened = data_DoneTasks($id); }
		break;

		case 'tank_fill':
			if(isset($id)) { $opened = data_Tanks($id); }
		break;

		case 'userSelect':
			if(isset($id)) { $opened = userAnalysis($id); }
		break;
		
		default:

		break;
	}




?>
