<?php

	include('template/header.php');

	if(file_exists('views/'.$page.'.php')){
		include('views/'.$page.'.php');
	}else{
		include('views/404.php');
	}
	include('template/footer.php');
?>
