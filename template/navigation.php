<!-- ========== HEADER ========== -->
<header id="header" class="u-header u-header-center-aligned-nav u-header--bg-transparent u-header--white-nav-links-md u-header--sub-menu-dark-bg-md u-header--abs-top"
          data-header-fix-moment="500"
          data-header-fix-effect="slide">
    <div class="u-header__section">
      <div id="logoAndNav" class="container">
        <!-- Nav -->
        <nav class="js-mega-menu navbar navbar-expand-md u-header__navbar u-header__navbar--no-space">
          <div class="u-header-center-aligned-nav__col">
            <!-- Logo -->
            <a class="navbar-brand u-header__navbar-brand u-header__navbar-brand-center u-header__navbar-brand-text-white" href="/" aria-label="Front">
                <!-- <img src="/assets/img/logos/dreamlogo.png" alt="" height="80px" width="80px"> -->
              <!-- <span class="u-header__navbar-brand-text">TDF</span> -->
            </a>
            <!-- End Logo -->

            <!-- Responsive Toggle Button -->
            <button type="button" class="navbar-toggler btn u-hamburger u-hamburger--white"
                    aria-label="Toggle navigation"
                    aria-expanded="false"
                    aria-controls="navBar"
                    data-toggle="collapse"
                    data-target="#navBar">
              <span id="hamburgerTrigger" class="u-hamburger__box">
                <span class="u-hamburger__inner"></span>
              </span>
            </button>
            <!-- End Responsive Toggle Button -->
          </div>

          <!-- Navigation -->
          <div id="navBar" class="collapse navbar-collapse u-header__navbar-collapse">
            <ul class="navbar-nav u-header__navbar-nav">
              
            </ul>
          </div>
          <!-- End Navigation -->

          <!-- Last Item -->
          <div class="u-header-center-aligned-nav__col u-header-center-aligned-nav__col-last-item">
            <a class="btn btn-sm btn-white transition-3d-hover p-2" href="/" style="margin-top: -20px;">
            <img src="/assets/img/logos/logo.gif" alt="" height="190px" width="190px">
            <!-- The Dream Factory -->
            </a>
          </div>
          <!-- End Last Item -->
        </nav>
        <!-- End Nav -->
      </div>
    </div>
  </header>
  <!-- ========== END HEADER ========== -->