<!-- ========== FOOTER ========== -->
<footer class="container space-1">
    <div class="text-center">
      <a href="/qualatex" class="btn btn-xs btn-transparent btn-wide btn-pill text-left mb-2 mb-sm-0 mr-1">
        <span class="media align-items-center">
          <!-- <span class="fas fa-fire font-size-2 mr-3"></span> -->
          <img class="img-fluid rounded mx-auto" width="50px" src="/assets/img/qualatex-logo.jpg" alt="Qualatex Logo">
          <span class="media-body">
            <!-- <strong class="font-size-1">Qualatex</strong> -->
          </span>
        </span>
      </a>

      <a href="/lego" class="btn btn-xs btn-transparent btn-wide btn-pill text-left mb-2 mb-sm-0 ml-1">
        <span class="media align-items-center">
          <!-- <span class="fab fa-whmcs font-size-2 mr-3"></span> -->
          <img class="img-fluid rounded mx-auto" width="50px" src="/assets/img/500px-LEGO_logo.svg.png" alt="Lego Logo">
          <span class="media-body">
            <!-- <strong class="font-size-1">Lego</strong> -->
          </span>
        </span>
      </a>

      <a href="/hasbro" class="btn btn-xs btn-transparent btn-wide btn-pill text-left mb-2 mb-sm-0 ml-1">
        <span class="media align-items-center">
          <!-- <span class="fab fa-whmcs font-size-2 mr-3"></span> -->
          <img class="img-fluid rounded mx-auto" width="50px" src="/assets/img/HASBRO Logo.png" alt="Hasbro Logo">
          <span class="media-body">
            <!-- <strong class="font-size-1">Hasbro</strong> -->
          </span>
        </span>
      </a>
    </div>

    <hr class="my-7">

    <div class="row align-items-md-center">
      <div class="col-md-4 mb-4 mb-md-0">
        <div class="d-flex align-items-center">
          <!-- Logo -->
          <a class="mr-3" href="#" aria-label="Front">
            <img src="/assets/img/logos/dreamlogo.png" alt="" height="70px" width="70px">
          </a>
          <!-- End Logo -->

          <p class="small mb-0">© TDF. 2019 The Dream Factory.<br>All rights reserved.</p>
        </div>
      </div>

      <div class="col-sm-6 col-md-4 mb-4 mb-sm-0 text-center">
        <!-- List -->
        <p class="small mb-0">Mayfair Business Centre,<br>4th Floor, Msapo Close,<br>Off Parklands Road.<br>P.O. Box 22902-00100, Nairobi, Kenya<br>0731777118 | 0202178797</p>
        <!-- End List -->
      </div>

      <div class="col-sm-6 col-md-4">
        <!-- Social Networks -->
        <ul class="list-inline text-sm-right mb-0">
          <li class="list-inline-item">
            <a target="_blank" class="btn btn-sm btn-icon btn-soft-secondary btn-bg-transparent rounded-circle" href="https://www.facebook.com/thedreamfactorykenya/">
              <span class="fab fa-facebook-f btn-icon__inner"></span>
            </a>
          </li>
          <li class="list-inline-item">
            <a target="_blank" class="btn btn-sm btn-icon btn-soft-secondary btn-bg-transparent rounded-circle" href="mailto:info@thedreamfactorykenya.co.ke/">
              <span class="fa fa-envelope btn-icon__inner"></span>
            </a>
          </li>

          <li class="list-inline-item">
            <a target="_blank" class="btn btn-sm btn-icon btn-soft-secondary btn-bg-transparent rounded-circle" href="https://www.instagram.com/thedreamfactorykenya/">
              <span class="fab fa-instagram btn-icon__inner"></span>
            </a>
          </li>

          
        </ul>
        <!-- End Social Networks -->
      </div>
    </div>
  </footer>


  <!-- Account Sidebar Navigation -->
  <aside id="sidebarContent" class="u-sidebar" aria-labelledby="sidebarNavToggler">
    <div class="u-sidebar__scroller">
      <div class="u-sidebar__container">
        <div class="u-sidebar--account__footer-offset">
          <!-- Toggle Button -->
          <div class="d-flex justify-content-between align-items-center pt-4 px-7">
            <h3 class="h6 mb-0">Send us a quick message</h3>

            <button type="button" class="close ml-auto"
                    aria-controls="sidebarContent"
                    aria-haspopup="true"
                    aria-expanded="false"
                    data-unfold-event="click"
                    data-unfold-hide-on-scroll="false"
                    data-unfold-target="#sidebarContent"
                    data-unfold-type="css-animation"
                    data-unfold-animation-in="fadeInRight"
                    data-unfold-animation-out="fadeOutRight"
                    data-unfold-duration="500">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <!-- End Toggle Button -->

          <!-- Content -->
          <div class="js-scrollbar u-sidebar__body">
            <!-- Holder Info -->
            <header class="d-flex align-items-center u-sidebar--account__holder mt-3">
              <div class="position-relative">
                <img class="u-sidebar--account__holder-img" src="/assets/img/350x400/susan.jpg" alt="Image Description">
                <span class="badge badge-xs badge-outline-success badge-pos rounded-circle"></span>
              </div>
              <div class="ml-3">
                <span class="font-weight-semi-bold">Susan Wahinya.</span>
                <span class="u-sidebar--account__holder-text">Sales & Marketing Manager</span>
              </div>
            </header>
            <!-- End Holder Info -->

            <div class="u-sidebar__content--account">
              <!-- List Links -->
              <form autocomplete="off" id="sendingmessage">
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="name">Your name</label>
                      <input type="text" id="name" name="name" class="form-control">
                      <span class="text-danger small" id="nameError"></span>
                    </div>
                  </div>

                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="name">Your email</label>
                      <input type="text" id="email" name="email" class="form-control">
                      <span class="text-danger small" id="emailError"></span>
                    </div>
                  </div>

                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="name">Your phone number</label>
                      <input type="number" id="phone" name="phone" class="form-control">
                    </div>
                  </div>

                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="name">Message</label>
                      <textarea style="resize:none;" name="message" id="message" cols="30" rows="5" class="form-control"></textarea>
                      <span class="text-danger small" id="messageError"></span>
                    </div>
                  </div>

                  <div class="col-md-12">
                    <button type="submit" class="btn btn-rounded btn-success btn-sm">Send</button>
                  </div>
                </div>
              </form>
              
              <!-- <ul class="list-unstyled u-sidebar--account__list">
                <li class="u-sidebar--account__list-item">
                  <a class="u-sidebar--account__list-link" href="../account/dashboard.html">
                    <span class="fas fa-home u-sidebar--account__list-icon mr-2"></span>
                    Dashboard
                  </a>
                </li>
                <li class="u-sidebar--account__list-item">
                  <a class="u-sidebar--account__list-link" href="../account/profile.html">
                    <span class="fas fa-user-circle u-sidebar--account__list-icon mr-2"></span>
                    Profile
                  </a>
                </li>
                <li class="u-sidebar--account__list-item">
                  <a class="u-sidebar--account__list-link" href="../account/my-tasks.html">
                    <span class="fas fa-tasks u-sidebar--account__list-icon mr-2"></span>
                    My tasks
                  </a>
                </li>
                <li class="u-sidebar--account__list-item">
                  <a class="u-sidebar--account__list-link" href="../account/projects.html">
                    <span class="fas fa-layer-group u-sidebar--account__list-icon mr-2"></span>
                    Projects <span class="badge badge-danger float-right mt-1">3</span>
                  </a>
                </li>
                <li class="u-sidebar--account__list-item">
                  <a class="u-sidebar--account__list-link" href="../account/members.html">
                    <span class="fas fa-users u-sidebar--account__list-icon mr-2"></span>
                    Members
                  </a>
                </li>
                <li class="u-sidebar--account__list-item">
                  <a class="u-sidebar--account__list-link" href="../account/activity.html">
                    <span class="fas fa-exchange-alt u-sidebar--account__list-icon mr-2"></span>
                    Activity
                  </a>
                </li>
                <li class="u-sidebar--account__list-item">
                  <a class="u-sidebar--account__list-link" href="../account/payment-methods.html">
                    <span class="fas fa-wallet u-sidebar--account__list-icon mr-2"></span>
                    Payment methods
                  </a>
                </li>
                <li class="u-sidebar--account__list-item">
                  <a class="u-sidebar--account__list-link" href="../account/plans.html">
                    <span class="fas fa-cubes u-sidebar--account__list-icon mr-2"></span>
                    Plans
                  </a>
                </li>
              </ul> -->
              <!-- End List Links -->

              <div class="u-sidebar--account__list-divider"></div>

              <!-- List Links -->
              
              <!-- End List Links -->
            </div>
          </div>
        </div>

        <!-- Footer -->
        <!-- <footer id="SVGwaveWithDots" class="svg-preloader u-sidebar__footer u-sidebar__footer--account">
          <div class="position-absolute right-0 bottom-inf left-0">
            <img class="js-svg-injector" src="/assets/svg/components/wave-bottom-with-dots.svg" alt="Image Description"
                   data-parent="#SVGwaveWithDots">
          </div>
        </footer> -->
        <!-- End Footer -->
      </div>
    </div>
  </aside>
  <!-- End Account Sidebar Navigation -->



  <!-- ========== END FOOTER ========== -->

<!-- Go to Top -->
<a class="js-go-to u-go-to" href="#"
    data-position='{"bottom": 15, "right": 15 }'
    data-type="fixed"
    data-offset-top="400"
    data-compensation="#header"
    data-show-effect="slideInUp"
    data-hide-effect="slideOutDown">
    <span class="fas fa-arrow-up u-go-to__inner"></span>
</a>
<!-- End Go to Top -->
<?php include('config/js.php'); ?>
