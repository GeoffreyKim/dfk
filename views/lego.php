<main id="content" role="main">
    <!-- Hero Section -->
    <div class="gradient-overlay-half-danger-video-v1 bg-img-hero" style="background-image: url(../../assets/img/LEGO.gif);">
        <!-- Video Background -->
        <!-- <div class="js-bg-video d-none d-md-block position-absolute w-100 h-80"
            data-hs-bgv-type="youtube"
            data-hs-bgv-id="0qisGSwZym4"
            data-hs-bgv-loop="true">
        </div> -->
        <!-- End Video Background -->
        <div class="position-relative z-index-2">
            <!-- Content -->
            <div class="d-md-flex">
            <div class="container d-md-flex align-items-md-center min-height-md-80vh text-center space-3 space-top-md-4 space-top-lg-3">
                <div class="row justify-content-md-center w-100">
                <div class="col-md-10">
                    <!-- Fancybox -->
                    <a class="js-fancybox u-media-player" href="javascript:;"
                    data-src="//vimeo.com/167434033"
                    data-speed="700"
                    data-animate-in="zoomIn"
                    data-animate-out="zoomOut"
                    data-caption="Front - Responsive Website Template">
                    <span class="u-media-player__icon u-media-player__icon--lg">
                    <span class="fas fa-play u-media-player__icon-inner"></span>
                    </span>
                    </a>
                    <!-- End Fancybox -->

                    <!-- Info -->
                    <div class="mt-5 mb-8">
                        <h1 class="display-4 text-white font-weight-normal">Race into a world of fun and fantasy</h1>
                        <p class="lead text-white">Enter the world of GT endurance racing with the authentic replica of the classic Bugatti Chiron</p>
                    </div>
                    <!-- End Info -->
                </div>
                </div>
            </div>
            </div>
            <!-- End Content -->
        </div>
    </div>
    <!-- End Hero Section -->


    <div class="row">
    <div class="col-md-6 col-lg-5 text-md-right mr-auto ml-auto mt-5">
          <a class="btn btn-warning btn-wide transition-3d-hover mb-1 mb-sm-0 mr-sm-1" href="/qualatex">Qualatex</a>
          <a class="btn btn-danger btn-wide transition-3d-hover mb-1 mb-sm-0 mr-sm-1" href="/lego">Lego</a>
          <a class="btn btn-primary btn-wide transition-3d-hover mb-1 mb-sm-0 mr-sm-1" href="/hasbro">Hasbro</a>
          
        </div>
    </div>




    <!-- Hero Section -->
    <div class="container space">
      <!-- Title -->
      <div class="w-md-80 w-lg-50 text-center mx-md-auto mb-9">
        <small class="text-secondary text-uppercase font-weight-medium mb-2">What we do?</small>
        <h2 class="h3 font-weight-medium">Fast and easy design</h2>
      </div>
      <!-- End Title -->

      <div class="row justify-content-lg-center">
        <div class="col-md-12 mb-7">
          <!-- Icon Blocks -->
          <div class="media pl-lg-5">
            <div class="media-body">
              <h3 class="h5">Lego</h3>
              <p class="mb-1">Built by one of the world’s leading manufacturers of play materials, Lego has been named “Toy of the Century” twice. </p>
            </div>
          </div>
          <!-- End Icon Blocks -->
        </div>

        <div class="col-md-12 mb-7">
          <!-- Icon Blocks -->
          <div class="media pl-lg-5">
            
            <div class="media-body">
              <h3 class="h5">Our Vision</h3>
              <p class="mb-1">The Dream Factory Kenya is proud to have sole distribution rights for Lego in Africa. Our collaboration allows us to help both the young and the ‘young at heart’ refine their motor skills and build a wealth of creative ideas through play. Since the unveiling of the partnership, we’ve used diverse platforms to grow the Lego brand across Africa and generate maximum footfall to our partner stores while building an emotional connection with consumers.
</p>
            </div>
          </div>
          <!-- End Icon Blocks -->
        </div>


        <div class="col-md-12">
          <!-- Icon Blocks -->
          <div class="media pl-lg-5">
            
            <div class="media-body">
              <h3 class="h5">Our Goal </h3>
              <p class="mb-1">Our goal is to get Lego into every home with the support of big retailers like Carrefour, Toy World, Chandarana supermarkets and many more.
</p>
            </div>
          </div>
          <!-- End Icon Blocks -->
        </div>
      </div>
    </div>
    <!-- End Hero Section -->









    <!-- Icon Blocks Section -->
    <div class="container space-1">
      <!-- Title -->
      <div class="w-md-80 w-lg-50 text-center mx-md-auto mb-9">
        <h2 class="font-weight-normal">Latest Arrivals</h2>
        <!-- <p class="mb-0">Enhance your brand with easy-to-use powerful customization features.</p> -->
      </div>
      <!-- End Title -->

      <!-- Features Section -->
        <div class="container space">
            <div class="js-slick-carousel u-slick u-slick--equal-height u-slick--gutters-3"
                data-slides-show="3"
                data-slides-scroll="1"
                data-pagi-classes="d-lg-none text-center u-slick__pagination mt-7 mb-0"
                data-responsive='[{
                    "breakpoint": 992,
                    "settings": {
                    "slidesToShow": 2
                    }
                }, {
                    "breakpoint": 768,
                    "settings": {
                    "slidesToShow": 2
                    }
                }, {
                    "breakpoint": 576,
                    "settings": {
                    "slidesToShow": 1
                    }
                }]'>
                <div class="js-slide">
                <!-- Card -->
                <div class="w-100 bg-soft-primary text-primary text-center rounded p-5">
                    <img class="img-fluid" src="/assets/other/A1xIo.png" alt="Image Description">
                    <h4 class="font-weight-semi-bold">Porsche</h4>
                </div>
                <!-- End Card -->
                </div>

                <div class="js-slide">
                <!-- Card -->
                <div class="w-100 bg-soft-info text-warning text-center rounded p-5">
                    <img class="img-fluid" src="/assets/other/extender.png" alt="Image Description">
                    <h4 class="font-weight-semi-bold">First Responder</h4>
                </div>
                <!-- End Card -->
                </div>

                <div class="js-slide">
                <!-- Card -->
                <div class="w-100 bg-soft-danger text-danger text-center rounded p-5">
                    <img class="img-fluid" src="/assets/other/209516093.png" alt="Image Description">
                    <h4 class="font-weight-semi-bold">Mack Anthem</h4>
                </div>
                <!-- End Card -->
                </div>
            </div>

            <div class="js-slick-carousel mt-3 u-slick u-slick--equal-height u-slick--gutters-3"
                data-slides-show="3"
                data-slides-scroll="1"
                data-pagi-classes="d-lg-none text-center u-slick__pagination mt-7 mb-0"
                data-responsive='[{
                    "breakpoint": 992,
                    "settings": {
                    "slidesToShow": 2
                    }
                }, {
                    "breakpoint": 768,
                    "settings": {
                    "slidesToShow": 2
                    }
                }, {
                    "breakpoint": 576,
                    "settings": {
                    "slidesToShow": 1
                    }
                }]'>
                <div class="js-slide">
                <!-- Card -->
                <div class="w-100 bg-soft-info text-success text-center rounded p-5">
                    <img class="img-fluid" src="/assets/other/boys.png" alt="Image Description">
                    <h4 class="font-weight-semi-bold">Building Kit</h4>
                </div>
                <!-- End Card -->
                </div>

                <div class="js-slide">
                <!-- Card -->
                <div class="w-100 bg-soft-danger text-danger text-center rounded p-5">
                    <img class="img-fluid" src="/assets/other/91iyZ5xgFeL.png" alt="Image Description">
                    <h4 class="font-weight-semi-bold">Super Heroes</h4>
                </div>
                <!-- End Card -->
                </div>

                <div class="js-slide">
                <!-- Card -->
                <div class="w-100 bg-soft-primary text-primary text-center rounded p-5">
                    <img class="img-fluid" src="/assets/other/ladies.png" alt="Image Description">
                    <h4 class="font-weight-semi-bold">Mack Anthem</h4>
                </div>
                <!-- End Card -->
                </div>
            </div>
        </div>
    <!-- End Features Section -->
    </div>
    <!-- End Icon Blocks Section -->


    <!-- Hero Section -->
    <div class="border-bottom">
      <div class="container text-center space-top-2 space-top-md-4 space-top-lg-3">
        <div class="w-md-75 mx-md-auto mb-5">
          <h1 class="display-4 font-size-md-down-5 font-weight-medium mb-3">Our mission</h1>
          <p class="lead">Our goal is to get LEGO into every home through a partnership with big retailers like Carrefour, Toy World, Chandarana supermarkets and many more.</p>
        </div>
      </div>
    </div>
    <!-- End Hero Section -->

    <!-- Testimonials Section -->
    <div class="bg-img-hero-bottom" style="background: url('/assets/img/czubXG2.gif') center; background-size: cover;">
      <div class="container">
        <div class="row justify-content-md-end">
          <div class="col-md-6 col-lg-5 col-xl-4 space-top-3 mb-n9" style="height:600px;">
            <!-- Testimonials -->
            
            <!-- End Testimonials -->
          </div>
        </div>
      </div>
    </div>
    <!-- End Testimonials Section -->

    <!-- Team Section -->
    <div class="container space-top-3 space-bottom-1 space-bottom-md-1">
      <!-- Title -->
      <div class="w-md-80 w-lg-50 text-center mx-md-auto mb-9">
        <h2 class="font-weight-normal">Fun facts about <span class="font-weight-semi-bold">Lego</span></h2>
      </div>
      <!-- End Title -->





      <div class="row">
        <div class="col-md-4 mb-7">
          <div class="text-center px-lg-3">
            <span class="btn btn-icon btn-lg btn-soft-danger rounded-circle mb-5">
              <span class="fab fa-yelp fa-2x btn-icon__inner btn-icon__inner-bottom-minus"></span>
            </span>
            <p class="mb-md-0">This is the 2nd most popular brand in the world and the no 1 toy brand in the world</p>
          </div>
        </div>

        <div class="col-md-4 mb-7">
          <div class="text-center px-lg-3">
            <span class="btn btn-icon btn-lg btn-soft-primary rounded-circle mb-5">
              <span class="fas fa-fire fa-2x btn-icon__inner btn-icon__inner-bottom-minus"></span>
            </span>
            <p class="mb-md-0">There are no LEGO sets with a war or military theme. This is because LEGO creator Ole Kirk Christiansen didn’t want to make war seem appealing to children.</p>
          </div>
        </div>

        <div class="col-md-4 mb-7">
          <div class="text-center px-lg-3">
            <span class="btn btn-icon btn-lg btn-soft-success rounded-circle mb-5">
              <span class="fab fa-whmcs fa-2x btn-icon__inner btn-icon__inner-bottom-minus"></span>
            </span>
            <p class="mb-md-0">18 out of every million LEGO pieces fail to meet the company standard. This is because the factory process is so streamlined and efficient.</p>
          </div>
        </div>
      </div>
    </div>
    <!-- End Team Section -->

    <!-- Subscribe Section -->
    <div class="container space-1">
      <div class="w-md-80 w-lg-50 text-center mx-md-auto">
        <!-- Title -->
        <div class="mb-9">
          <!-- <span class="btn btn-xs btn-soft-primary btn-pill mb-2">Subscribe</span> -->
          <h2 class="font-weight-normal">Stay in the know</h2>
          <p class="mb-0">Subscribe now and get special offers on the latest products.</p>
        </div>
        <!-- End Title -->

        <!-- Input -->
        <form class="js-validate js-form-message">
          <label class="sr-only" for="subscribeSrEmail">Enter your email address</label>
          <div class="input-group">
            <input type="text" class="form-control" name="name" id="subscribeSrEmail" placeholder="Enter your email address" aria-label="Enter your email address" aria-describedby="subscribeEmailButton" required
                   data-msg="Please enter a valid email address."
                   data-error-class="u-has-error"
                   data-success-class="u-has-success">
            <div class="input-group-append">
              <button type="submit" class="btn btn-primary" id="subscribeEmailButton">Subscribe</button>
            </div>
          </div>
        </form>
        <!-- End Input -->
      </div>
    </div>
    <!-- End Subscribe Section -->

    <!-- SVG Icon -->
    <div class="container">
      <div id="SVGsubscribe1" class="svg-preloader w-lg-80 mx-lg-auto">
        <figure class="ie-subscribe-1">
          <img class="js-svg-injector" src="/assets/svg/flat-icons/subscribe-1.svg" alt="Image Description"
               data-parent="#SVGsubscribe1">
        </figure>
      </div>
    </div>
    <!-- End SVG Icon -->
  </main>