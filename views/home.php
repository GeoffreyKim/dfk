<!-- ========== MAIN ========== -->
<main id="content" role="main">

    <!-- Hero v1 Section -->
    <div class="u-hero-v1">
            <!-- Hero Carousel Main -->
        <div id="heroNav" class="js-slick-carousel u-slick"
            data-autoplay="true"
            data-speed="10000"
            data-adaptive-height="true"
            data-arrows-classes="d-none d-lg-inline-block u-slick__arrow-classic u-slick__arrow-centered--y rounded-circle"
            data-arrow-left-classes="fas fa-arrow-left u-slick__arrow-classic-inner u-slick__arrow-classic-inner--left ml-lg-2 ml-xl-4"
            data-arrow-right-classes="fas fa-arrow-right u-slick__arrow-classic-inner u-slick__arrow-classic-inner--right mr-lg-2 mr-xl-4"
            data-numbered-pagination="#slickPaging"
            data-nav-for="#heroNavThumb">
            <div class="js-slide">
                <!-- Slide #2 -->
                <div class="d-lg-flex align-items-lg-center u-hero-v1__main" style="background-image: url('/assets/img/1920x800/legobrand.jpg');">
                    <div class="container space-3 space-top-md-5 space-top-lg-3">
                    <div class="row">
                        <div class="col-md-8 col-lg-6">
                            <!-- <span class="d-block h4 text-white font-weight-light mb-2"
                                    data-scs-animation-in="fadeInUp">
                                These
                            </span> -->
                            <h2 class="text-white display-4 font-size-md-down-5 mb-0"
                                data-scs-animation-in="fadeInUp"
                                data-scs-animation-delay="200">
                                <span class="font-weight-semi-bold">The Building Blocks of Fun </span>
                                <span class="d-block h4 text-white font-weight-light mb-2" data-scs-animation-in="fadeInUp">
                                    A fun way to build your child’s creativity and imagination.
                                </span>
                                
                            </h2>
                        </div>
                    </div>
                    </div>
                </div>
                <!-- End Slide #2 -->
            </div>

            <div class="js-slide">
                <!-- Slide #1 -->
                <div class="d-lg-flex align-items-lg-center u-hero-v1__main" style="background-image: url('/assets/img/1920x800/qualatext.jpg');">
                    <div class="container space-3 space-top-md-5 space-top-lg-3">
                        <div class="row">
                            <div class="col-md-8 col-lg-6 position-relative">
                                <span class="d-block h4 text-white font-weight-light mb-2"
                                        data-scs-animation-in="fadeInUp">
                                        <!-- Make Your Events ‘Pop’ -->
                                </span>
                                <!-- <h1 class="text-white display-4 font-size-md-down-5 mb-0"
                                    data-scs-animation-in="fadeInUp"
                                    data-scs-animation-delay="200">
                                    With <span class="font-weight-semi-bold">Qualatex</span>
                                </h1> -->
                                <span class="d-block h4 text-white font-weight-light mb-2" data-scs-animation-in="fadeInUp"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Slide #1 -->
            </div>
        </div>
        <!-- End Hero Carousel Main -->

        <!-- Slick Paging -->
        <!-- <div class="container position-relative">
            <div id="slickPaging" class="u-slick__paging"></div>
        </div> -->
        <!-- End Slick Paging -->

        <!-- Hero Carousel Secondary -->
            <div id="heroNavThumb" class="js-slick-carousel u-slick"
            data-autoplay="true"
            data-speed="10000"
            data-is-thumbs="true"
            data-nav-for="#heroNav">
                    <div class="js-slide"></div>
            </div>
    </div>
    <!-- End Hero Carousel Secondary -->



    <div class="row">
        <div class="col-md-6 col-lg-5 text-center mr-auto ml-auto mt-5">
            <a class="btn btn-warning btn-wide transition-3d-hover mb-1 mb-sm-0 mr-sm-1" href="/qualatex">Qualatex</a>
            <a class="btn btn-danger btn-wide transition-3d-hover mb-1 mb-sm-0 mr-sm-1" href="/lego">Lego</a>
            <a class="btn btn-primary btn-wide transition-3d-hover mb-1 mb-sm-0 mr-sm-1" href="/hasbro">Hasbro</a>
        </div>
    </div>


    <!-- Front in Frames Section -->
    <div class="overflow-hidden vector-bg">
        <div class="container space-2 space-md-2">
            <div class="row justify-content-between align-items-center">
            <div class="col-lg-5 mb-7 mb-lg-0">
                <div class="pr-md-4">
                <!-- Title -->
                <div class="mb-7">
                    <span class="btn btn-xs btn-soft-success btn-pill mb-2">About</span>
                    <h2 class="text-danger">The Dream <span class="font-weight-semi-bold">Factory</span></h2>
                    <p>We are a privately-owned limited liability company based in Nairobi, Kenya, with a presence in all East African countries. Established in 2007, we specialize in the distribution and marketing of international brands across the region.</p>
                    <p>Our current portfolio includes Lego, Qualatex, Hasbro of which we retain the sole distribution rights for the East African region.</p>
                </div>
                <!-- End Title -->
                </div>
            </div>

            <div class="col-lg-6 position-relative">
                <!-- Image Gallery -->
                <div class="row mx-gutters-2">
                <div class="col-5 align-self-end px-2 mb-3">
                    <!-- Fancybox -->
                    <a class="js-fancybox u-media-viewer" href="javascript:;"
                    data-src="/assets/img/IMG_9366.JPG"
                    data-fancybox="lightbox-gallery-hidden"
                    data-caption="Lego"
                    data-speed="700">
                    <img class="img-fluid rounded" src="/assets/img/IMG_9366Thumb.jpg" alt="Image Description">

                    <span class="u-media-viewer__container">
                        <span class="u-media-viewer__icon">
                        <span class="fas fa-plus u-media-viewer__icon-inner"></span>
                        </span>
                    </span>
                    </a>
                    <!-- End Fancybox -->
                </div>

                <div class="col-7 px-2 mb-3">
                    <!-- Fancybox -->
                    <a class="js-fancybox u-media-viewer" href="javascript:;"
                    data-src="/assets/img/IMG_9342.JPG"
                    data-fancybox="lightbox-gallery-hidden"
                    data-caption="Lego Sale"
                    data-speed="700">
                    <img class="img-fluid rounded" src="/assets/img/IMG_9342Thumb.jpg" alt="Image Description">

                    <span class="u-media-viewer__container">
                        <span class="u-media-viewer__icon">
                        <span class="fas fa-plus u-media-viewer__icon-inner"></span>
                        </span>
                    </span>
                    </a>
                    <!-- End Fancybox -->
                </div>

                <div class="col-5 offset-1 px-2 mb-3">
                    <!-- Fancybox -->
                    <a class="js-fancybox u-media-viewer" href="javascript:;"
                    data-src="/assets/img/Organic_Heart_LS.jpg"
                    data-fancybox="lightbox-gallery-hidden"
                    data-caption="Lego play"
                    data-speed="700">
                    <img class="img-fluid rounded" src="/assets/img/Organic_Heart_LSThumb.jpg" alt="Image Description">

                    <span class="u-media-viewer__container">
                        <span class="u-media-viewer__icon">
                        <span class="fas fa-plus u-media-viewer__icon-inner"></span>
                        </span>
                    </span>
                    </a>
                    <!-- End Fancybox -->
                </div>

                <div class="col-5 px-2 mb-3">
                    <!-- Fancybox -->
                    <a class="js-fancybox u-media-viewer" href="javascript:;"
                    data-src="/assets/img/cover.jpg"
                    data-fancybox="lightbox-gallery-hidden"
                    data-caption="Quaalatex"
                    data-speed="700">
                    <img class="img-fluid rounded" src="/assets/img/coverThumb.jpg" alt="Image Description">

                    <span class="u-media-viewer__container">
                        <span class="u-media-viewer__icon">
                        <span class="fas fa-plus u-media-viewer__icon-inner"></span>
                        </span>
                    </span>
                    </a>
                    <!-- End Fancybox -->
                </div>
                </div>
                <!-- End Image Gallery -->

                <!-- SVG Background Shape -->
                <div id="SVGbgShapeID1" class="svg-preloader w-100 content-centered-y z-index-n1">
                <figure class="ie-soft-triangle-shape">
                    <img class="js-svg-injector" src="/assets/svg/components/soft-triangle-shape.svg" alt="Image Description"
                        data-parent="#SVGbgShapeID1">
                </figure>
                </div>
                <!-- End SVG Background Shape -->
            </div>
            </div>
        </div>
    </div>
    <!-- End Front in Frames Section -->

    <!-- Testimonials Section gradient-half-warning-v1-->
    <div class="bg-danger u-devices-v2">
        <div class="container space-1 space-md-1">
            <div class="w-md-80 w-lg-50 text-center mx-md-auto mb-3">
                <h2 class="h3 font-weight-normal text-white">Recent Events</h2>
            </div>
            <div class="container space">
                <div class="row justify-content-lg-center">
                    <div class="col-md-12 mb-7">
                        <div class="media pl-lg-5">
                            <div class="media-body">
                                <h3 class="h5">Qualatex Training Event</h3>
                                <p class="mb-1 text-white">On the 16th and 17th of October, celebrated certified balloon artists, Dante Longhi and Keith Stirman flew in from the US and the UK to hold a 2 day workshop in Nairobi and were accompanied by the head of Emerging market, Jag Dhillon and the head of marketing, Ishani Dubb. The team, along with The Dream Factory Kenya, conducted a workshop on balloon design techniques and training which was very well received and was a popular and much talked about event. One of the highlights of the event was making the very first Kenyan flag out of balloons which was later displayed at Sarit Centre, Westlands. The delegates learnt how to make luxury bouquets, columns and balloon walls and balloon characters amongst other things. </p>
                            </div>
                        </div>
                    </div>
                </div>







                <div class="row">
                    <div class="col-sm-4 mb-3">
                        <a class="js-fancybox u-media-viewer" href="javascript:;"
                        data-src="/assets/img/IMG_0046.JPG"
                        data-fancybox="fancyboxGallery4"
                        data-caption="Front in frames - image #01"
                        data-speed="700"
                        data-overlay-blur-bg="true">
                        <img class="img-fluid rounded" src="/assets/img/IMG_0046.JPG" alt="Image Description">

                        <span class="u-media-viewer__container">
                            <span class="u-media-viewer__icon">
                            <span class="fas fa-plus u-media-viewer__icon-inner"></span>
                            </span>
                        </span>
                        </a>
                    </div>

                    <div class="col-sm-4 mb-3">
                        <a class="js-fancybox u-media-viewer" href="javascript:;"
                        data-src="/assets/img/IMG_0165.JPG"
                        data-fancybox="fancyboxGallery4"
                        data-caption="Front in frames - image #02"
                        data-speed="700"
                        data-overlay-blur-bg="true">
                        <img class="img-fluid rounded" src="/assets/img/IMG_0165.JPG" alt="Image Description">

                        <span class="u-media-viewer__container">
                            <span class="u-media-viewer__icon">
                            <span class="fas fa-plus u-media-viewer__icon-inner"></span>
                            </span>
                        </span>
                        </a>
                    </div>

                    <div class="col-sm-4 mb-3">
                        <a class="js-fancybox u-media-viewer" href="javascript:;"
                        data-src="/assets/img/IMG_0169.JPG"
                        data-fancybox="fancyboxGallery4"
                        data-caption="Front in frames - image #03"
                        data-speed="700"
                        data-overlay-blur-bg="true">
                        <img class="img-fluid rounded" src="/assets/img/IMG_0169.JPG" alt="Image Description">

                        <span class="u-media-viewer__container">
                            <span class="u-media-viewer__icon">
                            <span class="fas fa-plus u-media-viewer__icon-inner"></span>
                            </span>
                        </span>
                        </a>
                    </div>
                </div>




                
            </div>
        </div>
    </div>





    <div class="container space-top-3 space-bottom-2 space-bottom-md-3">
        <!-- Title -->
        <div class="w-md-80 text-center mx-md-auto mb-9">
            <h2 class="font-weight-normal">Lego Activation at Kabete International School</h2>
            <p>Kabete International School celebrated its 40th year with a grand fun day. The Dream Factory supported the school and participated in the event by setting up a Lego tent. Children could come and free play in the Lego pool and could also partake in competitions going on the whole day! We had a creative competition in which children were judged for the most creative design of a school, and we also had a speed challenge. The lucky winners were given gift vouchers to spend at the Lego kiosk in Westgate. We had a lot of excited children running around, and many also danced with our Lego mascot!</p>
        </div>

        <div class="row">
            <div class="col-sm-4 mb-3">
                <a class="js-fancybox u-media-viewer" href="javascript:;"
                data-src="/assets/img/IMG_0046.JPG"
                data-fancybox="fancyboxGallery4"
                data-caption="Front in frames - image #01"
                data-speed="700"
                data-overlay-blur-bg="true">
                <img class="img-fluid rounded" src="/assets/img/IMG_0046.JPG" alt="Image Description">

                <span class="u-media-viewer__container">
                    <span class="u-media-viewer__icon">
                    <span class="fas fa-plus u-media-viewer__icon-inner"></span>
                    </span>
                </span>
                </a>
            </div>

            <div class="col-sm-4 mb-3">
                <a class="js-fancybox u-media-viewer" href="javascript:;"
                data-src="/assets/img/IMG_0165.JPG"
                data-fancybox="fancyboxGallery4"
                data-caption="Front in frames - image #02"
                data-speed="700"
                data-overlay-blur-bg="true">
                <img class="img-fluid rounded" src="/assets/img/IMG_0165.JPG" alt="Image Description">

                <span class="u-media-viewer__container">
                    <span class="u-media-viewer__icon">
                    <span class="fas fa-plus u-media-viewer__icon-inner"></span>
                    </span>
                </span>
                </a>
            </div>

            <div class="col-sm-4 mb-3">
                <a class="js-fancybox u-media-viewer" href="javascript:;"
                data-src="/assets/img/IMG_0169.JPG"
                data-fancybox="fancyboxGallery4"
                data-caption="Front in frames - image #03"
                data-speed="700"
                data-overlay-blur-bg="true">
                <img class="img-fluid rounded" src="/assets/img/IMG_0169.JPG" alt="Image Description">

                <span class="u-media-viewer__container">
                    <span class="u-media-viewer__icon">
                    <span class="fas fa-plus u-media-viewer__icon-inner"></span>
                    </span>
                </span>
                </a>
            </div>
        </div>
    </div>




    <div class="overflow-hidden bg-danger">
        <div class="container space-2">
            <div class="row justify-content-between align-items-center">
                <div class="col-lg-5 mb-7 mb-lg-0">
                    <div class="pr-md-4">
                        <!-- Title -->
                        <div class="mb-7">
                            <h2 class="text-white"> <span class="font-weight-semi-bold">Tribal Chic</span></h2>
                            <p class="text-white">The Dream Factory brought together some of the best balloon artists in Nairobi to create “Clouds with a Silver Lining”, a balloon design done for their glamorous fashion show, Tribal Chic. This was the 10 year anniversary for the fashion show and this year the proceeds were going to Rafiki Mwema.</p>
                            <p class="text-white">The artists created clouds over the catwalk truss, falling down from the high ceiling, as well as a huge balloon wall. In total, approximately 15,000 balloons were used for this elaborate balloon design.</p>
                        </div>
                        <!-- End Title -->
                    </div>
                </div>

                <div class="col-lg-6 position-relative">
                    <!-- Image Gallery -->
                    <div class="row mx-gutters-2">
                        <div class="col-5 align-self-end px-2 mb-3">
                            <!-- Fancybox -->
                            <a class="js-fancybox u-media-viewer" href="javascript:;"
                                data-src="/assets/img/IMG_0046.JPG" data-fancybox="lightbox-gallery-hidden"
                                data-caption="Front in frames - image #01" data-speed="700">
                                <img class="img-fluid rounded" src="/assets/img/IMG_0046.JPG"
                                    alt="Image Description">

                                <span class="u-media-viewer__container">
                                    <span class="u-media-viewer__icon">
                                        <span class="fas fa-plus u-media-viewer__icon-inner"></span>
                                    </span>
                                </span>
                            </a>
                            <!-- End Fancybox -->
                        </div>

                        <div class="col-7 px-2 mb-3">
                            <!-- Fancybox -->
                            <a class="js-fancybox u-media-viewer" href="javascript:;"
                                data-src="/assets/img/IMG_0165.JPG" data-fancybox="lightbox-gallery-hidden"
                                data-caption="Front in frames - image #02" data-speed="700">
                                <img class="img-fluid rounded" src="/assets/img/IMG_0165.JPG"
                                    alt="Image Description">

                                <span class="u-media-viewer__container">
                                    <span class="u-media-viewer__icon">
                                        <span class="fas fa-plus u-media-viewer__icon-inner"></span>
                                    </span>
                                </span>
                            </a>
                            <!-- End Fancybox -->
                        </div>

                        <div class="col-5 offset-1 px-2 mb-3">
                            <!-- Fancybox -->
                            <a class="js-fancybox u-media-viewer" href="javascript:;"
                                data-src="../../assets/img/1920x1920/img1.jpg" data-fancybox="lightbox-gallery-hidden"
                                data-caption="Front in frames - image #03" data-speed="700">
                                <img class="img-fluid rounded" src="../../assets/img/280x310/img1.jpg"
                                    alt="Image Description">

                                <span class="u-media-viewer__container">
                                    <span class="u-media-viewer__icon">
                                        <span class="fas fa-plus u-media-viewer__icon-inner"></span>
                                    </span>
                                </span>
                            </a>
                            <!-- End Fancybox -->
                        </div>

                        <div class="col-5 px-2 mb-3">
                            <!-- Fancybox -->
                            <a class="js-fancybox u-media-viewer" href="javascript:;"
                                data-src="/assets/img/IMG_0169.JPG" data-fancybox="lightbox-gallery-hidden"
                                data-caption="Front in frames - image #04" data-speed="700">
                                <img class="img-fluid rounded" src="/assets/img/IMG_0169.JPG"
                                    alt="Image Description">

                                <span class="u-media-viewer__container">
                                    <span class="u-media-viewer__icon">
                                        <span class="fas fa-plus u-media-viewer__icon-inner"></span>
                                    </span>
                                </span>
                            </a>
                            <!-- End Fancybox -->
                        </div>
                    </div>
                    <!-- End Image Gallery -->

                    <!-- SVG Background Shape -->
                    <div id="SVGbgShapeID1" class="w-100 content-centered-y z-index-n1">
                        <figure class="ie-soft-triangle-shape">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
                                y="0px" viewBox="0 0 1109.8 797.1" style="enable-background:new 0 0 1109.8 797.1;"
                                xml:space="preserve" class="injected-svg js-svg-injector" data-parent="#SVGbgShapeID1">
                                <style type="text/css">
                                    .soft-triangle-shape-0 {
                                        fill: #377DFF;
                                    }
                                </style>
                                <path class="soft-triangle-shape-0 fill-primary" opacity=".1"
                                    d="M105.1,267.1C35.5,331.5-3.5,423,0.3,517.7C6.1,663,111,831.9,588.3,790.8c753-64.7,481.3-358.3,440.4-398.3  c-4-3.9-7.9-7.9-11.7-12L761.9,104.8C639.4-27.6,432.5-35.6,299.9,87L105.1,267.1z">
                                </path>
                            </svg>
                        </figure>
                    </div>
                    <!-- End SVG Background Shape -->
                </div>
            </div>
        </div>
    </div>

    


    <div class="gradient-half-primary-v2">
        <div class="container space-2 space-md-3 px-lg-7">
            <!-- Title -->
            <div class="w-md-80 text-center mx-md-auto mb-9">
                <h2>Lego Activation at TBC</h2>
                <p>The brand new Text Book Centre just opened in Sarit Centre. This flagship store is selling some of the newest and high-tech sets of Lego, as well as Hasbro. For the eventful opening weekend, we organised a lot of activities such as free play, competitions as well as our Lego mascot dancing around and entertaining the children!</p>
            </div>
            <!-- End Title -->
            <div class="row">
                <div class="col-sm-4 mb-3">
                    <a class="js-fancybox u-media-viewer" href="javascript:;"
                    data-src="/assets/img/IMG_0046.JPG"
                    data-fancybox="fancyboxGallery4"
                    data-caption="Front in frames - image #01"
                    data-speed="700"
                    data-overlay-blur-bg="true">
                    <img class="img-fluid rounded" src="/assets/img/IMG_0046.JPG" alt="Image Description">

                    <span class="u-media-viewer__container">
                        <span class="u-media-viewer__icon">
                        <span class="fas fa-plus u-media-viewer__icon-inner"></span>
                        </span>
                    </span>
                    </a>
                </div>

                <div class="col-sm-4 mb-3">
                    <a class="js-fancybox u-media-viewer" href="javascript:;"
                    data-src="/assets/img/IMG_0165.JPG"
                    data-fancybox="fancyboxGallery4"
                    data-caption="Front in frames - image #02"
                    data-speed="700"
                    data-overlay-blur-bg="true">
                    <img class="img-fluid rounded" src="/assets/img/IMG_0165.JPG" alt="Image Description">

                    <span class="u-media-viewer__container">
                        <span class="u-media-viewer__icon">
                        <span class="fas fa-plus u-media-viewer__icon-inner"></span>
                        </span>
                    </span>
                    </a>
                </div>

                <div class="col-sm-4 mb-3">
                    <a class="js-fancybox u-media-viewer" href="javascript:;"
                    data-src="/assets/img/IMG_0169.JPG"
                    data-fancybox="fancyboxGallery4"
                    data-caption="Front in frames - image #03"
                    data-speed="700"
                    data-overlay-blur-bg="true">
                    <img class="img-fluid rounded" src="/assets/img/IMG_0169.JPG" alt="Image Description">

                    <span class="u-media-viewer__container">
                        <span class="u-media-viewer__icon">
                        <span class="fas fa-plus u-media-viewer__icon-inner"></span>
                        </span>
                    </span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    





    <div class="container bg-white space-top-2 space-bottom-2 space-bottom-md-3">
        <div class="w-md-80 text-center mx-md-auto mb-9">
            <h2>Media</h2>
        </div>
        <div class="row">
            <div class="col-sm-4 mb-3">
                <a class="js-fancybox u-media-viewer" href="javascript:;"
                data-src="/assets/img/media0.jpg"
                data-fancybox="fancyboxGallery4"
                data-caption="Front in frames - image #01"
                data-speed="700"
                data-overlay-blur-bg="true">
                <img class="img-fluid rounded" src="/assets/img/media0.jpg" alt="Image Description">

                <span class="u-media-viewer__container">
                    <span class="u-media-viewer__icon">
                    <span class="fas fa-plus u-media-viewer__icon-inner"></span>
                    </span>
                </span>
                </a>
            </div>

            <div class="col-sm-4 mb-3">
                <a class="js-fancybox u-media-viewer" href="javascript:;"
                data-src="/assets/img/media2.jpg"
                data-fancybox="fancyboxGallery4"
                data-caption="Front in frames - image #02"
                data-speed="700"
                data-overlay-blur-bg="true">
                <img class="img-fluid rounded" src="/assets/img/media2.jpg" alt="Image Description">

                <span class="u-media-viewer__container">
                    <span class="u-media-viewer__icon">
                    <span class="fas fa-plus u-media-viewer__icon-inner"></span>
                    </span>
                </span>
                </a>
            </div>

            <div class="col-sm-4 mb-3">
                <a class="js-fancybox u-media-viewer" href="javascript:;"
                data-src="/assets/img/media4.jpg"
                data-fancybox="fancyboxGallery4"
                data-caption="Front in frames - image #03"
                data-speed="700"
                data-overlay-blur-bg="true">
                <img class="img-fluid rounded" src="/assets/img/media4.jpg" alt="Image Description">

                <span class="u-media-viewer__container">
                    <span class="u-media-viewer__icon">
                    <span class="fas fa-plus u-media-viewer__icon-inner"></span>
                    </span>
                </span>
                </a>
            </div>

            <div class="col-sm-4 mb-3">
                <a class="js-fancybox u-media-viewer" href="javascript:;"
                data-src="/assets/img/media1.jpg"
                data-fancybox="fancyboxGallery4"
                data-caption="Front in frames - image #01"
                data-speed="700"
                data-overlay-blur-bg="true">
                <img class="img-fluid rounded" src="/assets/img/media1.jpg" alt="Image Description">

                <span class="u-media-viewer__container">
                    <span class="u-media-viewer__icon">
                    <span class="fas fa-plus u-media-viewer__icon-inner"></span>
                    </span>
                </span>
                </a>
            </div>

            <div class="col-sm-4 mb-3">
                <a class="js-fancybox u-media-viewer" href="javascript:;"
                data-src="/assets/img/media3.jpg"
                data-fancybox="fancyboxGallery4"
                data-caption="Front in frames - image #02"
                data-speed="700"
                data-overlay-blur-bg="true">
                <img class="img-fluid rounded" src="/assets/img/media3.jpg" alt="Image Description">

                <span class="u-media-viewer__container">
                    <span class="u-media-viewer__icon">
                    <span class="fas fa-plus u-media-viewer__icon-inner"></span>
                    </span>
                </span>
                </a>
            </div>

            <div class="col-sm-4 mb-3">
                <a class="js-fancybox u-media-viewer" href="javascript:;"
                data-src="/assets/img/media5.jpg"
                data-fancybox="fancyboxGallery4"
                data-caption="Front in frames - image #03"
                data-speed="700"
                data-overlay-blur-bg="true">
                <img class="img-fluid rounded" src="/assets/img/media5.jpg" alt="Image Description">

                <span class="u-media-viewer__container">
                    <span class="u-media-viewer__icon">
                    <span class="fas fa-plus u-media-viewer__icon-inner"></span>
                    </span>
                </span>
                </a>
            </div>
        </div>
    </div>



    <div class="bg-light">
        <div class="container space-2 space-md-2">
            <!-- Title -->
            <div class="w-md-80 w-lg-50 text-center mx-md-auto mb-9">
            <span class="btn btn-xs btn-soft-success btn-pill mb-2">Our Team</span>
            <h2 class="text-danger">Trust the <span class="font-weight-semi-bold">Professionals</span></h2>
            <p>Our top professionals are ready to help with your business</p>
            </div>
            <!-- End Title -->

            <!-- Slick Carousel -->
            <div class="js-slick-carousel u-slick u-slick--gutters-3"
                data-slides-show="2"
                data-slides-scroll="1"
                data-pagi-classes="text-center u-slick__pagination mt-7 mb-0"
                data-responsive='[{
                "breakpoint": 992,
                "settings": {
                    "slidesToShow": 1
                }
                }, {
                "breakpoint": 768,
                "settings": {
                    "slidesToShow": 1
                }
                }, {
                "breakpoint": 554,
                "settings": {
                    "slidesToShow": 1
                }
                }]'>
                <div class="js-slide px-3">
                    <!-- Team -->
                    <div class="row">
                        <div class="col-sm-6 d-sm-flex align-content-sm-start flex-sm-column text-center text-sm-left mb-7 mb-sm-0">
                                <div class="w-100">
                                    <h3 class="h5 mb-4">Abdul Rahim</h3>
                                </div>
                                <div class="d-inline-block">
                                    <span class="badge badge-danger badge-pill badge-bigger mb-3">Head of Sales And Marketing</span>
                                </div>
                                <p class="font-size-1">A retail expert with extensive experience in the fields of brand incubation, new market penetration and retail sales and marketing, spanning over 25 years. Passionate about leading teams, mentorship and creating systems as vehicles for driving business with a deep focus on ROI management.</p>
                        </div>
                        <div class="col-sm-6">
                            <img class="img-fluid rounded mx-auto" src="/assets/img/350x400/abjul.gif" alt="Image Description">
                        </div>
                    </div>
                    <!-- End Team -->
                </div>

                <div class="js-slide px-3">
                    <!-- Team -->
                    <div class="row">
                    <div class="col-sm-6 d-sm-flex align-content-sm-start flex-sm-column text-center text-sm-left mb-7 mb-sm-0">
                        <div class="w-100">
                        <h3 class="h5 mb-4">Jean Claude Thesee</h3>
                        </div>
                        <div class="d-inline-block">
                        <span class="badge badge-danger badge-pill badge-bigger mb-3">Sales Manager</span>
                        </div>
                        <p class="font-size-1">Jean-Claude is the sales and marketing manager for the toys department at the Dream Factory Kenya. With over 6 years of experience in the sales sector in East Africa, he still has a little boy inside of him (perfect for toys, right?). The secret behind his proven track record is, 'look to make a customer and not a sale, so don't watch the clock, do what it does'.</p>
                    </div>
                    <div class="col-sm-6">
                        <img class="img-fluid rounded mx-auto" src="/assets/img/350x400/jean.gif" alt="Image Description">
                    </div>
                    </div>
                    <!-- End Team -->
                </div>

                
                <div class="js-slide px-3">
                    <!-- Team -->
                    <div class="row">
                    <div class="col-sm-6 d-sm-flex align-content-sm-start flex-sm-column text-center text-sm-left mb-7 mb-sm-0">
                        <div class="w-100">
                        <h3 class="h5 mb-4">Susan Wahinya</h3>
                        </div>
                        <div class="d-inline-block">
                        <span class="badge badge-danger badge-pill badge-bigger mb-3">Sales & Marketing Manager</span>
                        </div>
                        <p class="font-size-1">Susan Wahinya is a sales and marketing executive at The Dream Factory Kenya and a Qualatex Balloon Network Member. She is a certified balloon artist developing and elevating the balloon industry. She is also leading the Qualatex support group.</p>
                    </div>
                    <div class="col-sm-6">
                        <img class="img-fluid rounded mx-auto" src="/assets/img/350x400/susan1.gif" alt="Image Description">
                    </div>
                    </div>
                    <!-- End Team -->
                </div>

                <div class="js-slide px-3">
                    <!-- Team -->
                    <div class="row">
                    <div class="col-sm-6 d-sm-flex align-content-sm-start flex-sm-column text-center text-sm-left mb-7 mb-sm-0">
                        <div class="w-100">
                        <h3 class="h5 mb-4">Edwin Mahiri</h3>
                        </div>
                        <div class="d-inline-block">
                        <span class="badge badge-danger badge-pill badge-bigger mb-3">Retail & Events Manager</span>
                        </div>
                        <p class="font-size-1">Edwin Mahiri is marketing Guru, with more than 5 years experience working in an agency, He studied Marketing and 3D design. He is known for his agility towards pushing projects, going over and above his limits with one aim in mind; To make the client happy. He has a resilient personality, Charming and always smiling.</p>
                    </div>
                    <div class="col-sm-6">
                        <img class="img-fluid rounded mx-auto" src="/assets/img/350x400/eddy.gif" alt="Image Description">
                    </div>
                    </div>
                </div>
            </div>
            <!-- End Slick Carousel -->
        </div>
    </div>

        
  </main>
  <!-- ========== END MAIN ========== -->