<main id="content" role="main">
    <!-- Hero v1 Section -->
    <div class="u-hero-v1">
      <!-- Hero Carousel Main -->
      <div id="heroNav" class="js-slick-carousel u-slick"
           data-autoplay="true"
           data-speed="10000"
           data-adaptive-height="true"
           data-arrows-classes="d-none d-lg-inline-block u-slick__arrow-classic u-slick__arrow-centered--y rounded-circle"
           data-arrow-left-classes="fas fa-arrow-left u-slick__arrow-classic-inner u-slick__arrow-classic-inner--left ml-lg-2 ml-xl-4"
           data-arrow-right-classes="fas fa-arrow-right u-slick__arrow-classic-inner u-slick__arrow-classic-inner--right mr-lg-2 mr-xl-4"
           data-numbered-pagination="#slickPaging"
           data-nav-for="#heroNavThumb">

        <div class="js-slide">
          <!-- Slide #1 -->
          <div class="d-lg-flex align-items-lg-center u-hero-v1__main" style="background-image: url('/assets/img/1920x800/qualatext.jpg');">
            <div class="container space-3 space-top-md-5 space-top-lg-3">
              <div class="row">
                <div class="col-md-8 col-lg-6 position-relative">
                  <span class="d-block h4 text-white font-weight-light mb-2"
                        data-scs-animation-in="fadeInUp">
                        <!-- Make Your Events ‘Pop’ -->
                  </span>
                  <!-- <h1 class="text-white display-4 font-size-md-down-5 mb-0"
                      data-scs-animation-in="fadeInUp"
                      data-scs-animation-delay="200">
                    With <span class="font-weight-semi-bold">Qualatex</span>
                  </h1> -->
                  <span class="d-block h4 text-white font-weight-light mb-2" data-scs-animation-in="fadeInUp">
                            <!-- Design amazing balloon displays that turn your function into a festival. -->
                    </span>
                </div>
              </div>
            </div>
          </div>
          <!-- End Slide #1 -->
        </div>
      </div>
      <!-- End Hero Carousel Main -->

      <!-- Slick Paging -->
      <!-- <div class="container position-relative">
        <div id="slickPaging" class="u-slick__paging"></div>
      </div> -->
      <!-- End Slick Paging -->

      <!-- Hero Carousel Secondary -->
      <div id="heroNavThumb" class="js-slick-carousel u-slick"
           data-autoplay="true"
           data-speed="10000"
           data-is-thumbs="true"
           data-nav-for="#heroNav">
        <div class="js-slide">
          <!-- Slide #1 -->
          <!-- <div class="d-flex align-items-center bg-white u-hero-v1__secondary"> -->
            <!-- <div class="container space-2">
              <div class="row">
                <div class="col-lg-4">
                  <h3 class="h5 text-muted">
                    <span class="d-block text-danger"></span>
                  </h3>
                  <p class="mb-0">LEGO aspires to inspire and develop the builders of tomorrow through creative play and learning...a brick at a time.</p>
                </div>
              </div>
            </div>

            <div class="w-100 h-100 d-none d-lg-inline-block bg-danger u-hero-v1__last">
              <div class="u-hero-v1__last-inner">
                <h3 class="h5 text-white">
                Why<strong class="u-hero-v1__last-prev"> LEGO?</strong>
                </h3>
                <p class="text-white-70 mb-0">LEGO products are tested rigorously to live up to the strictest safety and quality standards so you and your child can have a fun playing and learning experience.</p>
              </div>
            </div> -->
          <!-- </div> -->
          
          
          <!-- End Slide #1 -->
        </div>

        <!-- <div class="js-slide"> -->
          <!-- Slide #2 -->
          <!-- <div class="d-flex align-items-center bg-white u-hero-v1__secondary">
            <div class="container space-2">
              <div class="row">
                <div class="col-lg-4">
                  <h3 class="h5 text-muted">
                  </h3>
                  <p class="mb-0">Qualatex is the premier brand of latex balloons, known throughout the world as The Very Best<sup>TM</sup> balloons. The Dream Factory is proud to be the sole distributors of Qualatex balloons in East Africa.</p>
                </div>
              </div>
            </div>

            <div class="w-100 h-100 d-none d-lg-inline-block bg-success u-hero-v1__last">
              <div class="u-hero-v1__last-inner">
                <h3 class="h5 text-white">
                  <strong class="u-hero-v1__last-next">Why</strong> Qualatex?
                </h3>
                <p class="text-white-70 mb-0">Qualatex high quality and unique designs make it the balloon brand of choice for event planners and balloon professionals all over the world.</p>
              </div>
            </div> -->
          <!-- </div> -->
          <!-- End Slide #2 -->
        </div>
      </div>
      <!-- End Hero Carousel Secondary -->
    </div>
    <!-- End Hero v1 Section -->
    <div class="row">
      <div class="col-md-6 col-lg-5 text-md-right mr-auto ml-auto mt-5">
          <a class="btn btn-warning btn-wide transition-3d-hover mb-1 mb-sm-0 mr-sm-1" href="/qualatex">Qualatex</a>
          <a class="btn btn-danger btn-wide transition-3d-hover mb-1 mb-sm-0 mr-sm-1" href="/lego">Lego</a>
          <a class="btn btn-primary btn-wide transition-3d-hover mb-1 mb-sm-0 mr-sm-1" href="/hasbro">Hasbro</a>
          
        </div>
    </div>







    <div class="container space-2 space-md-3">
      <!-- Title -->
      <div class="w-md-80 w-lg-50 text-center mx-md-auto mb-9">
        <small class="text-secondary text-uppercase font-weight-medium mb-2">What we do?</small>
        <h2 class="h3 font-weight-medium">Fast and easy design</h2>
      </div>
      <!-- End Title -->

      <div class="row justify-content-lg-center">
        <div class="col-md-6 col-lg-5 mb-7">
          <!-- Icon Blocks -->
          <div class="media pr-lg-5">
            <div class="media-body">
              <h3 class="h5">Our Balloons</h3>
              <p class="mb-1">Pioneer Balloon Company the manufacturers of Qualatex and Northstar Balloons manufacture a wide range of latex balloons, modelling balloons, foil balloons and bubble balloons. They also manufacture party supplies and balloon accessories. The latex balloons are the preferred choice for retailers and decorators due to their consistent size, shape and variety of colours! 
</p>
            </div>
          </div>
          <!-- End Icon Blocks -->
        </div>

        <div class="col-md-6 col-lg-5 mb-7">
          <!-- Icon Blocks -->
          <div class="media pl-lg-5">
            
            <div class="media-body">
              <h3 class="h5">Our Vision</h3>
              <p class="mb-1">The Dream Factory Kenya is proud to partner with Qualatex and is the sole distributor of the brand in Africa. This strategic collaboration is an indispensable part of how we serve our customers and grow our portfolio.</p>
            </div>
          </div>
          <!-- End Icon Blocks -->
        </div>

        <div class="w-100"></div>

        <div class="col-md-6 col-lg-5 mb-7 mb-lg-0">
          <!-- Icon Blocks -->
          <div class="media pr-lg-5">
           
            <div class="media-body">
              <h3 class="h5">Our Partners</h3>
              <p class="mb-1">We have also partnered with an extensive network of big retailers like Carrefour, Toy World, Chandarana, Party Depot and so on, to take Qualatex closer to consumers. Additionally, we have gained tremendous brand exposure and strengthened the relationship between the brand and customers through a mix of marketing interventions like brand activations, market storms, events and activations.</p>
            </div>
          </div>
          <!-- End Icon Blocks -->
        </div>

        <div class="col-md-6 col-lg-5">
          <!-- Icon Blocks -->
          <div class="media pl-lg-5">
            
            <div class="media-body">
              <h3 class="h5">Our Mission </h3>
              <p class="mb-1">Our mission is to grow the brand by making every event a Qualatex event, and offer endless opportunities to our partners to grow their business.</p>
            </div>
          </div>
          <!-- End Icon Blocks -->
        </div>
      </div>
    </div>





    <!-- Cubeportfolio Section -->
    <div class="container u-cubeportfolio">
        <!-- Filter -->
        <!-- <ul id="cubeFilter" class="list-inline cbp-l-filters-alignRight d-sm-flex">
            <li class="list-inline-item cbp-filter-item cbp-filter-item-active u-cubeportfolio__item mr-auto" data-filter="*">Show all</li>
            <li class="list-inline-item cbp-filter-item u-cubeportfolio__item" data-filter=".branding">Branding</li>
            <li class="list-inline-item cbp-filter-item u-cubeportfolio__item" data-filter=".abstract">Abstract</li>
            <li class="list-inline-item cbp-filter-item u-cubeportfolio__item" data-filter=".graphic">Graphic</li>
            <li class="list-inline-item cbp-filter-item u-cubeportfolio__item" data-filter=".illustration">Illustration</li>
        </ul> -->
        <!-- End Filter -->

        <!-- Content -->
        <div class="cbp mb-7"
                data-controls="#cubeFilter"
                data-animation="quicksand"
                data-x-gap="16"
                data-y-gap="16"
                data-load-more-selector="#cubeLoadMore"
                data-load-more-action="auto"
                data-load-items-amount="2"
                data-media-queries='[
                {"width": 1500, "cols": 4},
                {"width": 1100, "cols": 4},
                {"width": 800, "cols": 3},
                {"width": 480, "cols": 2},
                {"width": 300, "cols": 1}
                ]'>
            <!-- Item -->
            <div class="cbp-item rounded abstract">
            <div class="cbp-caption">
                <a class="cbp-lightbox u-media-viewer" href="/assets/img/qualatexbanner/Magic_2019_3_Santa_Falloon_Figure.png" data-title="Dashboard<br>by Paul Flavius">
                <img src="/assets/img/qualatexbanner/Magic_2019_3_Santa_Falloon_Figure.png" alt="Image Description">
                <span class="u-media-viewer__container">
                    <span class="u-media-viewer__icon">
                    <span class="fas fa-plus u-media-viewer__icon-inner"></span>
                    </span>
                </span>
                </a>
            </div>
            </div>
            <!-- End Item -->

            <!-- Item -->
            <div class="cbp-item rounded branding">
            <div class="cbp-caption">
                <a class="cbp-lightbox u-media-viewer" href="/assets/img/qualatexbanner/Number.png" data-title="World Clock<br>by Alex">
                <img src="/assets/img/qualatexbanner/Number.png" alt="Image Description">
                <span class="u-media-viewer__container">
                    <span class="u-media-viewer__icon">
                    <span class="fas fa-plus u-media-viewer__icon-inner"></span>
                    </span>
                </span>
                </a>
            </div>
            </div>
            <!-- End Item -->

            <!-- Item -->
            <div class="cbp-item rounded abstract">
            <div class="cbp-caption">
                <a class="cbp-lightbox u-media-viewer" href="/assets/img/qualatexbanner/pg3b_airfilled_flier2019.png" data-title="To-Do Dashboard<br>by Tiberiu Neamu">
                <img src="/assets/img/qualatexbanner/pg3b_airfilled_flier2019.png" alt="Image Description">
                <span class="u-media-viewer__container">
                    <span class="u-media-viewer__icon">
                    <span class="fas fa-plus u-media-viewer__icon-inner"></span>
                    </span>
                </span>
                </a>
            </div>
            </div>
            <!-- End Item -->

            <!-- Item -->
            <div class="cbp-item rounded branding">
            <div class="cbp-caption">
                <a class="cbp-lightbox u-media-viewer" href="/assets/img/qualatexbanner/pg10a_airfilled_flier2019.png" data-title="Events and  More<br>by Tiberiu Neamu">
                <img src="/assets/img/qualatexbanner/pg10a_airfilled_flier2019.png" alt="Image Description">
                <span class="u-media-viewer__container">
                    <span class="u-media-viewer__icon">
                    <span class="fas fa-plus u-media-viewer__icon-inner"></span>
                    </span>
                </span>
                </a>
            </div>
            </div>
            <!-- End Item -->

            <!-- Item -->
            <div class="cbp-item rounded abstract branding">
                <div class="cbp-caption">
                    <a class="cbp-lightbox u-media-viewer" href="/assets/img/qualatexbanner/pg10b_SS2020-US.png" data-title="Ski * Buddy<br>by Tiberiu Neamu">
                    <img src="/assets/img/qualatexbanner/pg10b_SS2020-US.png" alt="Image Description">
                    <span class="u-media-viewer__container">
                        <span class="u-media-viewer__icon">
                        <span class="fas fa-plus u-media-viewer__icon-inner"></span>
                        </span>
                    </span>
                    </a>
                </div>
            </div>
            <!-- End Item -->

            <!-- Item -->
            <div class="cbp-item rounded abstract branding">
                <div class="cbp-caption">
                    <a class="cbp-lightbox u-media-viewer" href="/assets/img/qualatexbanner/pg95a_QED_PEL.png" data-title="Ski * Buddy<br>by Tiberiu Neamu">
                    <img src="/assets/img/qualatexbanner/pg95a_QED_PEL.png" alt="Image Description">
                    <span class="u-media-viewer__container">
                        <span class="u-media-viewer__icon">
                        <span class="fas fa-plus u-media-viewer__icon-inner"></span>
                        </span>
                    </span>
                    </a>
                </div>
            </div>
            <!-- End Item -->

            <!-- Item -->
            <div class="cbp-item rounded abstract branding">
                <div class="cbp-caption">
                    <a class="cbp-lightbox u-media-viewer" href="/assets/img/qualatexbanner/PG109a.png" data-title="Ski * Buddy<br>by Tiberiu Neamu">
                    <img src="/assets/img/qualatexbanner/PG109a.png" alt="Image Description">
                    <span class="u-media-viewer__container">
                        <span class="u-media-viewer__icon">
                        <span class="fas fa-plus u-media-viewer__icon-inner"></span>
                        </span>
                    </span>
                    </a>
                </div>
            </div>
            <!-- End Item -->

            <!-- Item -->
            <div class="cbp-item rounded abstract branding">
                <div class="cbp-caption">
                    <a class="cbp-lightbox u-media-viewer" href="/assets/img/qualatexbanner/pg158c_QED_US.png" data-title="Ski * Buddy<br>by Tiberiu Neamu">
                    <img src="/assets/img/qualatexbanner/pg158c_QED_US.png" alt="Image Description">
                    <span class="u-media-viewer__container">
                        <span class="u-media-viewer__icon">
                        <span class="fas fa-plus u-media-viewer__icon-inner"></span>
                        </span>
                    </span>
                    </a>
                </div>
            </div>
            <!-- End Item -->

            <!-- Item -->
            <div class="cbp-item rounded abstract branding">
                <div class="cbp-caption">
                    <a class="cbp-lightbox u-media-viewer" href="/assets/img/qualatexbanner/pg184_QED_US.png" data-title="Ski * Buddy<br>by Tiberiu Neamu">
                    <img src="/assets/img/qualatexbanner/pg184_QED_US.png" alt="Image Description">
                    <span class="u-media-viewer__container">
                        <span class="u-media-viewer__icon">
                        <span class="fas fa-plus u-media-viewer__icon-inner"></span>
                        </span>
                    </span>
                    </a>
                </div>
            </div>
            <!-- End Item -->

            <!-- Item -->
            <div class="cbp-item rounded abstract branding">
                <div class="cbp-caption">
                    <a class="cbp-lightbox u-media-viewer" href="/assets/img/qualatexbanner/pg10b_airfilled_flier2019.png" data-title="Ski * Buddy<br>by Tiberiu Neamu">
                    <img src="/assets/img/qualatexbanner/pg10b_airfilled_flier2019.png" alt="Image Description">
                    <span class="u-media-viewer__container">
                        <span class="u-media-viewer__icon">
                        <span class="fas fa-plus u-media-viewer__icon-inner"></span>
                        </span>
                    </span>
                    </a>
                </div>
            </div>
            <!-- End Item -->





            <!-- Item -->
            <div class="cbp-item rounded abstract branding">
                <div class="cbp-caption">
                    <a class="cbp-lightbox u-media-viewer" href="/assets/img/qualatexbanner/2019_Images_3_Design_Inspiration_17.jpg" data-title="Ski * Buddy<br>by Tiberiu Neamu">
                    <img src="/assets/img/qualatexbanner/2019_Images_3_Design_Inspiration_17.jpg" alt="Image Description">
                    <span class="u-media-viewer__container">
                        <span class="u-media-viewer__icon">
                        <span class="fas fa-plus u-media-viewer__icon-inner"></span>
                        </span>
                    </span>
                    </a>
                </div>
            </div>
            <!-- End Item -->
            <!-- Item -->
            <div class="cbp-item rounded abstract branding">
                <div class="cbp-caption">
                    <a class="cbp-lightbox u-media-viewer" href="/assets/img/qualatexbanner/37076B.png" data-title="Ski * Buddy<br>by Tiberiu Neamu">
                    <img src="/assets/img/qualatexbanner/37076B.png" alt="Image Description">
                    <span class="u-media-viewer__container">
                        <span class="u-media-viewer__icon">
                        <span class="fas fa-plus u-media-viewer__icon-inner"></span>
                        </span>
                    </span>
                    </a>
                </div>
            </div>
            <!-- End Item -->
            <!-- Item -->
            <div class="cbp-item rounded abstract branding">
                <div class="cbp-caption">
                    <a class="cbp-lightbox u-media-viewer" href="/assets/img/qualatexbanner/91544B.png" data-title="Ski * Buddy<br>by Tiberiu Neamu">
                    <img src="/assets/img/qualatexbanner/91544B.png" alt="Image Description">
                    <span class="u-media-viewer__container">
                        <span class="u-media-viewer__icon">
                        <span class="fas fa-plus u-media-viewer__icon-inner"></span>
                        </span>
                    </span>
                    </a>
                </div>
            </div>
            <!-- End Item -->
            <!-- Item -->
            <div class="cbp-item rounded abstract branding">
                <div class="cbp-caption">
                    <a class="cbp-lightbox u-media-viewer" href="/assets/img/qualatexbanner/pg10a_airfilled_flier2019.png" data-title="Ski * Buddy<br>by Tiberiu Neamu">
                    <img src="/assets/img/qualatexbanner/pg10a_airfilled_flier2019.png" alt="Image Description">
                    <span class="u-media-viewer__container">
                        <span class="u-media-viewer__icon">
                        <span class="fas fa-plus u-media-viewer__icon-inner"></span>
                        </span>
                    </span>
                    </a>
                </div>
            </div>
            <!-- End Item -->
            <!-- Item -->
            <div class="cbp-item rounded abstract branding">
                <div class="cbp-caption">
                    <a class="cbp-lightbox u-media-viewer" href="/assets/img/qualatexbanner/bb.png" data-title="Ski * Buddy<br>by Tiberiu Neamu">
                    <img src="/assets/img/qualatexbanner/bb.png" alt="Image Description">
                    <span class="u-media-viewer__container">
                        <span class="u-media-viewer__icon">
                        <span class="fas fa-plus u-media-viewer__icon-inner"></span>
                        </span>
                    </span>
                    </a>
                </div>
            </div>
            <!-- End Item -->
            <!-- Item -->
            <div class="cbp-item rounded abstract branding">
                <div class="cbp-caption">
                    <a class="cbp-lightbox u-media-viewer" href="/assets/img/qualatexbanner/unnamed.png" data-title="Ski * Buddy<br>by Tiberiu Neamu">
                    <img src="/assets/img/qualatexbanner/unnamed.png" alt="Image Description">
                    <span class="u-media-viewer__container">
                        <span class="u-media-viewer__icon">
                        <span class="fas fa-plus u-media-viewer__icon-inner"></span>
                        </span>
                    </span>
                    </a>
                </div>
            </div>
            <!-- End Item -->
        </div>
        <!-- End Content -->
    </div>
    <!-- End Cubeportfolio Section -->
</main>