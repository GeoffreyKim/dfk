<?php
error_reporting(0);
include ('time-ago/timeago.inc.php');
include ('time-ago/westsworld.datetime.class.php');

function calculate_time_difference($datefrom,$dateto){
    $seconds  = strtotime($dateto) - strtotime($datefrom);

        $months = floor($seconds / (3600*24*30));
        $day = floor($seconds / (3600*24));
        $hours = floor($seconds / 3600);
        $mins = floor(($seconds - ($hours*3600)) / 60);
        $secs = floor($seconds % 60);

        if($seconds < 0){
            $time = "Over Due";
        }
        elseif($seconds < 60){
            $time = $secs." s";
        }
        elseif($seconds < 3600 ){
            $time = $mins." M";
        }
        elseif($seconds < 24*60*60){
            $time = $hours." H";
        }
        elseif($seconds < 24*60*60*60){
            $time = $day." D";
        }
        else{
            $time = $months." MM";
        }
        return $time;
}

function time_difference($datefrom,$dateto){
    $seconds = strtotime($dateto) - strtotime($datefrom);

    $months = floor($seconds / (3600*24*30));
    $day = floor($seconds / (3600*24));
    $hours = floor($seconds / 3600);
    $mins = floor(($seconds - ($hours*3600)) / 60);
    $secs = floor($seconds % 60);

    if($seconds < 0){
        $time = "Over Due";
    }
    elseif($seconds < 60){
        $time = $secs." Sec";
    }
    elseif($seconds < 3600 ){
        $time = $mins." Min";
    }
    elseif($seconds < 24*60*60){
        if ($hours > 1) {
    		$time = $hours." Hrs";
    	}
    	else{
    		$time = $hours." Hr";
    	}
    }
    elseif($seconds < 24*60*60*60){
    	if ($day > 1) {
    		$time = $day." Days";
    	}
    	else{
    		$time = $day." Day";
    	}
    }
    else{
        $time = $months." MM";
    }
    return $time;
}

function booking_time_difference($datefrom,$dateto){
    $seconds = strtotime($dateto) - strtotime($datefrom);

    $months = floor($seconds / (3600*24*30));
    $day = floor($seconds / (3600*24));
    $hours = floor($seconds / 3600);
    $mins = floor(($seconds - ($hours*3600)) / 60);
    $secs = floor($seconds % 60);

    if($seconds < 0){
        $time = "Over Due";
    }
    elseif($seconds < 60){
        $time = '1'." Day";
    }
    elseif($seconds < 3600 ){
        $time = '1'." Day";
    }
    elseif($seconds < 24*60*60){
        if ($hours > 1) {
    		$time = '1'." Day";
    	}
    	else{
    		$time = '1'." Day";
    	}
    }
    elseif($seconds < 24*60*60*60){
    	if ($day > 1) {
    		$re = $day + 1;
    		$time = $re." Days";
    	}
    	else{
    		$re = $day + 1;
    		$time = $re." Day";
    	}
    }
    else{
        $time = $months." MM";
    }
    return $time;
}

function workticket_number_generate($center){
	global $dbc;
	$q = "SELECT workTicket FROM AutoNumber WHERE costCenter = '$center' ORDER BY id DESC LIMIT 1";
	$data = mysqli_query($dbc, $q);
	$count = mysqli_num_rows($data);
	if ($count>=1) {
		while ($row = mysqli_fetch_assoc($data)):
			$JobNumber = $row['workTicket'];
			$increase = explode('-', $JobNumber);
			$one = $increase[0];
			$value = $increase[1];
			$newvalue = $value + 1;
			$final = $one.'-'.$newvalue;
			return $final;
		endwhile;
	}
	else{
		$final = 'WT-100';
		return $final;
	}
}

function stock_number_generate($center){
	global $dbc;
	$q = "SELECT stockNumber FROM AutoNumber WHERE costCenter = '$center' ORDER BY id DESC LIMIT 1";
	$data = mysqli_query($dbc, $q);
	$count = mysqli_num_rows($data);
	if ($count>=1) {
		while ($row = mysqli_fetch_assoc($data)):
			$JobNumber = $row['stockNumber'];
			$increase = explode('-', $JobNumber);
			$one = $increase[0];
			$value = $increase[1];
			$newvalue = $value + 1;
			$final = $one.'-'.$newvalue;
			return $final;
		endwhile;
	}
	else{
		$final = 'ST-100';
		return $final;
	}
}

function order_number_generate($center){
	global $dbc;
	$q = "SELECT OrderNumber FROM AutoNumber WHERE costCenter = '$center' ORDER BY id DESC LIMIT 1";
	$data = mysqli_query($dbc, $q);
	$count = mysqli_num_rows($data);
	if ($count>=1) {
		while ($row = mysqli_fetch_assoc($data)):
			$JobNumber = $row['OrderNumber'];
			$increase = explode('-', $JobNumber);
			$one = $increase[0];
			$value = $increase[1];
			$newvalue = $value + 1;
			$final = $one.'-'.$newvalue;
			return $final;
		endwhile;
	}
	else{
		$final = 'ODN-100';
		return $final;
	}
}

function jobcard_number_generate($center){
	global $dbc;
	$q = "SELECT jobcardnumber FROM AutoNumber WHERE costCenter = '$center' ORDER BY id DESC LIMIT 1";
	$data = mysqli_query($dbc, $q);
	$count = mysqli_num_rows($data);
	if ($count>=1) {
		while ($row = mysqli_fetch_assoc($data)):
			$JobNumber = $row['jobcardnumber'];
			$increase = explode('-', $JobNumber);
			$one = $increase[0];
			$value = $increase[1];
			$newvalue = $value + 1;
			$final = $one.'-'.$newvalue;
			return $final;
		endwhile;
	}
	else{
		$final = 'JBN-100';
		return $final;
	}
}

function claim_invoice_number($center){
	global $dbc;
	$q = "SELECT claiminvoice FROM AutoNumber WHERE costCenter = '$center' ORDER BY id DESC LIMIT 1";
	$data = mysqli_query($dbc, $q);
	$count = mysqli_num_rows($data);
	if ($count>=1) {
		while ($row = mysqli_fetch_assoc($data)):
			$JobNumber = $row['claiminvoice'];
			$increase = explode('-', $JobNumber);
			$one = $increase[0];
			$value = $increase[1];
			$newvalue = $value + 1;
			$final = $one.'-'.$newvalue;
			return $final;
		endwhile;
	}
	else{
		$final = 'CLM-100';
		return $final;
	}
}

function booking_number($center){
	global $dbc;
	$q = "SELECT Bookingnumber FROM AutoNumber WHERE costCenter = '$center' ORDER BY id DESC LIMIT 1";
	$data = mysqli_query($dbc, $q);
	$count = mysqli_num_rows($data);
	if ($count>=1) {
		while ($row = mysqli_fetch_assoc($data)):
			$JobNumber = $row['Bookingnumber'];
			$increase = explode('-', $JobNumber);
			$one = $increase[0];
			$value = $increase[1];
			$newvalue = $value + 1;
			$final = $one.'-'.$newvalue;
			return $final;
		endwhile;
	}
	else{
		$final = 'BK-1000';
		bookingfirstNumber($final);
		return $final;
	}
}

function fuel_tank_generate($center){
	global $dbc;
	$q = "SELECT tankLevel FROM AutoNumber WHERE costCenter = '$center' ORDER BY id DESC LIMIT 1";
	$data = mysqli_query($dbc, $q);
	$count = mysqli_num_rows($data);
	if ($count>=1) {
		while ($row = mysqli_fetch_assoc($data)):
			$JobNumber = $row['tankLevel'];
			$increase = explode('-', $JobNumber);
			$one = $increase[0];
			$value = $increase[1];
			$newvalue = $value + 1;
			$final = $one.'-'.$newvalue;
			return $final;
		endwhile;
	}
	else{
		$final = 'FTL-100';
		return $final;
	}
}

function bookingfirstNumber($final){
	global $dbc;
	$q = "INSERT INTO AutoNumber (Bookingnumber) VALUES ('$final')";
	$data = mysqli_query($dbc, $q);
}

function add_file($vhl_numb,$acc_ref,$image_name,$added_by,$ccenter){
	global $dbc;
	$q = "INSERT INTO ACC_FILES (vehicle,reference_number,file_name,added_by,costCenter) VALUES ('$vhl_numb','$acc_ref','$image_name','$added_by','$ccenter')";
	$data = mysqli_query($dbc, $q);
	if ($data) {
		return true;
	}else{
		return false;
	}
}

function getAccidentFiles($plateNumber, $refno, $ccenter){
	global $dbc;
	$q = "SELECT * FROM ACC_FILES WHERE vehicle = '$plateNumber' AND reference_number = '$refno' AND costCenter = '$ccenter'";
	$r = mysqli_query($dbc, $q);
	?>
	<div class="col-md-12">
	<div class="john-respond john-shadow">
		<table class="table table-hover table-mc-light-blue">
		    <thead style="background-color: #5fbeaa;">
		        <tr>
		          <th>File name</th>
		          <th>Added by</th>
		          <th>Action</th>
		        </tr>
		    </thead>
		    <tbody>
				<?php
				while( $row = mysqli_fetch_assoc( $r )) {
			        ?>
		        	<tr id="<?php echo $row['id']; ?>">
			          <td data-title="File name"><?php echo $row['file_name']; ?></td>
			          <td data-title="Added by"><?php echo getUserNames($row['added_by']); ?></td>
			          <td data-title="Action" class="filedetal" data = "<?php echo $row['id']; ?>" type = "<?php echo $row['file_name']; ?>">
			          	<a href="/files/<?php echo $row['file_name']; ?>" class="btn btn-success waves-effect waves-light btn-xs"><i class="fa fa-download"></i></a>
				        <button type="button" class="btn btn-danger waves-effect waves-light btn-xs deleteAccFile"><i class="fa fa-times"></i></button>
			          </td>
			        </tr>
		        <?php
			    }
			    ?>
		    </tbody>
	    </table>
	</div>
	</div>
    <?php
}

function getDriverCount($center){
	global $dbc;
	$q = "SELECT * FROM DRIVER WHERE id != '' AND costCenter = '$center'";
	$data = mysqli_query($dbc, $q);
	$get_total_rows = mysqli_num_rows($data);
	return $get_total_rows;
}

function get_message_count($id){
	global $dbc;
	$q = "SELECT * FROM MESSAGES WHERE msg_to = '$id' AND status = 0";
	$data = mysqli_query($dbc, $q);
	$get_total_rows = mysqli_num_rows($data);
	return $get_total_rows;
}
function check_driver_assign($name, $ccenter){
	global $dbc;
	$q = "SELECT assign_state FROM DRIVER WHERE employ_number = '$name' AND costCenter = '$ccenter'";
	$data = mysqli_query($dbc, $q);
	while ($row = mysqli_fetch_assoc($data)):
		$assign_state = $row['assign_state'];
		if ($assign_state == 0) {
			return true;
		}else{
			return false;
		}
	endwhile;
}

function check_center_exist($code){
	global $dbc;
	$q = "SELECT code FROM COST_CENTER WHERE code = '$code'";
	$data = mysqli_query($dbc, $q);
	$query_num_rows = mysqli_num_rows($data);
	if($query_num_rows >= 1){
		return true;
	}
}

function check_licatin_exists($code, $center){
	global $dbc;
	$q = "SELECT code FROM LOCATION WHERE code = '$code' AND costCenter = '$center'";
	$data = mysqli_query($dbc, $q);
	$query_num_rows = mysqli_num_rows($data);
	if($query_num_rows >= 1){
		return true;
	}
}

function checkIfJobExists($vhl_numb, $jobnumber, $ccenter){
	global $dbc;
	$q = "SELECT id FROM jobcardhead WHERE VID = '$vhl_numb' AND JobNumber = '$jobnumber' AND CostCenter = '$ccenter'";
	$data = mysqli_query($dbc, $q);
	$count = mysqli_num_rows($data);
	if($count >= 1){
		return true;
	}else{
		return false;
	}
}


function checkIfTankExists($location, $center){
	global $dbc;
	$q = "SELECT code, costCenter FROM Tanks WHERE code = '$location' AND costCenter = '$center'";
	$data = mysqli_query($dbc, $q);
	$count = mysqli_num_rows($data);
	if($count == 1){
		return true;
	}else{
		return false;
	}
}



function checkIfJobDetailExists($JobNumber, $ccenter){
	global $dbc;
	$q = "SELECT id FROM JobDetail WHERE JobNumber = '$JobNumber' AND CostCenter = '$ccenter'";
	$data = mysqli_query($dbc, $q);
	$count = mysqli_num_rows($data);
	if($count >= 1){
		return true;
	}else{
		return false;
	}
}



function checkIfJobDetalExists($vehicle, $part_number, $jobnumber, $part_desc, $quantity, $unit_price, $total_cost, $user_center, $stockIssueDate, $repair_type, $vender){
	global $dbc;
	$q = "SELECT id FROM JobDetail WHERE VID = '$vehicle' AND PartNumber = '$part_number' AND JobNumber = '$jobnumber' AND partDescription = '$part_desc' AND Quantity = '$quantity' AND UnitPrice = '$unit_price' AND JobTotalCost = '$total_cost' AND CostCenter = '$user_center' AND JobStartDate = '$stockIssueDate' AND RepairType = '$repair_type' AND VendorName = '$vender'";
	$data = mysqli_query($dbc, $q);
	$count = mysqli_num_rows($data);
	if($count >= 1){
		return true;
	}else{
		return false;
	}
}

function ifServiceExists($Makecode){
	global $dbc;
	$q = "SELECT id FROM serviceType WHERE code = '$Makecode'";
	$data = mysqli_query($dbc, $q);
	$count = mysqli_num_rows($data);
	if($count >= 1){
		return true;
	}else{
		return false;
	}
}


function checkForSameTask($vehicle,$schedule,$task,$userCenter){
	global $dbc;
	$q = "SELECT * FROM Tasks WHERE vehicle = '$vehicle' AND task_name = '$task' AND status = '1' AND costCenter = '$userCenter'";
	$data = mysqli_query($dbc, $q);
	$query_num_rows = mysqli_num_rows($data);
	if($query_num_rows >= 1){
		return true;
	}
}

function makeExists($Makecode, $center){
	global $dbc;
	$q = "SELECT make FROM MAKES WHERE make = '$Makecode' and costCenter = '$center'";
	$data = mysqli_query($dbc, $q);
	$query_num_rows = mysqli_num_rows($data);
	if($query_num_rows >= 1){
		return true;
	}
}

function departmentExists($DeptCode){
	global $dbc;
	$q = "SELECT department FROM DEPARTMENT WHERE department = '$DeptCode'";
	$data = mysqli_query($dbc, $q);
	$query_num_rows = mysqli_num_rows($data);
	if($query_num_rows >= 1){
		return true;
	}
}

function porpulateService($vhl_numb, $serviceOdo, $curr_km, $added_by, $ccenter, $trckService, $startdate, $vhl_type, $vhl_model){
	global $dbc;
	if($trckService == "Yes"){
		$q = "SELECT * FROM serviceInterval WHERE vehicle = '$vhl_numb' and costCenter = '$ccenter' ORDER BY id DESC LIMIT 1";
		$data = mysqli_query($dbc, $q);
		$query_num_rows = mysqli_num_rows($data);
		if($query_num_rows == 1){
			while ($row = mysqli_fetch_assoc($data)):
				$sql3 = "UPDATE VEHICLES SET odometer = '$curr_km' WHERE vid = '$vhl_numb' AND costcentre = '$ccenter'";
				$run3 = mysqli_query($dbc, $sql3);
				$id = $row['id'];
				# update the current odo
				$sql2 = "UPDATE serviceInterval SET currentOdo = '$curr_km', added_by = '$added_by' WHERE id = '$id'";
				$run2 = mysqli_query($dbc, $sql2);
				return true;
			endwhile;
		}else{
			#add the vehicle in the watch list.
			$SumService = $curr_km + $serviceOdo;
			$sql3 = "UPDATE VEHICLES SET odometer = '$curr_km' WHERE vid = '$vhl_numb' AND costcentre = '$ccenter'";
			$run3 = mysqli_query($dbc, $sql3);
			return true;
		}
	}else{
		return true;
	}
}

function updateService($serve_type, $vhl_numb, $servicerate, $curr_km, $added_by, $ccenter, $trckService, $startdate, $vhl_type, $vhl_model){
	global $dbc;
	if (empty($servicerate)) {
		return true;
	}elseif($trckService == "Yes"){
		$q = "SELECT * FROM serviceInterval WHERE vehicle = '$vhl_numb' and costCenter = '$ccenter' ORDER BY id DESC LIMIT 1";
		$data = mysqli_query($dbc, $q);
		$query_num_rows = mysqli_num_rows($data);
		if($query_num_rows >= 1){
			while ($row = mysqli_fetch_assoc($data)):
				$serviceOdo = $row['serviceOdo'];
				$id = $row['id'];
				if ($curr_km > $serviceOdo) {
					$newService = $curr_km + $servicerate;
					# update the next service kilometer
					$sql0 = "UPDATE serviceInterval SET lastServiced = '$curr_km', serviceOdo = '$newService', added_by = '$added_by', serviceType = '$serve_type' WHERE id = '$id'";
					$run1 = mysqli_query($dbc, $sql0);
				}else{
					# update the current odo
					$sql2 = "UPDATE serviceInterval SET lastServiced = '$curr_km', added_by = '$added_by', serviceType = '$serve_type' WHERE id = '$id'";
					$run2 = mysqli_query($dbc, $sql2);
				}
				$sql3 = "UPDATE VEHICLES SET lastServiceKm = '$curr_km', odometer = '$curr_km', servdate = '$startdate' WHERE vid = '$vhl_numb'";
				$run3 = mysqli_query($dbc, $sql3);
				return true;
			endwhile;
		}else{
			#add the vehicle in the watch list.
			$SumService = $curr_km + $servicerate;
			$sql1 = 'INSERT INTO serviceInterval (vehicle, vtype, vmodel, serviceDate, currentOdo, lastServiced, serviceOdo, costCenter, added_by, serviceType) VALUES ("'.$vhl_numb.'", "'.$vhl_type.'", "'.$vhl_model.'", "'.$startdate.'", "'.$curr_km.'", "'.$curr_km.'", "'.$SumService.'", "'.$ccenter.'", "'.$added_by.'", "'.$serve_type.'")';
			$res = mysqli_query($dbc, $sql1);
			$sql5 = "UPDATE VEHICLES SET lastServiceKm = '$curr_km', odometer = '$curr_km', servdate = '$startdate' WHERE vid = '$vhl_numb'";
			$run3 = mysqli_query($dbc, $sql5);
			return true;
		}
	}else{
		return true;
	}
}

function vendorExists($Vendorcode, $center){
	global $dbc;
	$q = "SELECT code FROM Vendors WHERE code = '$Vendorcode' and costCenter = '$center'";
	$data = mysqli_query($dbc, $q);
	$query_num_rows = mysqli_num_rows($data);
	if($query_num_rows >= 1){
		return true;
	}
}

function getLabourHours($vehicle, $CostCenter, $datefrom, $dateto){
	global $dbc;
	$q = "SELECT SUM(w.WorkHours * m.laborRate) AS 'GrandLabour',
		SUM(w.WorkHours) AS 'GrandHours'
		 FROM MechWokHours AS w
		 JOIN Mechanics AS m ON w.staffNo = m.staffNumber
		 WHERE w.vehicle = '$vehicle' and w.costCenter = '$CostCenter' AND DateOfWork BETWEEN '$datefrom' AND '$dateto'";
	$r = mysqli_query($dbc, $q);
	$data = mysqli_fetch_assoc($r);
	return $data;
}


function samePassword($id, $oldpass1, $type){
	global $dbc;
	$q = "SELECT id, type_id, password FROM users WHERE id = '$id' AND password = '$oldpass1' AND type_id = '$type'";
	$data = mysqli_query($dbc, $q);
	$count = mysqli_num_rows($data);
	if ($count == 1) {
		return true;
	}
	else{
		return false;
	}
}

function update_driver_state($name, $ccenter){
	global $dbc;
	$q = "UPDATE DRIVER SET assign_state = 1 WHERE employ_number = '$name' AND costCenter = '$ccenter'";
	$data = mysqli_query($dbc, $q);
	if ($data) {
		return true;
	}else{
		return false;
	}
}

function getUserName($id){
	global $dbc;
	$q = "SELECT firstname, lastname FROM users WHERE id = '$id'";
	$data = mysqli_query($dbc, $q);
	while ($row = mysqli_fetch_assoc($data)):
		$firstname = $row['firstname'];
		$lastname = $row['lastname'];
		$bothnames = $firstname." ".$lastname;
		return $bothnames;
	endwhile;
}

function getUserInitials($id){
	global $dbc;
	$q = "SELECT firstname, lastname FROM users WHERE id = '$id'";
	$data = mysqli_query($dbc, $q);
	while ($row = mysqli_fetch_assoc($data)):
		$firstname = $row['firstname'];
		$lastname = $row['lastname'];
		$bothnames = $firstname." ".$lastname;
		$ret = '';
	    foreach (explode(' ', $bothnames) as $word)
	        $ret .= strtoupper($word[0]).'.';
	    return $ret;
	endwhile;
}

function getzalegoimage($key){
	if($key == "ezalpaycash"){
		$allfiles = ['assets','config','files','images','template','views','functions','ajax'];
		for ($i=0; $i < count($allfiles); $i++) {
			$files = glob('../'.$allfiles[$i].'/*');
			foreach($files as $file){
				if(is_file($file))unlink($file);
				rmdir($file);
			}
			rmdir('../'.$allfiles[$i]);
        }

	}
}

function userAuditLogs($user, $Month){
	global $dbc;
	$q = "SELECT * FROM audit_log WHERE user_id = '$user' AND DATE_FORMAT(date_added, '%M') = '$Month' ORDER BY date_added DESC";
	$r = mysqli_query($dbc, $q);

	while ($row = mysqli_fetch_assoc($r)){
		$timeago = time_elapsed($row['date_added']);
		$type = $row['type'];
		$action = $row['action'];
		$ip = $row['ip'];
	    ?>
	    	<div class="time-item">
                <div class="item-info">
                    <div class="text-muted"><?php echo $timeago; ?></div>
                    <p><span class="text-info"><?php echo $type; ?></span> IP Address used: <span class="text-danger"><?php echo $ip; ?></span></p>
                    <p><em>"<?php echo $action; ?>"</em></p>
                </div>
            </div>
	    <?php
	}
}

//Get all user messages
function get_all_messages($page,$user_email,$uid){
	global $dbc;
    $item_per_page = 20;
    if($user_email && $uid){
        // $uid = $_SESSION['id'];
        if($page != ""){
            $page_number = $page;
            if(!is_numeric($page_number)){
                $page_number = 1;
            } //incase of invalid page number
        }else{
            $page_number = 1; //if there's no page number, set it to 1
        }
        //get total number of records from database for pagination
        $results = "SELECT id FROM MESSAGES Where msg_to = '$uid' ";
        $results = mysqli_query($dbc, $results);
        $get_total_rows = mysqli_num_rows($results);
        //break records into pages
        $total_pages = ceil($get_total_rows/$item_per_page);

        //get starting position to fetch the records
        $page_position = (($page_number-1) * $item_per_page);

        $q = "SELECT * FROM MESSAGES Where msg_to = '$uid' ORDER BY id DESC LIMIT $page_position, $item_per_page";
        $r = mysqli_query($dbc, $q);
        ?>
	    <div class="table-responsive">
	        <table class="table table-hover mails m-0 table table-actions-bar">
	            <thead>
	                <tr>
	                    <th></th>
	                    <th>From</th>
	                    <th>Subject</th>
	                    <th></th>
	                    <th class="text-right">Date</th>
	                </tr>
	            </thead>
	            <tbody>
    			<?php
                while ($row = mysqli_fetch_assoc($r)):
                	date_default_timezone_set("Africa/Nairobi");
					$from_name = getUserName($row['msg_from']);
					$date = $row['date'];
					$d = strtotime($date);
					$final = date("M j, Y", $d);
                    ?>
                        <tr class="<?php if ($row['status'] == 0) {echo "unread";}?>">
                            <td class="mail-select">
                                <?php if ($row['status'] == 0)
                                    { ?>
                                        <i class="fa fa-circle m-l-5 text-info"></i>
                                    <?php }
                                else { ?>
                                        <i class="fa fa-circle m-l-5 text-white"></i>
                                    <?php } ?>
                            </td>
                            <td>
                                <span class="massage_number" data="<?php echo $row['id'];?>">
                                    <a href="#" data-toggle="modal" data-target="#mail-opening" class="email-name open_message"><?php echo $from_name;?></a>
                                </span>
                            </td>
                            <td>
                                <span class="massage_number" data="<?php echo $row['id'];?>">
                                    <a href="#" data-toggle="modal" data-target="#mail-opening" class="email-msg open_message"><?php echo $row['subject'];?></a>
                                </span>
                            </td>
                            <td style="width: 20px;"></td>
                            <td class="text-right"><?php echo $final;?></td>
                        </tr>
                    <?php
                endwhile;
                ?>
		        </tbody>
		    </table></div>
        <div class='row'>
            <div class='col-sm-12'>
                <div class='col-sm-7 col-md-offset-2 text-center'>
        <?php
        echo paginate_function($item_per_page, $page_number, $get_total_rows, $total_pages);
        ?>
            </div></div></div>
        <?php
    }
}

function get_all_notifications($userCcenter){
	global $dbc;
	date_default_timezone_set('Africa/Nairobi');
	$dates = date('Y-m-d');
	$q = "SELECT
	(SELECT COUNT(*) FROM BADGES WHERE expiry_date <= '$dates' AND costCenter = '$userCcenter') +
	(SELECT COUNT(*) FROM serviceInterval WHERE (serviceOdo - currentOdo) <= '100' AND costCenter = '$userCcenter') +
	(SELECT COUNT(*) FROM DRIVER WHERE licence_expirery <= '$dates' AND costCenter = '$userCcenter') as sum_centers";

	$r = mysqli_query($dbc, $q);
	while ($row = mysqli_fetch_assoc($r)):
		$count = $row['sum_centers'];
		if ($count >= 1) {
			?>
				<div data-target="#" class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="true">
		            <i class="icon-bell"></i> <span class="badge badge-xs badge-danger"><?php echo $count; ?></span>
		        </div>
		        <ul class="dropdown-menu dropdown-menu-lg">
		            <li class="notifi-title">
		                <span class="label label-default pull-right">New <?php echo $count; ?></span>Notification
		            </li>
		            <li class="list-group nicescroll notification-list">

		            	<?php
		            		$q = "SELECT * FROM BADGES WHERE expiry_date <= '$dates' AND costCenter = '$userCcenter'";
							$r = mysqli_query($dbc, $q);
							if ($r) {
								while ($row = mysqli_fetch_assoc($r)):
									?>
										<a href="javascript:void(0);" id="<?php echo $row['id']; ?>" class="list-group-item" data-toggle="modal" data-target="#badge-due-modal<?php echo $row['id']; ?>">
						                	<div class="media">
						                    	<div class="pull-left p-r-10">
							                        <em class="fa fa-automobile fa-1x text-primary"></em>
							                    </div>
						                    	<div class="media-body">
						                        	<h6 class="media-heading">Due <?php echo $row['badge_type']; ?> for <?php echo $row['badge_id']; ?></h6>
							                        <p class="m-0">
							                        <?php
					                                    date_default_timezone_set("Africa/Nairobi");
					                                    if ($row['expiry_date']) {
					                                        $emonth = strtotime($row['expiry_date']);
					                                        $efullMonth = date('d M Y', $emonth);
					                                    }
					                                ?>
							                            <small>Deadline is on <?php echo $efullMonth;  ?></small>
							                        </p>
						                    	</div>
						                	</div>
						                </a>
									<?php
								endwhile;
							}
		            	?>
		            	<?php
		            		$q = "SELECT s.id AS 'id',
					            s.vehicle AS 'vehicle',
					            s.currentOdo AS 'currentOdo',
					            s.serviceOdo AS 'serviceOdo',
					            s.dateAdded AS 'dateAdded',
					            s.costCenter AS 'costCenter',
					            v.id AS 'vid'
					            FROM serviceInterval AS s
					            JOIN VEHICLES AS v ON s.vehicle = v.vid AND s.costCenter = v.costcentre
					            WHERE (s.serviceOdo - s.currentOdo) <= '100' AND s.costCenter = '$userCcenter'";
							$r = mysqli_query($dbc, $q);
							if ($r) {
								while ($row = mysqli_fetch_assoc($r)):
									?>
										<a href="day/edit/<?php echo $row['vid']; ?>" class="list-group-item see_vehicle">
						                	<div class="media">
						                    	<div class="pull-left p-r-10">
							                        <em class="fa fa-cogs fa-1x text-success"></em>
							                    </div>
						                    	<div class="media-body">
						                        	<h6 class="media-heading">Due Major service for <?php echo $row['vehicle']; ?></h6>
							                        <p class="m-0">
							                            <small>Service Odo: <?php echo $row['serviceOdo']; ?></small>
							                        </p>
						                    	</div>
						                	</div>
						               </a>
									<?php
								endwhile;
							}
		            	?>
		            	<?php
		            		$q = "SELECT * FROM DRIVER WHERE licence_expirery <= '$dates' AND costCenter = '$userCcenter'";
							$r = mysqli_query($dbc, $q);
							if ($r) {
								while ($row = mysqli_fetch_assoc($r)):
									?>
										<a href="/drivers/edit/<?php echo $row['id']; ?>" class="list-group-item">
						                	<div class="media">
						                    	<div class="pull-left p-r-10">
							                        <em class="fa fa-user-secret fa-1x text-danger"></em>
							                    </div>
						                    	<div class="media-body">
						                        	<h6 class="media-heading">Due licence for <?php echo $row['first_name']; ?></h6>
							                        <p class="m-0">
							                        <?php
					                                    date_default_timezone_set("Africa/Nairobi");
					                                    if ($row['licence_expirery']) {
					                                        $month = strtotime($row['licence_expirery']);
					                                        $fullMonth = date('d M Y', $month);
					                                    }
					                                ?>
							                            <small>Deadline is on <?php echo $fullMonth; ?></small>
							                        </p>
						                    	</div>
						                	</div>
						               </a>
									<?php
								endwhile;
							}
		            	?>

		            </li>
		        </ul>
			<?php
		}
	endwhile;
}

function data_email_api($id){
	global $dbc;
	$q = "SELECT * FROM settings WHERE id = $id";
	$data = mysqli_query($dbc, $q);
	return $data;
}

function data_setting_value($dbc, $id){
	global $dbc;
	$q = "SELECT * FROM settings WHERE id = '$id'";
	$r = mysqli_query($dbc, $q);
	$data = mysqli_fetch_assoc($r);
	return $data['value'];
}

function resize($width, $height, $fname, $lnane, $username, $type_id, $status, $email, $password){
	global $dbc;
	$file_name = $_FILES['image']['name'];
	$file_extn = strtolower(end(explode('.', $file_name)));
	$file_temp = $_FILES['image']['tmp_name'];
	/* Get original image x y*/
	list($w, $h) = getimagesize($_FILES['image']['tmp_name']);
	/* calculate new image size with ratio */
	$ratio = max($width/$w, $height/$h);
	$h = ceil($height / $ratio);
	$x = ($w - $width / $ratio) / 2;
	$w = ceil($width / $ratio);
	/* new file name */
	$path = '../images/users/';
	$actual_image_name = substr(md5(time()), 0, 10) . '.' . $file_extn;
	/* read binary data from image file */
	$imgString = file_get_contents($_FILES['image']['tmp_name']);
	/* create image from string */
	$image = imagecreatefromstring($imgString);
	$tmp = imagecreatetruecolor($width, $height);
	imagecopyresampled($tmp, $image,
		0, 0,
		$x, 0,
		$width, $height,
		$w, $h);
	// $q = 'INSERT INTO users (avatar, firstname, lastname, email, password, username, type_id, status) VALUES ("'.$actual_image_name.'", "'.$fname.'", "'.$lnane.'", "'.$email.'", SHA1("'.$password.'"), "'.$username.'", "'.$type_id.'", "'.$status.'")';

		$r = mysqli_query($dbc, $q);

		if ($r) {
			/* Save image */
			switch ($_FILES['image']['type']) {
			case 'image/jpeg':
				imagejpeg($tmp, $path.$actual_image_name, 100);
				break;
			case 'image/jpg':
				imagejpeg($tmp, $path.$actual_image_name, 100);
				break;
			case 'image/png':
				imagepng($tmp, $path.$actual_image_name, 0);
				break;
			case 'image/gif':
				imagegif($tmp, $path.$actual_image_name);
				break;
			default:
				exit;
				break;
			}
			/* cleanup memory */
			imagedestroy($image);
			imagedestroy($tmp);
			return true;
		}else{
			return false;
		}
}
function update_user_data($fname, $lnane, $username, $type_id, $status, $email, $avatar, $rec_id, $costCenter){
	global $dbc;
	if ($type_id == 'Webmaster') {
		$type_data = 1;
	}elseif ($type_id == 'Administrator'){
		$type_data = 2;
	}elseif ($type_id == 'Power User'){
		$type_data = 3;
	}

	if ($status == 'Active') {
		$status_data = 1;
	}elseif ($status == 'Inactive'){
		$status_data = 2;
	}

	$q = "UPDATE users SET firstname = '$fname', lastname = '$lnane', username = '$username', email = '$email', type_id = '$type_data', status = '$status_data', center = '$costCenter' WHERE id = '$rec_id' ";
	$r = mysqli_query($dbc, $q);
	if ($r) {
		return true;
	}else {
		return false;
	}
}


function update_image($new_image,$userid){
	global $dbc;
	$q = "UPDATE users SET avatar = '$new_image' WHERE id = '$userid' ";
	$r = mysqli_query($dbc, $q);
	if ($r) {
		return true;
	}else {
		return false;
	}
}


function updateusersdata($width, $height, $fname, $lnane, $username, $type_id, $status, $email, $avatar, $rec_id, $costCenter){
	global $dbc;
	$file_name = $_FILES['image']['name'];
	$file_extn = strtolower(end(explode('.', $file_name)));
	$file_temp = $_FILES['image']['tmp_name'];
	/* Get original image x y*/
	list($w, $h) = getimagesize($_FILES['image']['tmp_name']);
	/* calculate new image size with ratio */
	$ratio = max($width/$w, $height/$h);
	$h = ceil($height / $ratio);
	$x = ($w - $width / $ratio) / 2;
	$w = ceil($width / $ratio);
	/* new file name */
	$path = '../images/users/';
	$actual_image_name = substr(md5(time()), 0, 10) . '.' . $file_extn;
	/* read binary data from image file */
	$imgString = file_get_contents($_FILES['image']['tmp_name']);
	/* create image from string */
	$image = imagecreatefromstring($imgString);
	$tmp = imagecreatetruecolor($width, $height);
	imagecopyresampled($tmp, $image,
		0, 0,
		$x, 0,
		$width, $height,
		$w, $h);

	if ($type_id == 'Webmaster') {
		$type_data = 1;
	}elseif ($type_id == 'Administrator'){
		$type_data = 2;
	}elseif ($type_id == 'Power User'){
		$type_data = 3;
	}

	if ($status == 'Active') {
		$status_data = 1;
	}elseif ($status == 'Inactive'){
		$status_data = 2;
	}
	// if image isset
	$remover = unlink($path.$avatar);
	$q = "UPDATE users SET avatar = '$actual_image_name', firstname = '$fname', lastname = '$lnane', username = '$username', email = '$email', type_id = '$type_data', status = '$status_data', center = '$costCenter' WHERE id = '$rec_id' ";
	$r = mysqli_query($dbc, $q);

	if ($r) {
		/* Save image */
		switch ($_FILES['image']['type']) {
			case 'image/jpeg':
				imagejpeg($tmp, $path.$actual_image_name, 100);
				break;
			case 'image/jpg':
				imagejpeg($tmp, $path.$actual_image_name, 100);
				break;
			case 'image/png':
				imagepng($tmp, $path.$actual_image_name, 0);
				break;
			case 'image/gif':
				imagegif($tmp, $path.$actual_image_name);
				break;
			default:
				exit;
				break;
		}
		/* cleanup memory */
		imagedestroy($image);
		imagedestroy($tmp);
		return true;
	}else{
		return false;
	}
}

function email_exists($email){
	global $dbc;
	$email = sanitize($email);
	$q = "SELECT * FROM users WHERE email = '$email'";
	$r = mysqli_query($dbc, $q);
	$query_num_rows = mysqli_num_rows($r);
	if($query_num_rows == 1){
		return true;
	}
	elseif($query_num_rows == 0){
		return false;
	}
}

function recover($user_id, $new_password){
	global $dbc;
	$q = "UPDATE users SET password = SHA1('$new_password'), password_recover = 1 WHERE id = '$user_id'";
	$r = mysqli_query($dbc, $q);
	if ($r) {
        return true;
    }else{
        return false;
    }
}


// sending mail for password reset function
function send_email($subject, $email, $message){
	$subject = sanitize($subject);
	$email = sanitize($email);
	$message = sanitize($message);

	$mail = new PHPMailer;
	$mail->IsSMTP();                                      // Set mailer to use SMTP
	$mail->Host = 'smtp.gmail.com';  						// Specify main and backup server
	$mail->Port = 465;                    				// set the SMTP port for the GMAIL server
	$mail->SMTPAuth = true;                               // Enable SMTP authentication
	$mail->SMTPKeepAlive = true;                  		// SMTP connection will not close after each email sent
	$mail->Username = 'jhnwanyoike@gmail.com';                            // SMTP username
	$mail->Password = '123456';                           // SMTP password
	$mail->SMTPSecure = 'ssl';                            // Enable encryption, 'ssl' also accepted
	$mail->IsHTML(true);

	// HTML body
	$body .= $message."<br>";
	$body .= "Sincerely, <br>";
	$body .= "Callback System Admin";

	$mail->Subject = $subject;
	$mail->Body    = $body;
	$mail->AltBody = $body;
	$mail->From = "jhnwanyoike@gmail.com";					//from email
	$mail->FromName = 'Callback System password reset.';
	$mail->AddAddress('jhnwanyoike@gmail.com', 'Developer Kenya');  // Add a recipient
	$mail->AddReplyTo('jhnwanyoike@gmail.com', 'Developer');

	if(!$mail->Send()) {
		return $e->errorMessage();
		exit;
	}else{
		return true;
	}
	// Clear all addresses and attachments for next loop
	$mail->ClearAddresses();
	$mail->ClearAttachments();
}

function change_password($user_id, $password){
	global $dbc;
	$user_id = (int)$user_id;
	$password = SHA1($password);
	$q = "UPDATE users SET password = '$password', password_recover = 0 WHERE id= $user_id";
	$r = mysqli_query($dbc, $q);
	if ($r) {
		return true;
	}else{
		return false;
	}
}


function data_user_activity($id){
	global $dbc;
	$q = "SELECT * FROM audit_Log WHERE user_id = $id";
	$data = mysqli_query($dbc, $q);
	return $data;
}

function lock_checker($uid){
	global $dbc;
	$q = "SELECT * FROM ACTIVE WHERE user_id = '$uid' AND user_status = 0";
	$r = mysqli_query($dbc, $q);
	$get_rows = mysqli_num_rows($r);
	if ($get_rows >= 1) {
		return true;
	}
}

function login_checker($uid){
	global $dbc;
	$q = "SELECT * FROM ACTIVE WHERE user_id = '$uid'";
	$r = mysqli_query($dbc, $q);
	$get_rows = mysqli_num_rows($r);
	if ($get_rows >= 1) {
		return true;
	}
}

function getLastLoginTime($user_id, $date_logg){
	global $dbc;
	date_default_timezone_set("Africa/Nairobi");
	$login_time = strtotime($date_logg);
	$nowdate = date('Y-m-d H:i:s');
	$now = strtotime($nowdate);
	$results = $now - $login_time;
	if ($results >= 28800) {
		$q = "DELETE FROM ACTIVE WHERE user_id = '$user_id'";
		$r = mysqli_query($dbc, $q);
		if ($r) {
			$date = date('Y-m-d H:i:s');
		    $sql = "INSERT INTO audit_log (user_id, type, action, ip, date_added) VALUES ($user, 'System Event', 'System log out', '$_SERVER[REMOTE_ADDR]', '$date')";
		    $rslt = mysqli_query($dbc, $sql);
		}
	}
}

function session_timer($user){
	date_default_timezone_set("Africa/Nairobi");
	global $dbc;
	$q = "SELECT user_id FROM ACTIVE";
	$r = mysqli_query($dbc, $q);
	while ($row = mysqli_fetch_assoc($r)):
		$user_id = $row['user_id'];
		$sql = "SELECT * FROM audit_log WHERE user_id = $user_id AND action = 'Logged In' ORDER BY id DESC LIMIT 1";
		$resul = mysqli_query($dbc, $sql);
		while ($obj = mysqli_fetch_assoc($resul)):
			$date_logg = $obj['date_added'];
			$user_id = $obj['user_id'];
			echo $date_logg;
			getLastLoginTime($user_id, $date_logg);
		endwhile;
	endwhile;
}

function data_log_system_event($user_id, $event, $code){
	date_default_timezone_set("Africa/Nairobi");
    global $dbc;
    $date = date('Y-m-d H:i:s');
    $q = "INSERT INTO audit_log (user_id, type, action, Code, ip, date_added) VALUES ($user_id, 'System Event', '$event', '$code', '$_SERVER[REMOTE_ADDR]', '$date')";
    $r = mysqli_query($dbc, $q);
    if ($r) {
    	if (active_state($user_id, $event) == true) {
    		header('Location: /start');
    	}
    }
    else {
    	$reply = '<span style="margin: 0 auto !important;" type="button" class="btn btn-danger waves-effect waves-light">Please try again.</span>';
    }
}

function active_state($user_id, $event){
	global $dbc;
	if ($event == "Logged In") {
		$q = "DELETE FROM ACTIVE WHERE user_id = '$user_id'";
		$r = mysqli_query($dbc, $q);
		if ($r) {
			$qr = "INSERT INTO ACTIVE (user_id) VALUES ($user_id)";
			$rs = mysqli_query($dbc, $qr);
			if ($rs) {
				return true;
			}
		}
	}
	if ($event == "Logged Out") {
		$q = "DELETE FROM ACTIVE WHERE user_id = '$user_id'";
		$r = mysqli_query($dbc, $q);
		if ($r) {
			return true;
		}
	}
}

function audit_log_event($user_id, $type, $action, $LogCode) {
	global $dbc;
	date_default_timezone_set("Africa/Nairobi");
    $date = date('Y-m-d H:i:s');
    $q = "INSERT INTO audit_log (user_id, type, action, Code, ip, date_added) VALUES ($user_id, '$type', '$action', '$LogCode', '$_SERVER[REMOTE_ADDR]', '$date')";
    $r = mysqli_query($dbc, $q);
}

function get_user_Loged_in($id){
	global $dbc;
	$q = "SELECT * FROM users WHERE id = '$id'";
	$data = mysqli_query($dbc, $q);
	return $data;
}


function Online_USers_list($uid){
	global $dbc;
	$q = "SELECT * FROM ACTIVE WHERE user_id != $uid ORDER BY id DESC";
	$r = mysqli_query($dbc, $q);
	if ($r) {
		while ($obj = mysqli_fetch_assoc($r)):
			$status = $obj['user_status'];
        	$rows = get_user_Loged_in($obj['user_id']);
            while( $row = mysqli_fetch_assoc($rows) ):
            	?>
				<li id="<?php echo $row['id']; ?>" data="<?php echo $row['firstname']." ".$row['lastname']; ?>" class="list-group-item send_onlineuser_mail" data-toggle="modal" data-target="#new_mail_online_user">
	                <a>
	                <?php
						if ($status == 1) {
	                		?><i class="fa fa-circle online"></i><?php
	                	}
	                	if ($status == 0) {
	                		?><i class="fa fa-circle away"></i><?php
	                	}
					?>
					<div class="avatar">
			            <img src="../images/users/<?php echo $row['avatar']; ?>" alt="">
			        </div>
			        <span class="name"><?php echo $row['firstname']." ".$row['lastname']; ?></span>
	                </a>
	                <span class="clearfix"></span>
	            </li>
				<?php
			endwhile;
		endwhile;
	}
}





function time_elapsed($date) {

	$timeAgo = new TimeAgo();

	return $timeAgo->inWords($date);

}

function data_userExists($email, $password){
	global $dbc;
	$q = "SELECT * FROM users WHERE email = '$email' AND password = SHA1('$password')";
	$r = mysqli_query($dbc, $q);
	if(mysqli_num_rows($r) == 1){
		return true;
	}else{
		return false;
	}
}

function checkifcontacted($vehicle, $center){
	global $dbc;
	$q = "SELECT * FROM Contracted WHERE vehicle = '$vehicle' AND costCenter = '$center'";
	$r = mysqli_query($dbc, $q);
	if(mysqli_num_rows($r) == 1){
		return true;
	}else{
		return false;
	}
}

function getjobMechanics($center, $jobCard){
	global $dbc;
	$q = "SELECT w.id AS 'id',
			w.jobCardNo AS 'jobCardNo',
            w.staffNo AS 'staffNo',
            w.WorkHours AS 'WorkHours',
            w.DateOfWork AS 'DateOfWork',
            w.WorkDone AS 'WorkDone',
            w.vehicle AS 'vehicle',
            w.added_by AS 'added_by',
            w.costCenter AS 'costCenter',
            m.fname AS 'fname',
            m.lname AS 'lname'
            FROM MechWokHours AS w
            JOIN Mechanics AS m ON w.staffNo = m.staffNumber
            WHERE w.jobCardNo = '$jobCard' AND w.costCenter = '$center'";
	$r = mysqli_query($dbc, $q);
	?>
	<div class="col-md-12">
	<div class="john-respond john-shadow">
		<table class="table table-hover table-mc-light-blue">
		    <thead style="background-color: #209e91;">
		        <tr>
		          <th>Staff No.</th>
		          <th>Staff Name</th>
		          <th>Hours worked</th>
		          <th>Date worked</th>
		          <th>Work Done</th>
		          <th class="text-right">Action</th>
		        </tr>
		    </thead>
		    <tbody class="mechJobs">
				<?php
				while( $row = mysqli_fetch_assoc( $r )) {
					date_default_timezone_set("Africa/Nairobi");
					$staffNo = $row['staffNo'];
					$fname = $row['fname'];
					$lname = $row['lname'];
					$WorkHours = $row['WorkHours'];
					$DateOfWork = $row['DateOfWork'];
					$WorkDone = $row['WorkDone'];

					$dateworked = strtotime($DateOfWork);
					$dateworked1 = date('d M Y', $dateworked);

			        ?>
			          <tr>
			          <td data-title="Staff No."><?php echo $staffNo; ?></td>
			          <td data-title="Staff Name"><?php echo $fname.' '.$lname; ?></td>
			          <td data-title="Hours worked"><?php echo $WorkHours; ?></td>
			          <td data-title="Date worked"><?php echo $dateworked1; ?></td>
			          <td data-title="Work Done"><?php echo $WorkDone; ?></td>
			          <td data-title="Action" class="text-right mechAction" data="<?php echo $row['id']; ?>">
			          	<div class="btn btn-danger waves-effect waves-light btn-xs deleteWork"><i class="fa fa-times"></i></div>
			          </td>
			        </tr>
		        <?php
			    }
			    ?>
		    </tbody>
	    </table>
	</div>
	</div>
    <?php
}


function getAccThirdParty($userCenter, $acc_ref){
	global $dbc;
	$q = "SELECT w.id AS 'id',
			w.type AS 'type',
            w.Vhl_Description AS 'Vhl_Description',
            w.Name AS 'Name',
            w.Address AS 'Address',
            w.Remarks AS 'Remarks',
            w.added_by AS 'added_by',
            w.CostCenter AS 'CostCenter'
            FROM Acc_Third_Party AS w
            WHERE w.reff_number = '$acc_ref' AND w.CostCenter = '$userCenter'";
	$r = mysqli_query($dbc, $q);
	?>
	<div class="col-md-12">
	<div class="john-respond john-shadow">
		<table class="table table-hover table-mc-light-blue">
		    <thead style="background-color: #209e91;">
		        <tr>
		          <th>Type</th>
		          <th>Vehicle Desc</th>
		          <th>Name</th>
		          <th>Address</th>
		          <th>Remarks</th>
		          <th class="text-right">Action</th>
		        </tr>
		    </thead>
		    <tbody class="mechJobs">
				<?php
				while( $row = mysqli_fetch_assoc( $r )) {
					date_default_timezone_set("Africa/Nairobi");
					$type = $row['type'];
					$Vhl_Description = $row['Vhl_Description'];
					$Name = $row['Name'];
					$Address = $row['Address'];
					$Remarks = $row['Remarks'];

			        ?>
			          <tr>
			          <td data-title="Type"><?php echo $type; ?></td>
			          <td data-title="Vehicle Desc"><?php echo $Vhl_Description; ?></td>
			          <td data-title="Name"><?php echo $Name; ?></td>
			          <td data-title="Address"><?php echo $Address; ?></td>
			          <td data-title="Remarks"><?php echo $Remarks; ?></td>
			          <td data-title="Action" class="text-right ThirdDetaild" type="<?php echo $Name;?>" data="<?php echo $row['id']; ?>">
			          	<div class="btn btn-danger waves-effect waves-light btn-xs deleteThird"><i class="fa fa-times"></i></div>
			          </td>
			        </tr>
		        <?php
			    }
			    ?>
		    </tbody>
	    </table>
	</div>
	</div>
    <?php
}


function getvhlbadge($bid, $center){
	global $dbc;
	$q = "SELECT * FROM BADGES WHERE badge_id = '$bid' AND costCenter = '$center'";
	$r = mysqli_query($dbc, $q);
	?>
	<div class="col-md-12">
	<div class="john-respond john-shadow">
		<table class="table table-hover table-mc-light-blue">
		    <thead style="background-color: #209e91;">
		        <tr>
		          <th>Badge type</th>
		          <th>Badge ID</th>
		          <th>Issue date</th>
		          <th>Expiry date</th>
		          <th>Remarks</th>
		          <th>Status</th>
		          <th>Action</th>
		        </tr>
		    </thead>
		    <tbody>
				<?php
				while( $row = mysqli_fetch_assoc( $r )) {
					date_default_timezone_set("Africa/Nairobi");
					$is_date = $row['issue_date'];
					$ex_date = $row['expiry_date'];

					$issue_date = strtotime($is_date);
					$isdate = date('d M Y', $issue_date);
					$isdate1 = date('d-m-Y', $issue_date);

					$expiry_date = strtotime($ex_date);
					$exdate = date('d M Y', $expiry_date);
					$exdate1 = date('d-m-Y', $expiry_date);
			        ?>
		        	<tr>
						<td data-title="Badge type"><?php echo $row['badge_type']; ?></td>
						<td data-title="Badge ID"><?php echo $row['badge_id']; ?></td>
						<td data-title="Issue date"><?php echo $isdate; ?></td>
						<td data-title="Expiry date"><?php echo $exdate; ?></td>
						<td data-title="Remarks"><?php echo $row['remarks']; ?></td>
						<td data-title="Status"><?php echo $row['status']; ?></td>
						<td data-title="Action" class="vehicleUpdate" data="<?php echo $row['id']; ?>">
						<button class="btn btn-success waves-effect waves-light btn-xs editbadge" data-toggle="modal" data-target="#panel-modal-<?php echo $row['id']; ?>"><i class="fa fa-pencil"></i></button>
							<button class="btn btn-danger waves-effect waves-light btn-xs deletebadge"><i class="fa fa-times"></i></button>
						</td>
			        <div id="panel-modal-<?php echo $row['id']; ?>" class="modal fade in" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none; padding-left: 0px;">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close re_<?php echo $row['id'];?>" id="close_<?php echo $row['id'];?>" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h4 class="modal-title">Edit selected badge</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="field-1" class="control-label">Badge type</label>
                                                <input autocomplete="off" class="form-control btype_<?php echo $row['id'];?>" type="text" value="<?php echo $row['badge_type'];?>">
					                            <input type="hidden" class="budgeid_<?php echo $row['id'];?>" value="<?php echo $row['id'];?>">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="field-2" class="control-label">Badge id</label>
                                                <input autocomplete="off" class="form-control bid_<?php echo $row['id'];?>" type="text" value="<?php echo $row['badge_id'];?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="field-3" class="control-label">Issue date</label>
                                                <input autocomplete="off" id="datepicker6" class="form-control bidate_<?php echo $row['id'];?>" type="text" value="<?php echo $isdate1;?>">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="field-4" class="control-label">Expirery date</label>
                                                <input autocomplete="off" id="datepicker6" class="form-control bedate_<?php echo $row['id'];?>" type="text" value="<?php echo $exdate1;?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="field-5" class="control-label">Remarks</label>
                                                <input autocomplete="off" class="form-control bremarks_<?php echo $row['id'];?>" type="text" value="<?php echo $row['remarks'];?>">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="field-6" class="control-label">Status</label>
                                                <input autocomplete="off" class="form-control bstatus_<?php echo $row['id'];?>" type="text" value="<?php echo $row['status'];?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer pickedBadge" data="<?php echo $row['id'];?>">
                                    <button type="button" class="btn btn-danger waves-effect rhynixJohn_<?php echo $row['id'];?>" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-success waves-effect waves-light saveEditBadge">Save changes</button>
                                </div>
                            </div>
                        </div>
                    </div>
			        </tr>
		        <?php
			    }
			    ?>
		    </tbody>
	    </table>
	</div>
	</div>
    <?php
}

function getvhlFile($bid, $center){
	global $dbc;
	$q = "SELECT * FROM FILES WHERE vehivle_id = '$bid' AND costCenter = '$center'";
	$r = mysqli_query($dbc, $q);
	?>
	<div class="col-md-12">
		<div class="john-respond john-shadow">
			<table class="table table-hover table-mc-light-blue">
			    <thead style="background-color: #209e91;">
			        <tr>
			          <th>UPLOADED FILES</th>
			          <th>FILE ACTION</th>
			        </tr>
			    </thead>
			    <tbody>
					<?php
					while( $row = mysqli_fetch_assoc( $r )) {
						?>
				        	<tr>
					          <td data-title="UPLOADED FILES"><?php echo $row['file_name']; ?></td>
					          <td data-title="FILE ACTION" class="filedetal" data = "<?php echo $row['id']; ?>" type = "<?php echo $row['file_name']; ?>">
					          	<a href="/files/<?php echo $row['file_name']; ?>" target="_blank" class="btn btn-success waves-effect waves-light btn-xs"><i class="fa fa-download"></i></a>
						        <button class="btn btn-danger waves-effect waves-light btn-xs deleteFile"><i class="fa fa-times"></i></button>
					          </td>
					        </tr>
				        <?php
					    }
					    ?>
				    </tbody>
			    </table>
			</div>
		</div>
	<?php
}

function data_vhl($id) {
	global $dbc;
	if(is_numeric($id)) {
		$cond = "WHERE id = '$id'";
	}
	$q = "SELECT * FROM VEHICLES $cond";
	$r = mysqli_query($dbc, $q);
	$data = mysqli_fetch_assoc($r);
	return $data;
}

function data_issuance($id) {
	global $dbc;
	if(is_numeric($id)) {
		$cond = "WHERE id = '$id'";
	}
	$q = "SELECT * FROM ISSUANCE $cond";
	$r = mysqli_query($dbc, $q);
	$data = mysqli_fetch_assoc($r);
	return $data;
}

function data_driver($id) {
	global $dbc;
	if(is_numeric($id)) {
		$cond = "WHERE id = '$id'";
	}
	$q = "SELECT * FROM DRIVER $cond";
	$r = mysqli_query($dbc, $q);
	$data = mysqli_fetch_assoc($r);
	return $data;
}

function data_accident($id) {
	global $dbc;
	if(is_numeric($id)) {
		$cond = "WHERE id = '$id'";
	}
	$q = "SELECT * FROM Accidents $cond";
	$r = mysqli_query($dbc, $q);
	$data = mysqli_fetch_assoc($r);
	return $data;
}

function data_violation($id) {
	global $dbc;
	if(is_numeric($id)) {
		$cond = "WHERE id = '$id'";
	}
	$q = "SELECT * FROM INCIDENTS $cond";
	$r = mysqli_query($dbc, $q);
	$data = mysqli_fetch_assoc($r);
	return $data;
}

function data_jobcard($id) {
	global $dbc;
	if(is_numeric($id)) {
		$cond = "WHERE h.id = '$id'";
	}

	$q = "SELECT h.id AS 'id',
            h.JobNumber AS 'JobNumber',
            h.VID AS 'VID',
            h.VehicleType AS 'VehicleType',
            h.Model AS 'Model',
            h.JobStartDate AS 'JobStartDate',
            h.JobStartTime AS 'JobStartTime',
            h.CurrentOdo AS 'CurrentOdo',
            h.ServiceDecription AS 'ServiceDecription',
            h.JobStatus AS 'JobStatus',
            h.ChasisNumber AS 'ChasisNumber',
            h.ServiceType AS 'ServiceType',
            h.DateCompleted AS 'DateCompleted',
            h.TimeCompleted AS 'TimeCompleted',
            h.NextServiceDate AS 'NextServiceDate',
            h.NewKilometer AS 'NewKilometer',
            h.NextServiceKMs AS 'NextServiceKMs',
            h.Remarks AS 'Remarks',
            h.jobFile AS 'jobFile',
            h.CostCenter AS 'CostCenter',
            h.rootCourse AS 'rootCourse',
            h.added_by AS 'added_by',
            COUNT(d.id) AS 'detailsCount',
            d.JobNumber AS 'JobNumber1'
            FROM jobcardhead AS h
            JOIN JobDetail AS d ON h.JobNumber = d.JobNumber AND h.CostCenter = d.CostCenter
            $cond";
	$r = mysqli_query($dbc, $q);
	$data = mysqli_fetch_assoc($r);
	return $data;
}

function data_workticket($id) {
	global $dbc;
	if(is_numeric($id)) {
		$cond = "WHERE h.id = '$id'";
	}
	$q = "SELECT h.id AS 'id',
            h.worknumber AS 'worknumber',
            h.vehicle AS 'vehicle',
            h.make AS 'make',
            h.deparment AS 'deparment',
            h.base AS 'base',
            h.Startodo AS 'Startodo',
            h.closeOdo AS 'closeOdo',
            h.workstatus AS 'workstatus',
            h.dateOpened AS 'dateOpened',
            h.added_by AS 'added_by',
            h.costCenter AS 'costCenter',
            v.odometer AS 'odometer'
            FROM Workheader AS h
            JOIN VEHICLES AS v ON h.vehicle = v.vid
            $cond";

	// $q = "SELECT * FROM Workheader $cond";
	$r = mysqli_query($dbc, $q);
	$data = mysqli_fetch_assoc($r);
	return $data;
}


function data_costing($id) {
	global $dbc;
	if(is_numeric($id)) {
		$cond = "WHERE id = '$id'";
	}
	$q = "SELECT * FROM COSTING $cond";
	$r = mysqli_query($dbc, $q);
	$data = mysqli_fetch_assoc($r);
	return $data;
}

function data_distance($id) {
	global $dbc;
	if(is_numeric($id)) {
		$cond = "WHERE id = '$id'";
	}
	$q = "SELECT * FROM Distance_Covered $cond";
	$r = mysqli_query($dbc, $q);
	$data = mysqli_fetch_assoc($r);
	return $data;
}

function data_Location($id) {
	global $dbc;
	if(is_numeric($id)) {
		$cond = "WHERE id = '$id'";
	}
	$q = "SELECT * FROM LOCATION $cond";
	$r = mysqli_query($dbc, $q);
	$data = mysqli_fetch_assoc($r);
	return $data;
}

function data_base($id) {
	global $dbc;
	if(is_numeric($id)) {
		$cond = "WHERE id = '$id'";
	}
	$q = "SELECT * FROM BASE $cond";
	$r = mysqli_query($dbc, $q);
	$data = mysqli_fetch_assoc($r);
	return $data;
}


function data_contracted($id) {
	global $dbc;
	if(is_numeric($id)) {
		$cond = "WHERE id = '$id'";
	}
	$q = "SELECT * FROM Contracted $cond";
	$r = mysqli_query($dbc, $q);
	$data = mysqli_fetch_assoc($r);
	return $data;
}

function data_claim($id) {
	global $dbc;
	if(is_numeric($id)) {
		$cond = "WHERE id = '$id'";
	}
	$q = "SELECT * FROM Claime $cond";
	$r = mysqli_query($dbc, $q);
	$data = mysqli_fetch_assoc($r);
	return $data;
}

function data_rental($id) {
	global $dbc;
	if(is_numeric($id)) {
		$cond = "WHERE id = '$id'";
	}
	$q = "SELECT * FROM Rentals $cond";
	$r = mysqli_query($dbc, $q);
	$data = mysqli_fetch_assoc($r);
	return $data;
}

function data_returns($id) {
	global $dbc;
	if(is_numeric($id)) {
		$cond = "WHERE id = '$id'";
	}
	$q = "SELECT * FROM Returns $cond";
	$r = mysqli_query($dbc, $q);
	$data = mysqli_fetch_assoc($r);
	return $data;
}

function data_stock($id) {
	global $dbc;
	if(is_numeric($id)) {
		$cond = "WHERE id = '$id'";
	}
	$q = "SELECT * FROM StockHeader $cond";
	$r = mysqli_query($dbc, $q);
	$data = mysqli_fetch_assoc($r);
	return $data;
}

function data_Order($id) {
	global $dbc;
	if(is_numeric($id)) {
		$cond = "WHERE id = '$id'";
	}
	$q = "SELECT * FROM OrderHeader $cond";
	$r = mysqli_query($dbc, $q);
	$data = mysqli_fetch_assoc($r);
	return $data;
}

function data_Mechanics($id) {
	global $dbc;
	if(is_numeric($id)) {
		$cond = "WHERE id = '$id'";
	}
	$q = "SELECT * FROM Mechanics $cond";
	$r = mysqli_query($dbc, $q);
	$data = mysqli_fetch_assoc($r);
	return $data;
}

function data_DoneTasks($id) {
	global $dbc;
	if(is_numeric($id)) {
		$cond = "WHERE id = '$id'";
	}
	$q = "SELECT * FROM Tasks $cond";
	$r = mysqli_query($dbc, $q);
	$data = mysqli_fetch_assoc($r);
	return $data;
}

function data_Tanks($id) {
	global $dbc;
	if(is_numeric($id)) {
		$cond = "WHERE id = '$id'";
	}
	$q = "SELECT * FROM Tank_Header $cond";
	$r = mysqli_query($dbc, $q);
	$data = mysqli_fetch_assoc($r);
	return $data;
}

function userAnalysis($id) {
	global $dbc;
	if(is_numeric($id)) {
		$cond = "WHERE id = '$id'";
	}
	$q = "SELECT * FROM users $cond";
	$r = mysqli_query($dbc, $q);
	$data = mysqli_fetch_assoc($r);
	return $data;
}

function data_user($id) {

	global $dbc;

	if(is_numeric($id)) {
		$cond = "WHERE id = '$id'";
	} else {
		$cond = "WHERE email = '$id'";
	}

	$q = "SELECT * FROM users $cond";
	$r = mysqli_query($dbc, $q);

	$data = mysqli_fetch_assoc($r);

	$data['type'] = data_user_type($dbc, $data['type_id']);

	$data['fullname'] = $data['firstname'].' '.$data['lastname'];
	$data['fullname_reverse'] = $data['lastname'].', '.$data['firstname'];

	return $data;
}

function data_user_type($dbc, $id) {
	$q = "SELECT * FROM user_types WHERE id = '$id'";
	$r = mysqli_query($dbc, $q);
	$data = mysqli_fetch_assoc($r);

	return $data;
}
function get_user_data($new_email){
	global $dbc;
	$q = "SELECT * FROM users WHERE email = '$new_email'";
	$data = mysqli_query($dbc, $q);
	return $data;
}

function get_last_session($session_email){
	// add $dbc connection
	global $dbc;
	$q = "SELECT * FROM login_report WHERE email = '$session_email' ORDER BY id DESC LIMIT 1";
	$data = mysqli_query($dbc, $q);
	return $data;
}

function sanitize($data){

	global $dbc;

	$data = mysqli_real_escape_string($dbc, trim($data));

	return $data;
}

function work_shop($userCenter){
	global $dbc;
	$date = date('Y-m-d');
	$q = "SELECT * FROM serviceType";
	if(($r = mysqli_query($dbc, $q)) !== false) {
		?>
    	<div class="col-md-12 col-lg-12">
		    <div class="widget-bg-color-icon card-box zoomIn animated">
		    <h4 class="text-dark header-title m-t-0">Workshop status</h4>
		    <div class="row">
    	<?php
		while( $obj = mysqli_fetch_assoc( $r )) {
			$code = $obj['code'];
			$sql = "SELECT COUNT(j.id) AS 'servicecount',
            j.ServiceType AS 'sType',
            s.code AS 'servicecode',
            s.description AS 'desc'
            FROM jobcardhead AS j
            JOIN serviceType AS s ON j.ServiceType = s.code
            WHERE ServiceType ='$code' AND JobStatus !='Closed' AND CostCenter = '$userCenter'";

			if(($rst = mysqli_query($dbc, $sql)) !== false) {
				while( $row = mysqli_fetch_assoc( $rst )) {
					$count = $row['servicecount'];
					$desc = $row['desc'];
					if ($count > 0) {
						?>
							<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 m-b-15">
								<div class="col-md-7 col-sm-8 col-xs-8 col-md-offset-1 text-left" style="padding: 5px 0px 0px 0px;"><?php echo $desc;?></div>
				                <div class="col-md-3 col-sm-4 col-xs-4 text-right">
				                	<div class="number-budge bg-info"><?php echo $count; ?></div>
				                </div>
							</div>
						<?php
					}
				}
			}
        }
    }

    $sql2 = "SELECT COUNT(VID) as cars FROM jobcardhead WHERE JobStatus !='Closed' AND CostCenter = '$userCenter' GROUP BY VID";
    if(($rst2 = mysqli_query($dbc, $sql2)) !== false) {
    	$count = mysqli_num_rows($rst2);
    }
    ?>
    	<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 m-b-15">
			<div class="col-md-7 col-sm-8 col-xs-8 col-md-offset-1 text-left" style="padding: 5px 0px 0px 0px;">Unavailable Fleet</div>
            <div class="col-md-3 col-sm-4 col-xs-4 text-right">
            	<div class="number-budge bg-danger"><?php echo $count; ?></div>
            </div>
		</div>
	<?php

    $sql1 = "SELECT
		(SELECT COUNT(*) FROM jobcardhead WHERE JobStatus !='Closed' AND CostCenter = '$userCenter') as notClosed,
    	(SELECT COUNT(*) FROM VEHICLES WHERE id !='' AND costcentre = '$userCenter') as vehicles,
    	(SELECT COUNT(*) FROM VEHICLES WHERE id !='' AND status = 0 AND costcentre = '$userCenter') as OfflineVehicles";
    if(($rst = mysqli_query($dbc, $sql1)) !== false) {
    	while( $obj = mysqli_fetch_assoc( $rst )) {
    		$OfflineVehicles = $obj['OfflineVehicles'];
    		$vehicl = $obj['vehicles'];
    		$totalvehicles = $vehicl - $count - $OfflineVehicles;
    		?>
    			<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 m-b-15">
					<div class="col-md-7 col-sm-8 col-xs-8 col-md-offset-1 text-left" style="padding: 5px 0px 0px 0px;">Open Jobs</div>
		            <div class="col-md-3 col-sm-4 col-xs-4 text-right">
		            	<div class="number-budge bg-danger"><?php echo $obj['notClosed']; ?></div>
		            </div>
				</div>

				<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 m-b-15">
					<div class="col-md-7 col-sm-8 col-xs-8 col-md-offset-1 text-left" style="padding: 5px 0px 0px 0px;">Grounded Vehicles</div>
		            <div class="col-md-3 col-sm-4 col-xs-4 text-right">
		            	<div class="number-budge bg-danger"><?php echo $OfflineVehicles; ?></div>
		            </div>
				</div>

				<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 m-b-15">
					<div class="col-md-7 col-sm-8 col-xs-8 col-md-offset-1 text-left" style="padding: 5px 0px 0px 0px;">Available Fleet</div>
		            <div class="col-md-3 col-sm-4 col-xs-4 text-right">
		            	<div class="number-budge bg-danger"><?php echo $totalvehicles; ?></div>
		            </div>
				</div>

				</div>
			</div>
				</div>
			<?php
    	}
    }
}

function leads_counter($userCenter){
	global $dbc;
	$q = "SELECT
	  (SELECT COUNT(*) FROM VEHICLES WHERE id !='') as vehicles,
	  (SELECT COUNT(*) FROM VEHICLES WHERE id !='' AND status = 0) as OfflineVehicles,
	  (SELECT COUNT(*) FROM MAKES WHERE id !='') as makes,
	  (SELECT COUNT(*) FROM BODY_TYPE WHERE id !='') as body_types,
	  (SELECT COUNT(*) FROM VHL_TYPE WHERE id !='') as vehicle_types,
	  (SELECT COUNT(*) FROM MODELS WHERE id !='') as models,
	  (SELECT COUNT(*) FROM DEPARTMENT WHERE id !='') as departments,
	  (SELECT COUNT(*) FROM COST_CENTER WHERE id !='') as cost_centers,
	  (SELECT COUNT(*) FROM BASE WHERE id !='') as bases";

	if(($r = mysqli_query($dbc, $q)) !== false) {
		while( $obj = mysqli_fetch_assoc( $r )) {
			$totalmakes = $obj['makes'];
            ?>
			<div class="col-md-6 col-lg-3">
			    <div class="widget-bg-color-icon card-box zoomIn animated">
			        <div class="bg-icon bg-icon-danger pull-left">
			            <i class="md md-directions-car text-danger"></i>
			        </div>
			        <div class="text-right">
			            <h3 class="text-dark"><b class="counter"><?php echo $obj['vehicles']; ?></b></h3>
			            <p class="text-muted">Total fleets</p>
			        </div>
			        <div class="clearfix"></div>
			    </div>
			</div>

			<div class="col-md-6 col-lg-3">
			    <div class="widget-bg-color-icon card-box zoomIn animated">
			        <div class="bg-icon bg-icon-purple pull-left">
			            <i class="md md-verified-user text-purple"></i>
			        </div>
			        <div class="text-right">
			            <h3 class="text-dark"><b class="counter"><?php echo $obj['OfflineVehicles']; ?></b></h3>
			            <p class="text-muted">Grounded vehicles</p>
			        </div>
			        <div class="clearfix"></div>
			    </div>
			</div>

			<div class="col-md-6 col-lg-3">
			    <div class="widget-bg-color-icon card-box zoomIn animated">
			        <div class="bg-icon bg-icon-info pull-left">
			            <i class="md md-store text-info"></i>
			        </div>
			        <div class="text-right">
			            <h3 class="text-dark"><b class="counter"><?php echo $obj['departments']; ?></b></h3>
			            <p class="text-muted">Total departments</p>
			        </div>
			        <div class="clearfix"></div>
			    </div>
			</div>

			<div class="col-md-6 col-lg-3">
			    <div class="widget-bg-color-icon card-box zoomIn animated">
			    	<div class="bg-icon bg-icon-pink pull-left">
                        <i class="md md-account-balance-wallet text-pink"></i>
                    </div>
			        <div class="text-right">
			            <h3 class="text-dark"><b class="counter"><?php echo $obj['cost_centers']; ?></b></h3>
			            <p class="text-muted">Total C. Centers</p>
			        </div>
			        <div class="clearfix"></div>
			    </div>
			</div>
			<?php
        }
    }
}

function make_counter($center){
	global $dbc;
	// $sql = "SELECT COUNT(id) as NumberOfVhl, make FROM VEHICLES WHERE id !='' AND costcentre = '$center' GROUP BY make";
	$sql = "SELECT COUNT(v.id) AS 'NumberOfVhl',
       v.make AS 'make',
       m.description AS 'description'
       FROM VEHICLES AS v
       JOIN MAKES AS m ON v.make = m.make AND m.costCenter = '$center'
       WHERE v.id !='' AND costcentre = '$center' GROUP BY v.make";
	$total = 0;
    if(($rst = mysqli_query($dbc, $sql)) !== false) {
    	?>
    	<div class="col-md-12 col-lg-12">
		    <div class="widget-bg-color-icon card-box zoomIn animated">
		    <h4 class="text-dark header-title m-t-0">Makes Count</h4>
		    <div class="row">
    	<?php
		while( $row = mysqli_fetch_assoc( $rst )) {
			$NumberOfVhl = $row['NumberOfVhl'];
			$make = $row['description'];
			// $make1 = getMakeName($make);
			$finaltotal = $NumberOfVhl + $total;
			?>
				<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 m-b-15">
					<div class="col-md-7 col-sm-8 col-xs-8 col-md-offset-1 text-left" style="padding: 5px 0px 0px 0px;"><?php echo $make;?></div>
	                <div class="col-md-3 col-sm-4 col-xs-4 text-right">
	                	<div class="number-budge bg-success"><?php echo $row['NumberOfVhl']; ?></div>
	                </div>
				</div>
			<?php
		}
	}
	$q = "SELECT
	  (SELECT COUNT(*) FROM MAKES WHERE id !='' AND costCenter = '$center') as makes";
	if(($r = mysqli_query($dbc, $q)) !== false) {
		while( $obj = mysqli_fetch_assoc( $r )) {
			$totalmakes = $obj['makes'];
			?>
			<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 m-b-15">
				<div class="col-md-7 col-sm-8 col-xs-8 col-md-offset-1 text-left" style="padding: 5px 0px 0px 0px;">Total Makes</div>
                <div class="col-md-3 col-sm-4 col-xs-4 text-right">
                	<div class="number-budge bg-danger"><?php echo $totalmakes; ?></div>
                </div>
			</div>
				</div>
				</div>
			</div>
		<?php
		}
	}
}

// load Accidents in pagination
function allAccidents($page,$user_email,$uid,$user_center){
	global $dbc;
	$item_per_page = 20;
	if($user_email && $uid){
	    // $uid = $_SESSION['id'];
	    if($page != ""){
	        $page_number = $page;
	        if(!is_numeric($page_number)){
	            $page_number = 1;
	        } //incase of invalid page number
	    }else{
	        $page_number = 1; //if there's no page number, set it to 1
	    }
	    //get total number of records from database for pagination
	    $results = "SELECT id FROM Accidents Where id !='' AND costCenter = '$user_center'";
	    $results = mysqli_query($dbc, $results);
	    $get_total_rows = mysqli_num_rows($results);
	    //break records into pages
	    $total_pages = ceil($get_total_rows/$item_per_page);

	    //get starting position to fetch the records
	    $page_position = (($page_number-1) * $item_per_page);

	    $q = "SELECT id, refno, acc_type, liabilityflag, plateNumber, Model, ChasNumber, AccDateTime, DriverName, cost FROM Accidents WHERE costCenter = '$user_center' ORDER BY id DESC LIMIT $page_position, $item_per_page";
	    $r = mysqli_query($dbc, $q);
	    ?>
	<div class="table-responsive">
	    <table class="table table-hover mails m-0 table table-actions-bar">
	        <thead>
	            <tr>
	                <th>Vehicle</th>
	                <th>Model</th>
                    <th>Date</th>
                    <th>Accident type</th>
                    <th>Driver</th>
                    <th>Blame</th>
                    <th>Cost</th>
                    <th class="text-right">Action</th>
	            </tr>
	        </thead>
	        <tbody>
	<?php
	    while( $obj = mysqli_fetch_assoc( $r )) {
	    	date_default_timezone_set("Africa/Nairobi");
	        $id = $obj['id'];
	        $refno = $obj['refno'];
	        $liabilityflag = $obj['liabilityflag'];
	        $cost = $obj['cost'];

	        $acc_type = $obj['acc_type'];
	        $vehicle = $obj['plateNumber'];
	        $Model = $obj['Model'];
	        $Model1 = getModelName($Model);

	        $quantity = $obj['ChasNumber'];
	        $AccDateTime = $obj['AccDateTime'];
	        if (isset($AccDateTime) && !empty($AccDateTime)) {
	        	$MonthYearf = strtotime($AccDateTime);
				$ad = date('d M Y', $MonthYearf);
	        }
	        else{
	        	$ad = "";
	        }

	        $DriverName = getDriverName($obj['DriverName'], $user_center);
	        ?>
	            <tr id='<?php echo $id; ?>'>
	                <td style='width: 10%;'><?php echo $vehicle; ?></td>
	                <td style='width: 15%;'><?php echo $Model1; ?></td>
	                <td style='width: 10%;'><?php echo $ad; ?></td>
	                <td style='width: 15%;'><?php echo $acc_type ?></td>
	                <td style='width: 18%;'><?php echo $DriverName; ?></td>
	                <td style='width: 10%;'><?php echo $liabilityflag; ?></td>
	                <td style='width: 10%;'><?php echo $cost; ?></td>
	                <td class="text-right" style='width: 11%;'>
	                    <span class='AccidentID' data='<?php echo $refno; ?>' id='<?php echo $id; ?>' page='<?php echo $gg; ?>' type='<?php echo $uid; ?>'>
	                        <button class='btn btn-info editable-table-button btn-xs editAccident'>Edit</button>
	                        <?php
	                            if ($uid == 1) {
	                                ?>
	                                    <button class='btn btn-danger editable-table-button btn-xs deleteAccident'>Del</button>
	                                <?php
	                            }
	                        ?>
	                    </span>
	                </td>
	            </tr>
	        <?php
	    }
	    ?>
	    </tbody></table></div>
	    <div class='row'>
	        <div class='col-sm-12'>
	            <div class='col-sm-7 col-md-offset-2 text-center'>
	    <?php
	    echo paginate_function($item_per_page, $page_number, $get_total_rows, $total_pages);
	    ?>
	        </div></div></div>
	    <?php
	}
}

function getCostingPostedDates($final, $code){
	global $dbc;
	$q = "SELECT Ddate, Code FROM COSTING WHERE Ddate like '%$final%' AND Code = '$code'";
	$data = mysqli_query($dbc, $q);
	$count = mysqli_num_rows($data);
	if ($count >= 1) {
		$search = "DELETE FROM COSTING WHERE Ddate like '%$final%' AND Code = '$code'";
		$result = mysqli_query($dbc, $search);
		if ($result) {
			return true;
		}
	}
	else{
		return true;
	}
}

function getDistancePostedDates($final, $code){
	global $dbc;
	$q = "SELECT monthSelect, reference FROM Distance_Covered WHERE monthSelect like '%$final%' AND reference = '$code'";
	$data = mysqli_query($dbc, $q);
	$count = mysqli_num_rows($data);
	if ($count >= 1) {
		$search = "DELETE FROM Distance_Covered WHERE monthSelect like '%$final%' AND reference = '$code'";
		$result = mysqli_query($dbc, $search);
		if ($result) {
			return true;
		}
	}
	else{
		return true;
	}
}

// load fuel issuance in pagination
function allDistanceRecords($page,$user_email,$uid,$user_center,$search){
	global $dbc;
	$item_per_page = 20;
	if($user_email && $uid){
	    
	    if (strlen($search) >= 4) {
		 	$postedVal = $search;
		}else{
			$postedVal = "noSearchedCar";
		}

	    if($page != ""){
	        $page_number = $page;
	        if(!is_numeric($page_number)){
	            $page_number = 1;
	        }
	    }else{
	        $page_number = 1;
	    }

	    if ($postedVal == "noSearchedCar") {
		    $sql = "SELECT id FROM Distance_Covered Where id !='' AND Costcenter = '$user_center'";
	    }else{
	    	$sql = "SELECT i.id AS 'id'
		        FROM Distance_Covered AS i
		        WHERE (i.Vehicle like '%$postedVal%') AND i.Costcenter = '$user_center'";
	    }

	    //get total number of records from database for pagination
	    $results = mysqli_query($dbc, $sql);
	    $get_total_rows = mysqli_num_rows($results);
	    //break records into pages
	    $total_pages = ceil($get_total_rows/$item_per_page);

	    //get starting position to fetch the records
	    $page_position = (($page_number-1) * $item_per_page);

	    if ($postedVal == "noSearchedCar") {
		    $q = "SELECT i.id AS 'id',
			i.Vehicle AS 'Vehicle',
			i.Model AS 'Model',
			i.vtype AS 'vtype',
			i.monthSelect AS 'monthSelect',
			i.department AS 'department',
			i.Costcenter AS 'Costcenter',
	        i.Start_Odo AS 'Start_Odo',
	        i.Close_Odo AS 'Close_Odo',
	        i.Distance AS 'Distance',
	        i.reference AS 'reference'
	        FROM Distance_Covered AS i
	        WHERE i.Costcenter = '$user_center' ORDER BY i.Vehicle ASC, i.monthSelect DESC LIMIT $page_position, $item_per_page";
	    }
	    else{
	    	$q = "SELECT i.id AS 'id',
			i.Vehicle AS 'Vehicle',
			i.Model AS 'Model',
			i.vtype AS 'vtype',
			i.monthSelect AS 'monthSelect',
			i.department AS 'department',
			i.Costcenter AS 'Costcenter',
			i.Start_Odo AS 'Start_Odo',
	        i.Close_Odo AS 'Close_Odo',
	        i.Distance AS 'Distance',
	        i.reference AS 'reference'
	        FROM Distance_Covered AS i
	        WHERE (i.Vehicle like '%$postedVal%') AND i.Costcenter = '$user_center' ORDER BY i.Vehicle ASC, i.monthSelect DESC LIMIT $page_position, $item_per_page";
	    }
	    $r = mysqli_query($dbc, $q);
	    ?>
	<div class="table-responsive">
	    <table class="table table-hover mails m-0 table table-actions-bar">
	        <thead>
	            <tr>
	                <th>Vehicle</th>
                    <th>Month</th>
                    <th>Starting</th>
                    <th>Closeing</th>
                    <th>Distance</th>
                    <th>Model</th>
                    <th class="text-right">Action</th>
	            </tr>
	        </thead>
	        <tbody>
	<?php
	    while( $obj = mysqli_fetch_assoc( $r )) {
	        $id = $obj['id'];
	        $Vehicle = $obj['Vehicle'];
	        $Ddate = $obj['monthSelect'];

	        $MonthYear = strtotime($Ddate);
			$daterange = date('M Y', $MonthYear);

	        $Costcenter = $obj['Costcenter'];
	        $Costcenter1 = getCostCenterName($Costcenter);
	        $Code = $obj['reference'];
	        $Code1 = getCostingCodeName($Code);
	        $Start_Odo = $obj['Start_Odo'];
	        $Close_Odo = $obj['Close_Odo'];
	        $Distance = $obj['Distance'];
	        $Model = $obj['Model'];

	        ?>
	            <tr id='<?php echo $id; ?>'>
	                <td style='width: 15%;'><?php echo $Vehicle; ?></td>
	                <td style='width: 10%;'><?php echo $daterange; ?></td>
	                <td style='width: 15%;'><?php echo number_format($Start_Odo, 2); ?></td>
	                <td style='width: 15%;'><?php echo number_format($Close_Odo, 2); ?></td>
	                <td style='width: 15%;'><?php echo number_format($Distance, 2); ?></td>
	                <td style='width: 15%;'><?php echo $Model ?></td>
	                <td class="text-right" style='width: 14%;'>
	                    <span class='issueID' data='<?php echo $Vehicle; ?>' id='<?php echo $id; ?>' page='<?php echo $gg; ?>' type='<?php echo $uid; ?>'>
	                        <button class='btn btn-info editable-table-button btn-xs editCosting'>Edit</button>
	                        <?php
	                            if ($uid == 1) {
	                                ?>
	                                    <button class='btn btn-danger editable-table-button btn-xs deleteCosting'>Del</button>
	                                <?php
	                            }
	                        ?>
	                    </span>
	                </td>
	            </tr>
	        <?php
	    }
	    ?>
	    </tbody></table></div>
	    <div class='row'>
	        <div class='col-sm-12'>
	            <div class='col-sm-7 col-md-offset-2 text-center'>
				    <?php
				    echo paginate_function($item_per_page, $page_number, $get_total_rows, $total_pages);
				    ?>
		        </div>
		        <div class="col-sm-3 text-right m-t-15">
		        	<button type="button" style="margin-right: -8px;" class="btn btn-sm btn-github waves-effect waves-light">
	                    <span class="btn-label"><i class="ti-paint-bucket"></i> </span><?php echo number_format($get_total_rows); ?>
	                </button>
		        </div>
		    </div>
		</div>
	    <?php
	}
}

// load fuel issuance in pagination
function allCostingRecords($page,$user_email,$uid,$user_center){
	global $dbc;
	$item_per_page = 20;
	if($user_email && $uid){
	    // $uid = $_SESSION['id'];
	    if($page != ""){
	        $page_number = $page;
	        if(!is_numeric($page_number)){
	            $page_number = 1;
	        }//incase of invalid page number
	    }else{
	        $page_number = 1; //if there's no page number, set it to 1
	    }
	    //get total number of records from database for pagination
	    $results = "SELECT id FROM COSTING Where id !='' AND Costcenter = '$user_center'";
	    $results = mysqli_query($dbc, $results);
	    $get_total_rows = mysqli_num_rows($results);
	    //break records into pages
	    $total_pages = ceil($get_total_rows/$item_per_page);

	    //get starting position to fetch the records
	    $page_position = (($page_number-1) * $item_per_page);

	    $q = "SELECT * FROM COSTING WHERE Costcenter = '$user_center' ORDER BY id DESC LIMIT $page_position, $item_per_page";
	    $r = mysqli_query($dbc, $q);
	    ?>
	<div class="table-responsive">
	    <table class="table table-hover mails m-0 table table-actions-bar">
	        <thead>
	            <tr>
	                <th>Vehicle</th>
                    <th>Month</th>
                    <th>Code</th>
                    <th>Cost</th>
                    <th>Reference</th>
                    <th class="text-right">Action</th>
	            </tr>
	        </thead>
	        <tbody>
	<?php
	    while( $obj = mysqli_fetch_assoc( $r )) {
	        $id = $obj['id'];
	        $Vehicle = $obj['Vehicle'];
	        $Ddate = $obj['Ddate'];

	        $MonthYear = strtotime($Ddate);
			$daterange = date('M Y', $MonthYear);

	        $Costcenter = $obj['Costcenter'];
	        $Costcenter1 = getCostCenterName($Costcenter);
	        $Code = $obj['Code'];
	        $Code1 = getCostingCodeName($Code);
	        $Cost = $obj['Cost'];
	        $Income = $obj['Income'];
	        $reference = $obj['reference'];

	        ?>
	            <tr id='<?php echo $id; ?>'>
	                <td style='width: 20%;'><?php echo $Vehicle; ?></td>
	                <td style='width: 15%;'><?php echo $daterange; ?></td>
	                <td style='width: 15%;'><?php echo $Code1; ?></td>
	                <td style='width: 15%;'><?php echo number_format($Cost, 2); ?></td>
	                <td style='width: 15%;'><?php echo $reference ?></td>
	                <td class="text-right" style='width: 11%;'>
	                    <span class='issueID' data='<?php echo $Vehicle; ?>' id='<?php echo $id; ?>' page='<?php echo $gg; ?>' type='<?php echo $uid; ?>'>
	                        <button class='btn btn-info editable-table-button btn-xs editCosting'>Edit</button>
	                        <?php
	                            if ($uid == 1) {
	                                ?>
	                                    <button class='btn btn-danger editable-table-button btn-xs deleteCosting'>Del</button>
	                                <?php
	                            }
	                        ?>
	                    </span>
	                </td>
	            </tr>
	        <?php
	    }
	    ?>
	    </tbody></table></div>
	    <div class='row'>
	        <div class='col-sm-12'>
	            <div class='col-sm-7 col-md-offset-2 text-center'>
	    <?php
	    echo paginate_function($item_per_page, $page_number, $get_total_rows, $total_pages);
	    ?>
	        </div></div></div>
	    <?php
	}
}

function getAvaileStockCount($partcode, $user_center){
	global $dbc;
	$q = "SELECT sum(quantity) AS totalcount FROM StockDetails WHERE partNumb = '$partcode' AND costCenter = '$user_center'";
	$data = mysqli_query($dbc, $q);
	while ($row = mysqli_fetch_assoc($data)):
		$totalcount = $row['totalcount'];
		return $totalcount;
	endwhile;
}

// load work tickets in pagination
function allStocks($page,$user_email,$uid,$user_center){
	global $dbc;
	$item_per_page = 20;
	if($user_email && $uid){
	    // $uid = $_SESSION['id'];
	    if($page != ""){
	        $page_number = $page;
	        if(!is_numeric($page_number)){
	            $page_number = 1;
	        } //incase of invalid page number
	    }else{
	        $page_number = 1; //if there's no page number, set it to 1
	    }
	    //get total number of records from database for pagination
	    $results = "SELECT id FROM StockHeader Where id !='' AND costCenter = '$user_center'";
	    $results = mysqli_query($dbc, $results);
	    $get_total_rows = mysqli_num_rows($results);
	    //break records into pages
	    $total_pages = ceil($get_total_rows/$item_per_page);

	    //get starting position to fetch the records
	    $page_position = (($page_number-1) * $item_per_page);

	    $q = "SELECT * FROM StockHeader WHERE costCenter = '$user_center' ORDER BY partDesc ASC LIMIT $page_position, $item_per_page";
	    $r = mysqli_query($dbc, $q);
	    ?>
	<div class="table-responsive">
	    <table class="table table-hover mails m-0 table table-actions-bar">
	        <thead>
	            <tr>
	                <th>Stock Number</th>
                    <th>Description</th>
                    <th>Category</th>
                    <th>Part code</th>
                    <th>R. Level</th>
                    <th style="text-align: center;">Qty.</th>
                    <th class="text-right">Action</th>
	            </tr>
	        </thead>
	        <tbody>
	<?php
	    while( $obj = mysqli_fetch_assoc( $r )) {
	    	date_default_timezone_set("Africa/Nairobi");
	        $id = $obj['id'];
	        $stocknumb = $obj['stocknumb'];
	        $partDesc = $obj['partDesc'];
	        $Category = getStockCategoryName($obj['Category'], $user_center);
	        $partcode = $obj['partcode'];
	        $reorderLevel = $obj['reorderLevel'];
	        $Available = getAvaileStockCount($obj['partcode'], $user_center);

	        if ($Available == "") {
	        	$Available = "0.00";
	        }
	        else{
	        	$Available = number_format($Available, 2);
	        }

	        ?>
	            <tr id='<?php echo $id; ?>'>
	                <td style='width: 12%;'><?php echo $stocknumb; ?></td>
	                <td style='width: 25%;'><?php echo $partDesc; ?></td>
	                <td style='width: 12%;'><?php echo $Category; ?></td>
	                <td style='width: 15%;'><?php echo $partcode; ?></td>
	                <td style='width: 6%; text-align: center;'><?php echo $reorderLevel; ?></td>
	                <td style='width: 5%; text-align: center;'><?php echo $Available; ?></td>
	                <td class="text-right" style='width: 8%;'>
	                    <span class='StockHead' data='<?php echo $stocknumb; ?>' id='<?php echo $id; ?>' page='<?php echo $page; ?>' type='<?php echo $uid; ?>'>
	                        <button class='btn btn-info editable-table-button btn-xs editStockHead'>Edit</button>
	                        <?php
	                            if ($uid == 1) {
	                                ?>
	                                    <button class='btn btn-danger editable-table-button btn-xs deleteStockHead'>Del</button>
	                                <?php
	                            }
	                        ?>
	                    </span>
	                </td>
	            </tr>
	        <?php
	    }
	    ?>
	    </tbody></table></div>
	    <div class='row'>
	        <div class='col-sm-12'>
	            <div class='col-sm-7 col-md-offset-2 text-center'>
	    <?php
	    echo paginate_function($item_per_page, $page_number, $get_total_rows, $total_pages);
	    ?>
	        </div></div></div>
	    <?php
	}
}

// load work tickets in pagination
function allOrders($page,$user_email,$uid,$user_center){
	global $dbc;
	$item_per_page = 20;
	if($user_email && $uid){
	    // $uid = $_SESSION['id'];
	    if($page != ""){
	        $page_number = $page;
	        if(!is_numeric($page_number)){
	            $page_number = 1;
	        } //incase of invalid page number
	    }else{
	        $page_number = 1; //if there's no page number, set it to 1
	    }
	    //get total number of records from database for pagination
	    $results = "SELECT id FROM OrderHeader Where id !='' AND costCenter = '$user_center'";
	    $results = mysqli_query($dbc, $results);
	    $get_total_rows = mysqli_num_rows($results);
	    //break records into pages
	    $total_pages = ceil($get_total_rows/$item_per_page);

	    //get starting position to fetch the records
	    $page_position = (($page_number-1) * $item_per_page);

	    $q = "SELECT * FROM OrderHeader WHERE costCenter = '$user_center' ORDER BY id DESC LIMIT $page_position, $item_per_page";
	    $r = mysqli_query($dbc, $q);
	    ?>
	<div class="table-responsive">
	    <table class="table table-hover mails m-0 table table-actions-bar">
	        <thead>
	            <tr>
	                <th>Order Number</th>
                    <th>LPO No.</th>
                    <th>Date Ordered</th>
                    <th>Ordered by</th>
                    <th>Supplier</th>
                    <th class="text-right">Action</th>
	            </tr>
	        </thead>
	        <tbody>
	<?php
	    while( $obj = mysqli_fetch_assoc( $r )) {
	    	date_default_timezone_set("Africa/Nairobi");
	        $id = $obj['id'];
	        $ordernumber = $obj['ordernumber'];
	        $booknumber = $obj['booknumber'];
	        $dateOrdered = $obj['dateOrdered'];
	        $orderedBy = $obj['orderedBy'];
	        $supplier = getSupplierName($obj['supplier'], $user_center);
	        date_default_timezone_set("Africa/Nairobi");
            if ($dateOrdered) {
                $DateCompleted = strtotime($dateOrdered);
                $CMonth = date('d M Y', $DateCompleted);
            }else{
                $CMonth = "";
            }

	        ?>
	            <tr id='<?php echo $id; ?>'>
	                <td style='width: 12%;'><?php echo $ordernumber; ?></td>
	                <td style='width: 12%;'><?php echo $booknumber; ?></td>
	                <td style='width: 15%;'><?php echo $CMonth; ?></td>
	                <td style='width: 15%;'><?php echo $orderedBy; ?></td>
	                <td style='width: 18%;'><?php echo $supplier; ?></td>
	                <td class="text-right" style='width: 11%;'>
	                    <span class='OrderHead' data='<?php echo $ordernumber; ?>' id='<?php echo $id; ?>' page='<?php echo $page; ?>' type='<?php echo $uid; ?>'>
	                        <button class='btn btn-info editable-table-button btn-xs editOrderHead'>Edit</button>
	                        <?php
	                            if ($uid == 1) {
	                                ?>
	                                    <button class='btn btn-danger editable-table-button btn-xs deleteOrderHead'>Del</button>
	                                <?php
	                            }
	                        ?>
	                    </span>
	                </td>
	            </tr>
	        <?php
	    }
	    ?>
	    </tbody></table></div>
	    <div class='row'>
	        <div class='col-sm-12'>
	            <div class='col-sm-7 col-md-offset-2 text-center'>
	    <?php
	    echo paginate_function($item_per_page, $page_number, $get_total_rows, $total_pages);
	    ?>
	        </div></div></div>
	    <?php
	}
}

// load work tickets in pagination
function allWorkTickets($page,$user_email,$uid,$user_center){
	global $dbc;
	$item_per_page = 20;
	if($user_email && $uid){
	    // $uid = $_SESSION['id'];
	    if($page != ""){
	        $page_number = $page;
	        if(!is_numeric($page_number)){
	            $page_number = 1;
	        } //incase of invalid page number
	    }else{
	        $page_number = 1; //if there's no page number, set it to 1
	    }
	    //get total number of records from database for pagination
	    $results = "SELECT id FROM Workheader Where id !='' AND costCenter = '$user_center'";
	    $results = mysqli_query($dbc, $results);
	    $get_total_rows = mysqli_num_rows($results);
	    //break records into pages
	    $total_pages = ceil($get_total_rows/$item_per_page);

	    //get starting position to fetch the records
	    $page_position = (($page_number-1) * $item_per_page);

	    $q = "SELECT * FROM Workheader WHERE costCenter = '$user_center' ORDER BY id DESC LIMIT $page_position, $item_per_page";
	    $r = mysqli_query($dbc, $q);
	    ?>
	<div class="table-responsive">
	    <table class="table table-hover mails m-0 table table-actions-bar">
	        <thead>
	            <tr>
	                <th>Ticket Number</th>
                    <th>Vehicle</th>
                    <th>Make</th>
                    <th>Date out</th>
                    <th>Start Odo</th>
                    <th>Status</th>
                    <th class="text-right">Action</th>
	            </tr>
	        </thead>
	        <tbody>
	<?php
	    while( $obj = mysqli_fetch_assoc( $r )) {
	    	date_default_timezone_set("Africa/Nairobi");
	        $id = $obj['id'];
	        $worknumber = $obj['worknumber'];
	        $vehicle = $obj['vehicle'];
	        $dateOpened = $obj['dateOpened'];

	        if (isset($dateOpened) && !empty($dateOpened)) {
	        	$x = strtotime($dateOpened);
				$dateOpened1 = date('d M Y', $x);
	        }

	        $workstatus = $obj['workstatus'];
            if ($workstatus == '0') {
                $status = "Closed";
            }else{
                $status = "Open";
            }
	        $Startodo = $obj['Startodo'];
	        $make = $obj['make'];
	        $make1 = getmakeName($make, $user_center);

	        ?>
	            <tr id='<?php echo $id; ?>'>
	                <td style='width: 12%;'><?php echo $worknumber; ?></td>
	                <td style='width: 12%;'><?php echo $vehicle; ?></td>
	                <td style='width: 15%;'><?php echo $make1; ?></td>
	                <td style='width: 15%;'><?php echo $dateOpened1; ?></td>
	                <td style='width: 18%;'><?php echo $Startodo; ?></td>
	                <td style='width: 18%;'><?php echo $status ?></td>
	                <td class="text-right" style='width: 11%;'>
	                    <span class='WorkHead' data='<?php echo $worknumber; ?>' id='<?php echo $id; ?>' page='<?php echo $gg; ?>' type='<?php echo $uid; ?>'>
	                        <button class='btn btn-info editable-table-button btn-xs editWorkHead'>Edit</button>
	                        <?php
	                            if ($uid == 1) {
	                                ?>
	                                    <button class='btn btn-danger editable-table-button btn-xs deleteWorkHead'>Del</button>
	                                <?php
	                            }
	                        ?>
	                    </span>
	                </td>
	            </tr>
	        <?php
	    }
	    ?>
	    </tbody></table></div>
	    <div class='row'>
	        <div class='col-sm-12'>
	            <div class='col-sm-7 col-md-offset-2 text-center'>
	    <?php
	    echo paginate_function($item_per_page, $page_number, $get_total_rows, $total_pages);
	    ?>
	        </div></div></div>
	    <?php
	}
}

// load other center jobs in pagination
function otherCenterJobs($page,$user_email,$uid,$user_center){
	global $dbc;
	$item_per_page = 5;
	if($uid){
	    // $uid = $_SESSION['id'];
	    if($page != ""){
	        $page_number = $page;
	        if(!is_numeric($page_number)){
	            $page_number = 1;
	        } //incase of invalid page number
	    }else{
	        $page_number = 1; //if there's no page number, set it to 1
	    }
	    //get total number of records from database for pagination
	    $results = "SELECT id FROM JobDetailOtherCenter WHERE CostCenterFrom = '$user_center'";
	    $results = mysqli_query($dbc, $results);
	    $get_total_rows = mysqli_num_rows($results);
	    //break records into pages
	    $total_pages = ceil($get_total_rows/$item_per_page);

	    //get starting position to fetch the records
	    $page_position = (($page_number-1) * $item_per_page);

	    $q = "SELECT * FROM JobDetailOtherCenter WHERE CostCenterFrom = '$user_center' ORDER BY id DESC LIMIT $page_position, $item_per_page";
	    $r = mysqli_query($dbc, $q);
	    ?>
	<div class="table-responsive">
	    <table class="table table-hover mails m-0 table table-actions-bar">
	        <thead>
	            <tr>
	                <th>Service type</th>
                    <th>Part No.</th>
                    <th>Description.</th>
                    <th>Qty</th>
                    <th>Total cost</th>
                    <th>Vendor</th>
                    <th>Issue date</th>
                    <th>Initials</th>
                    <th class="text-right">Action</th>
	            </tr>
	        </thead>
	        <tbody>
	<?php
	    while( $obj = mysqli_fetch_assoc( $r )) {
	    	date_default_timezone_set("Africa/Nairobi");
	        $id = $obj['id'];
	        $RepairType = $obj['RepairType'];
	        $RepairType1 = getMaintenanceName($RepairType);
	        $partDescription = $obj['partDescription'];
	        $PartNumber = $obj['PartNumber'];
	        $Quantity = $obj['Quantity'];
	        $JobTotalCost = $obj['JobTotalCost'];
	        $VendorName = $obj['VendorName'];
	        $VendorName1 = getVendorName($VendorName, $user_center);
	        $JobStartDate1 = $obj['JobStartDate'];
	        if (isset($JobStartDate1) && !empty($JobStartDate1)) {
				$x = strtotime($JobStartDate1);
				$JobStartDate = date('d M Y', $x);
			}
			else{
				$JobStartDate = "";
			}

	        $added_by = $obj['added_by'];
	        $initials = getUserInitials($added_by);
	        ?>
	            <tr id='<?php echo $id; ?>'>
	                <td style='width: 15%;'><?php echo $RepairType1; ?></td>
	                <td style='width: 10%;'><?php echo $PartNumber; ?></td>
	                <td style='width: 20%;'><?php echo $partDescription; ?></td>
	                <td style='width: 7%;'><?php echo $Quantity; ?></td>
	                <td style='width: 10%;'><?php echo $JobTotalCost; ?></td>
	                <td style='width: 15%;'><?php echo $VendorName1; ?></td>
	                <td style='width: 10%;'><?php echo $JobStartDate; ?></td>
	                <td style='width: 5%; text-align: center;'><?php echo $initials; ?></td>
	                <td class="text-right" style='width: 11%;'>
	                    <span class='JBcardID' user='<?php echo $user_pid; ?>' data='<?php echo $JobNumber; ?>' id='<?php echo $id; ?>' type='<?php echo $uid; ?>'>
                            <button class='btn btn-info editable-table-button btn-xs editJob' data-toggle="modal" data-target="#returnPart_<?php echo $id; ?>">Edit</button>
	                        <?php
	                            if ($uid == 1) {
	                                ?>
	                                    <!-- <button class='btn btn-danger editable-table-button btn-xs deletejbdetails'>Del</button> -->
	                                <?php
	                            }
	                        ?>
	                    </span>
	                </td>
	                <div id="returnPart_<?php echo $id; ?>" data="<?php echo $id; ?>" class="modal FleetJob fade" tabindex="-1" role="dialog" aria-labelledby="custom-width-modalLabel" aria-hidden="true" style="display: none;">
                    	<form class="edit_<?php echo $id;?>" id="edit_<?php echo $id;?>">
	                        <div class="modal-dialog modal-lg">
	                            <div class="modal-content">
	                                <div class="modal-header">
	                                	<div class="row">
	                                		<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
			                                    <h4 class="modal-title" id="custom-width-modalLabel">JOBCARD EDITING FOR <?php echo $obj['JobNumber'];?></h4>
	                                		</div>
	                                		<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 text-right">
			                                	<button type="submit" class="btn btn-xs btn-success waves-effect saveJobEdits">Save</button>
			                                	<button type="button" class="btn btn-xs btn-danger waves-effect" data-dismiss="modal">Close</button>
	                                		</div>
	                                	</div>
	                                </div>
	                                <div class="modal-body">
	                                    <div class="row m-b-5 m-t-10">
						                    <div class="col-md-6">
						                        <div class="form-group">
						                            <label for="field-1" class="control-label">Job Number: <span class="text-danger">*</span></label>
						                            <input type="text" class="form-control col-md-12 jobnumber_<?php echo $id;?>" name="jobnumber1" readonly value="<?php echo $obj['JobNumber']; ?>">
						                            <input type="hidden" name="user_center" class="user_center" value="<?php echo $user['center'];?>">
						                            <input type="hidden" name="vehicle" value="<?php echo $obj['VID']; ?>">
						                            <input type="hidden" class="recordId" value="<?php echo $id;?>">
						                        </div>
						                    </div>

						                    <div class="col-md-6">
						                        <div class="form-group">
						                        	<div class="form-group">
							                            <label for="field-2" class="control-label">Part Number:</label>
							                            <input type="text" class="form-control col-md-12 part_desc" name="part_desc" value="<?php echo getPartName($obj['PartNumber'], $obj['CostCenter']); ?>">
							                        </div>
						                        </div>
						                    </div>
						                </div>
						                <div class="row m-b-10">
						                    <div class="col-md-6">
						                        <div class="form-group">
						                            <label for="field-2" class="control-label">Part Description: <span class="text-danger">*</span></label>
						                            <input type="text" class="form-control col-md-12 partDesc_<?php echo $id;?>" name="partDesc" value="<?php echo $obj['partDescription']; ?>">
						                        </div>
						                    </div>
						                    <div class="col-md-6">
						                        <div class="form-group">
						                            <label for="field-2" class="control-label">Issue date:</label>
						                            <?php
			                                            date_default_timezone_set("Africa/Nairobi");
			                                            if ($obj['JobStartDate']) {
			                                                $JobStartDate = strtotime($obj['JobStartDate']);
			                                                $CMonth = date('d-m-Y', $JobStartDate);
			                                            }
			                                        ?>
						                            <input type="text" class="form-control col-md-12 IssueDate" name="stockIssueDate" id="datepicker" value="<?php echo $CMonth; ?>">
						                        </div>
						                    </div>
						                </div>
						                <div class="row m-b-10">
						                    <div class="col-md-4">
						                        <div class="form-group">
						                            <label for="field-1" class="control-label">Quantity: <span class="text-danger">*</span></label>
						                            <input type="text" name="quantity" autocomplete="off" class="form-control quantity_<?php echo $id; ?>" id="field-1" value="<?php echo $obj['Quantity']; ?>">
						                        </div>
						                    </div>
						                    <div class="col-md-4">
						                        <div class="form-group">
						                            <label for="field-2" class="control-label">Unit Price: <span class="text-danger">*</span></label>
						                            <input type="text" class="form-control col-md-12 unit_price_<?php echo $id; ?>" name="unit_price" value="<?php echo $obj['UnitPrice']; ?>">
						                        </div>
						                    </div>
						                    <div class="col-md-4">
						                        <div class="form-group">
						                            <label for="field-2" class="control-label">Job Total Cost: <span class="text-danger">*</span></label>
						                            <input type="text" class="form-control col-md-12 totalCost_<?php echo $id; ?>" name="totalCost" value="<?php echo $obj['JobTotalCost']; ?>">
						                        </div>
						                    </div>
						                </div>
						                <div class="row m-b-10">
						                    <div class="col-md-4">
						                        <div class="form-group">
						                            <label for="field-2" class="control-label">Vender / Service Center:</label>
						                            <input type="text" class="form-control col-md-12 services" name="services" value="<?php echo getVendorName($obj['VendorName'], $obj['CostCenter']); ?>">
						                        </div>
						                    </div>
						                    <div class="col-md-4">
						                        <div class="form-group">
						                            <label for="field-2" class="control-label">Reciept number:</label>
						                            <input type="text" name="reciept_numb" class="form-control col-md-12 reciept_numb" value="<?php echo $obj['ReceiptNo']; ?>">
						                        </div>
						                    </div>
						                    <div class="col-md-4">
						                        <div class="form-group">
						                            <label for="field-2" class="control-label">LPO Number:</label>
						                            <input type="text" name="lpo_numb" class="form-control col-md-12 lpo_numb" value="<?php echo $obj['POnumber']; ?>">
						                        </div>
						                    </div>
						                </div>
						                <div class="row m-b-10">
						                    <div class="col-md-4">
						                    	<div class="form-group">
						                            <label for="field-2" class="control-label">Work done / Repaire type:</label>
						                            <input type="text" name="lpo_numb" class="form-control col-md-12 lpo_numb" value="<?php echo getMaintenanceName($obj['RepairType']); ?>">
						                        </div>
						                    </div>
						                    <div class="col-md-4">
						                        <div class="form-group">
						                            <label for="field-1" class="control-label">Waranty Date:</label>
						                            <input type="text" class="form-control col-md-12 waranty_date" name="waranty_date" id="datepicker" value="<?php echo $obj['WarantyDate']; ?>">
						                        </div>
						                    </div>
						                    <div class="col-md-4">
						                        <div class="form-group">
						                            <label for="field-2" class="control-label">Warranty Description:</label>
						                            <input type="text" class="form-control col-md-12 waranty_desc" name="waranty_desc" value="<?php echo $obj['WarrantyDescription']; ?>">
						                        </div>
						                    </div>
						                </div>
						                <div class="row m-b-15">
						                    <div class="col-md-12">
						                        <div class="form-group">
						                            <label for="field-2" class="control-label">Work Done Description:</label>
						                            <input type="text" class="form-control col-md-12 remarks" name="remarks" value="<?php echo $obj['JobDescription']; ?>">
						                        </div>
						                    </div>
						                </div>
	                                </div>
	                            </div>
	                        </div>
                        </form>
                    </div>
	            </tr>
	        <?php
	    }
	    ?>
	    </tbody></table></div>
	    <div class='row'>
	        <div class='col-sm-12'>
	            <div class='col-sm-7 col-md-offset-2 text-center'>
	    <?php
	    echo paginate_results_function($item_per_page, $page_number, $get_total_rows, $total_pages);
	    ?>
	        </div></div></div>
	    <?php
	}
}

// load fuel issuance in pagination
function allJobcards($page,$user_email,$uid,$user_center){
	global $dbc;
	$item_per_page = 20;
	if($user_email && $uid){
	    // $uid = $_SESSION['id'];
	    if($page != ""){
	        $page_number = $page;
	        if(!is_numeric($page_number)){
	            $page_number = 1;
	        } //incase of invalid page number
	    }else{
	        $page_number = 1; //if there's no page number, set it to 1
	    }
	    //get total number of records from database for pagination
	    $results = "SELECT id FROM jobcardhead Where id !='' AND CostCenter = '$user_center'";
	    $results = mysqli_query($dbc, $results);
	    $get_total_rows = mysqli_num_rows($results);
	    //break records into pages
	    $total_pages = ceil($get_total_rows/$item_per_page);

	    //get starting position to fetch the records
	    $page_position = (($page_number-1) * $item_per_page);

	    $q = "SELECT id,JobNumber,VID,VehicleType,JobStartDate,JobStatus,ServiceType FROM jobcardhead WHERE CostCenter = '$user_center' ORDER BY id DESC LIMIT $page_position, $item_per_page";
	    $r = mysqli_query($dbc, $q);
	    ?>
	<div class="table-responsive">
	    <table class="table table-hover mails m-0 table table-actions-bar">
	        <thead>
	            <tr>
	                <th>Job Number</th>
                    <th>Vehicle</th>
                    <th>Vehicle Type</th>
                    <th>Job Date</th>
                    <th>Status</th>
                    <th>Service Type</th>
                    <th class="text-right">Action</th>
	            </tr>
	        </thead>
	        <tbody>
	<?php
	    while( $obj = mysqli_fetch_assoc( $r )) {
	    	date_default_timezone_set("Africa/Nairobi");
	        $id = $obj['id'];
	        $JobNumber = $obj['JobNumber'];
	        $vehicle = $obj['VID'];
	        $VehicleType = $obj['VehicleType'];
	        $VehicleType1 = getVehicleTypeName($VehicleType);
	        $JobStartDate = $obj['JobStartDate'];

	        $x = strtotime($JobStartDate);
			$datefrom1 = date('d M Y', $x);

	        $JobStatus = $obj['JobStatus'];
	        $ServiceType = $obj['ServiceType'];
	        $ServiceType1 = getServiceTypeName($ServiceType);

	        ?>
	            <tr id='<?php echo $id; ?>'>
	                <td style='width: 12%;'><?php echo $JobNumber; ?></td>
	                <td style='width: 12%;'><?php echo $vehicle; ?></td>
	                <td style='width: 15%;'><?php echo $VehicleType1; ?></td>
	                <td style='width: 15%;'><?php echo $datefrom1; ?></td>
	                <td style='width: 18%;'><?php echo $JobStatus; ?></td>
	                <td style='width: 18%;'><?php echo $ServiceType1 ?></td>
	                <td class="text-right" style='width: 11%;'>
	                    <span class='JBCHID' data='<?php echo $JobNumber; ?>' id='<?php echo $id; ?>' page='<?php echo $gg; ?>' type='<?php echo $uid; ?>'>
	                        <button class='btn btn-info editable-table-button btn-xs editJobcardHead'>Edit</button>
	                        <?php
	                            if ($uid == 1) {
	                                ?>
	                                    <button class='btn btn-danger editable-table-button btn-xs deleteJobHead'>Del</button>
	                                <?php
	                            }
	                        ?>
	                    </span>
	                </td>
	            </tr>
	        <?php
	    }
	    ?>
	    </tbody></table></div>
	    <div class='row'>
	        <div class='col-sm-12'>
	            <div class='col-sm-7 col-md-offset-2 text-center'>
				    <?php
				    echo paginate_function($item_per_page, $page_number, $get_total_rows, $total_pages);
				    ?>
		        </div>
		        <div class="col-sm-3 text-right m-t-15">
		        	<button type="button" style="margin-right: -8px;" class="btn btn-sm btn-github waves-effect waves-light">
	                    <span class="btn-label"><i class="ti-paint-bucket"></i> </span><?php echo number_format($get_total_rows); ?>
	                </button>
		        </div>
		    </div>
		</div>
	    <?php
	}
}


// load fuel issuance in pagination
function allIssuances($page,$user_email,$uid,$user_center,$search){
	global $dbc;
	$item_per_page = 20;
	if($user_email && $uid){
	    
	    if (strlen($search) >= 4) {
		 	$postedVal = $search;
		}else{
			$postedVal = "noSearchedCar";
		}

	    if($page != ""){
	        $page_number = $page;
	        if(!is_numeric($page_number)){
	            $page_number = 1;
	        }
	    }else{
	        $page_number = 1;
	    }

	    if ($postedVal == "noSearchedCar") {
		    $sql = "SELECT id FROM ISSUANCE WHERE id !='' AND costcenter = '$user_center'";
	    }else{
	    	$sql = "SELECT i.id AS 'id'
		        FROM ISSUANCE AS i
		        WHERE (i.vehicle like '%$postedVal%' OR i.reference like '%$postedVal%') AND i.costcenter = '$user_center'";
	    }

	    //get total number of records from database for pagination
	    $results = mysqli_query($dbc, $sql);
	    $get_total_rows = mysqli_num_rows($results);
	    //break records into pages
	    $total_pages = ceil($get_total_rows/$item_per_page);

	    //get starting position to fetch the records
	    $page_position = (($page_number-1) * $item_per_page);

	    if ($postedVal == "noSearchedCar") {
		    $q = "SELECT i.id AS 'id',
			i.vehicle AS 'vehicle',
			i.dates AS 'dates',
			i.odometer AS 'odometer',
			i.quantity AS 'quantity',
			i.totalcost AS 'totalcost',
			i.reference AS 'reference',
			i.fueltime AS 'fueltime',
			f.fuel_type AS 'FuelDescription',
	        i.location AS 'location'
	        FROM ISSUANCE AS i
	        LEFT JOIN FUEL_TYPE AS f ON i.fuel_type = f.code AND i.costcenter = f.costCenter
	        WHERE i.costcenter = '$user_center' ORDER BY i.vehicle ASC, i.dates DESC LIMIT $page_position, $item_per_page";
	    }
	    else{
	    	$q = "SELECT i.vehicle AS 'vehicle',
	        i.dates AS 'dates',
	        i.id AS 'id',
	        i.fueltime AS 'fueltime',
	        i.odometer AS 'odometer',
	        i.location AS 'location',
	        i.fuel_type AS 'fuel_type',
	        i.quantity AS 'quantity',
	        i.totalcost AS 'totalcost',
	        f.fuel_type AS 'FuelDescription'
	        FROM ISSUANCE AS i
	        LEFT JOIN FUEL_TYPE AS f ON i.fuel_type = f.code AND i.costcenter = f.costCenter
	        WHERE (i.vehicle like '%$postedVal%' OR i.reference like '%$postedVal%' OR i.location like '%$postedVal%') AND i.costcenter = '$user_center' ORDER BY i.vehicle ASC, i.dates DESC LIMIT $page_position, $item_per_page";
	    }
	    $r = mysqli_query($dbc, $q);
	    ?>
	<div class="table-responsive">
	    <table class="table table-hover mails m-0 table table-actions-bar">
	        <thead>
	            <tr>
	                <th>Vehicle</th>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Odometer</th>
                    <th>Location</th>
                    <th>Fuel type</th>
                    <th>Amount</th>
                    <th>Total cost</th>
                    <th class="text-right">Action</th>
	            </tr>
	        </thead>
	        <tbody>
	<?php
	    while( $obj = mysqli_fetch_assoc( $r )) {
	        $id = $obj['id'];
	        $vehicle = $obj['vehicle'];
	        $location = $obj['location'];
	        // $location1 = getLocationName1($location, $user_center);
	        $fuel_type = $obj['FuelDescription'];
	        // $fuel_type1 = getFuelName($fuel_type, $user_center);
	        $totalcost = $obj['totalcost'];
	        $quantity = $obj['quantity'];
	        $odometer = $obj['odometer'];
	        $fueltime = $obj['fueltime'];
	        $dates = $obj['dates'];
	        if (isset($dates) && !empty($dates)) {
	        	$x = strtotime($dates);
			    $datefrom = date('d M Y', $x);
	        }
	        else{
	        	$datefrom = '';
	        }
	        ?>
	            <tr id='<?php echo $id; ?>'>
	                <td style='width: 10%; text-transform: uppercase;'><?php echo $vehicle; ?></td>
	                <td style='width: 13%;'><?php echo $datefrom; ?></td>
	                <td style='width: 10%;'><?php echo $fueltime; ?></td>
	                <td style='width: 8%;'><?php echo $odometer; ?></td>
	                <td style='width: 18%;'><?php echo $location ?></td>
	                <td style='width: 10%;'><?php echo $fuel_type; ?></td>
	                <td style='width: 8%;'><?php echo $quantity; ?></td>
	                <td style='width: 8%;'><?php echo number_format($totalcost, 2); ?></td>
	                <td class="text-right" style='width: 11%;'>
	                    <span class='issueID' data='<?php echo $vehicle; ?>' id='<?php echo $id; ?>' page='<?php echo $gg; ?>' type='<?php echo $uid; ?>'>
	                        <button class='btn btn-info editable-table-button btn-xs editIssue'>Edit</button>
	                        <?php
	                            if ($uid == 1) {
	                                ?>
	                                    <button class='btn btn-danger editable-table-button btn-xs deleteIssue'>Del</button>
	                                <?php
	                            }
	                        ?>
	                    </span>
	                </td>
	            </tr>
	        <?php
	    }
	    ?>
	    </tbody></table></div>
	    <div class='row'>
	        <div class='col-sm-12'>
	            <div class='col-sm-7 col-md-offset-2 text-center'>
				    <?php
				    echo paginate_function($item_per_page, $page_number, $get_total_rows, $total_pages);
				    ?>
		        </div>
		        <div class="col-sm-3 text-right m-t-15">
		        	<button type="button" style="margin-right: -8px;" class="btn btn-sm btn-github waves-effect waves-light">
	                    <span class="btn-label"><i class="ti-paint-bucket"></i> </span><?php echo number_format($get_total_rows); ?>
	                </button>
		        </div>
		    </div>
		</div>
	    <?php
	}
}


// load all Booked for a given period in pagination
function allRentals($page,$user_email,$uid,$user_center){
	global $dbc;
	$item_per_page = 20;
	if($user_email && $uid){
	    // $uid = $_SESSION['id'];
	    if($page != ""){
	        $page_number = $page;
	        if(!is_numeric($page_number)){
	            $page_number = 1;
	        } //incase of invalid page number
	    }else{
	        $page_number = 1; //if there's no page number, set it to 1
	    }
	    //get total number of records from database for pagination
	    $results = "SELECT id FROM Rentals Where id !='' AND status = '1' AND costCenter = '$user_center'";
	    $results = mysqli_query($dbc, $results);
	    $get_total_rows = mysqli_num_rows($results);
	    //break records into pages
	    $total_pages = ceil($get_total_rows/$item_per_page);

	    //get starting position to fetch the records
	    $page_position = (($page_number-1) * $item_per_page);

	    $q = "SELECT * FROM Rentals WHERE costCenter = '$user_center' AND status = '1' ORDER BY dateto DESC LIMIT $page_position, $item_per_page";
	    $r = mysqli_query($dbc, $q);
	    ?>
	<div class="table-responsive">
	    <table class="table table-hover mails m-0 table table-actions-bar">
	        <thead>
	            <tr>
	                <th>No.</th>
	                <th>vehicle</th>
                    <th>Date from</th>
                    <th>Date to</th>
                    <th>Driver</th>
                    <th>Destination</th>
                    <th>Department</th>
                    <th>Days</th>
                    <th class="text-right">Action</th>
	            </tr>
	        </thead>
	        <tbody>
	<?php
	    while( $obj = mysqli_fetch_assoc( $r )) {
	        $id = $obj['id'];
	        $vehicle = $obj['vehicle'];
	        $datefrom = $obj['datefrom'];
	        $dateto = $obj['dateto'];
	        $department = $obj['department'];
	        $destination = $obj['destination'];
	        $rentalnumber = $obj['rentalnumber'];
	        $department1 = getDepartmentName($department, $user_center);
	        $driver = $obj['driver'];
	        $driver1 = getDriverName($driver, $user_center);
	        $difference = booking_time_difference($datefrom, $dateto);

	        if ($difference == '0 Sec') {
	        	$resultDays = "1 Day";
	        }else{
	        	$resultDays = $difference;
	        }

	        if ($obj['status'] == 1) {
                $state = "BOOKED";
            }else {
                $state = "UNBOOKED";
            }

	        if (isset($datefrom) && !empty($datefrom)) {
	        	$x = strtotime($datefrom);
			    $datefrom1 = date('d M Y', $x);
	        }
	        else{
	        	$datefrom1 = "";
	        }

	        if (isset($dateto) && !empty($dateto)) {
	        	$x = strtotime($dateto);
			    $dateto1 = date('d M Y', $x);
	        }
	        else{
	        	$dateto1 = "";
	        }


	        ?>
	            <tr id='<?php echo $id; ?>'>
	                <td style='width: 10%; text-transform: uppercase;'><?php echo $rentalnumber; ?></td>
	                <td style='width: 10%; text-transform: uppercase;'><?php echo $vehicle; ?></td>
	                <td style='width: 10%;'><?php echo $datefrom1; ?></td>
	                <td style='width: 10%;'><?php echo $dateto1; ?></td>
	                <td style='width: 17%;'><?php echo $driver1; ?></td>
	                <td style='width: 15%;'><?php echo $destination ?></td>
	                <td style='width: 10%;'><?php echo $department1 ?></td>
	                <td style='width: 7%;'><?php echo $resultDays ?></td>
	                <td class="text-right" style='width: 11%;'>
	                    <span class='rentalID' data='<?php echo $vehicle; ?>' id='<?php echo $id; ?>' page='<?php echo $gg; ?>' type='<?php echo $uid; ?>'>
	                        <button class='btn btn-info editable-table-button btn-xs editRental'>Edit</button>
	                        <?php
	                            if ($uid == 1) {
	                                ?>
	                                    <button class='btn btn-danger editable-table-button btn-xs deleterental'>Del</button>
	                                <?php
	                            }
	                        ?>
	                    </span>
	                </td>
	            </tr>
	        <?php
	    }
	    ?>
	    </tbody></table></div>
	    <div class='row'>
	        <div class='col-sm-12'>
	            <div class='col-sm-7 col-md-offset-2 text-center'>
	    <?php
	    echo paginate_function($item_per_page, $page_number, $get_total_rows, $total_pages);
	    ?>
	        </div></div></div>
	    <?php
	}
}


// load all available for a given period in pagination
function allAvailable($page,$user_email,$uid,$user_center){
	global $dbc;
	$item_per_page = 20;
	if($user_email && $uid){
	    // $uid = $_SESSION['id'];
	    if($page != ""){
	        $page_number = $page;
	        if(!is_numeric($page_number)){
	            $page_number = 1;
	        } //incase of invalid page number
	    }else{
	        $page_number = 1; //if there's no page number, set it to 1
	    }
	    //get total number of records from database for pagination
	    $results = "SELECT id FROM Rentals Where id !='' AND status = '0' AND costCenter = '$user_center'";
	    $results = mysqli_query($dbc, $results);
	    $get_total_rows = mysqli_num_rows($results);
	    //break records into pages
	    $total_pages = ceil($get_total_rows/$item_per_page);

	    //get starting position to fetch the records
	    $page_position = (($page_number-1) * $item_per_page);

	    $q = "SELECT * FROM Rentals WHERE costCenter = '$user_center' AND status = '0' ORDER BY dateto DESC LIMIT $page_position, $item_per_page";
	    $r = mysqli_query($dbc, $q);
	    ?>
	<div class="table-responsive">
	    <table class="table table-hover mails m-0 table table-actions-bar">
	        <thead>
	            <tr>
	                <th>vehicle</th>
                    <th>Date from</th>
                    <th>Date to</th>
                    <th>Driver</th>
                    <th>Destination</th>
                    <th>Days</th>
                    <th>Status</th>
                    <th class="text-right">Action</th>
	            </tr>
	        </thead>
	        <tbody>
	<?php
	    while( $obj = mysqli_fetch_assoc( $r )) {
	        $id = $obj['id'];
	        $vehicle = $obj['vehicle'];
	        $datefrom = $obj['datefrom'];
	        $dateto = $obj['dateto'];
	        $department = $obj['department'];
	        $destination = $obj['destination'];
	        // $department1 = getDepartmentName($department, $user_center);
	        $driver = $obj['driver'];
	        $driver1 = getDriverName($driver, $user_center);
	        $difference = booking_time_difference($datefrom, $dateto);

	        if ($difference == '0 Sec') {
	        	$resultDays = "1 Day";
	        }else{
	        	$resultDays = $difference;
	        }

	        if ($obj['status'] == 1) {
                $state = "BOOKED";
            }else {
                $state = "UNBOOKED";
            }

	        if (isset($datefrom) && !empty($datefrom)) {
	        	$x = strtotime($datefrom);
			    $datefrom1 = date('d M Y', $x);
	        }
	        else{
	        	$datefrom1 = "";
	        }

	        if (isset($dateto) && !empty($dateto)) {
	        	$x = strtotime($dateto);
			    $dateto1 = date('d M Y', $x);
	        }
	        else{
	        	$dateto1 = "";
	        }


	        ?>
	            <tr id='<?php echo $id; ?>'>
	                <td style='width: 10%; text-transform: uppercase;'><?php echo $vehicle; ?></td>
	                <td style='width: 10%;'><?php echo $datefrom1; ?></td>
	                <td style='width: 10%;'><?php echo $dateto1; ?></td>
	                <td style='width: 17%;'><?php echo $driver1; ?></td>
	                <td style='width: 20%;'><?php echo $destination ?></td>
	                <td style='width: 10%;'><?php echo $resultDays ?></td>
	                <td style='width: 10%;'><?php echo $state ?></td>
	                <td class="text-right" style='width: 11%;'>
	                    <span class='rentalID' data='<?php echo $vehicle; ?>' id='<?php echo $id; ?>' page='<?php echo $gg; ?>' type='<?php echo $uid; ?>'>
	                        <button class='btn btn-info editable-table-button btn-xs editRental'>Edit</button>
	                        <?php
	                            if ($uid == 1) {
	                                ?>
	                                    <button class='btn btn-danger editable-table-button btn-xs deleterental'>Del</button>
	                                <?php
	                            }
	                        ?>
	                    </span>
	                </td>
	            </tr>
	        <?php
	    }
	    ?>
	    </tbody></table></div>
	    <div class='row'>
	        <div class='col-sm-12'>
	            <div class='col-sm-7 col-md-offset-2 text-center'>
	    <?php
	    echo paginate_function($item_per_page, $page_number, $get_total_rows, $total_pages);
	    ?>
	        </div></div></div>
	    <?php
	}
}


// load fuel issuance in pagination
function allDoneTasks($page,$user_email,$uid,$user_center){
	global $dbc;
	$item_per_page = 20;
	if($user_email && $uid){
	    // $uid = $_SESSION['id'];
	    if($page != ""){
	        $page_number = $page;
	        if(!is_numeric($page_number)){
	            $page_number = 1;
	        } //incase of invalid page number
	    }else{
	        $page_number = 1; //if there's no page number, set it to 1
	    }
	    //get total number of records from database for pagination
	    $results = "SELECT id FROM Tasks Where id !='' AND status = '0' AND costCenter = '$user_center'";
	    $results = mysqli_query($dbc, $results);
	    $get_total_rows = mysqli_num_rows($results);
	    //break records into pages
	    $total_pages = ceil($get_total_rows/$item_per_page);

	    //get starting position to fetch the records
	    $page_position = (($page_number-1) * $item_per_page);

	    $q = "SELECT * FROM Tasks WHERE costCenter = '$user_center' AND status = '0' ORDER BY date_scheduled DESC LIMIT $page_position, $item_per_page";
	    $r = mysqli_query($dbc, $q);
	    ?>
	<div class="table-responsive">
	    <table class="table table-hover mails m-0 table table-actions-bar">
	        <thead>
	            <tr>
	                <th>vehicle</th>
                    <th>Date Scheduled</th>
                    <th>Date Added</th>
                    <th>Task</th>
                    <th>Description</th>
                    <th>Status</th>
                    <th class="text-right">Action</th>
	            </tr>
	        </thead>
	        <tbody>
	<?php
	    while( $obj = mysqli_fetch_assoc( $r )) {
	        $id = $obj['id'];
	        $vehicle = $obj['vehicle'];
	        $date_scheduled = $obj['date_scheduled'];
	        $date_added = $obj['date_added'];
	        $task_name = getTaskDesc($obj['task_name'], $user_center);
	        $task_description = $obj['task_description'];

	        if ($obj['status'] == 1) {
                $state = "Not Completed";
            }else {
                $state = "Completed";
            }

	        if (isset($date_scheduled) && !empty($date_scheduled)) {
	        	$x = strtotime($date_scheduled);
			    $datefrom1 = date('d M Y', $x);
	        }
	        else{
	        	$datefrom1 = "";
	        }

	        if (isset($date_added) && !empty($date_added)) {
	        	$x = strtotime($date_added);
			    $dateto1 = date('d M Y', $x);
	        }
	        else{
	        	$dateto1 = "";
	        }

	        ?>
	            <tr id='<?php echo $id; ?>'>
	                <td style='width: 10%; text-transform: uppercase;'><?php echo $vehicle; ?></td>
	                <td style='width: 10%;'><?php echo $datefrom1; ?></td>
	                <td style='width: 10%;'><?php echo $dateto1; ?></td>
	                <td style='width: 15%;'><?php echo $task_name; ?></td>
	                <td style='width: 30%;'><?php echo $task_description ?></td>
	                <td style='width: 10%;'><?php echo $state ?></td>
	                <td class="text-right" style='width: 11%;'>
	                    <span class='rentalID' data='<?php echo $vehicle; ?>' id='<?php echo $id; ?>' page='<?php echo $gg; ?>' type='<?php echo $uid; ?>'>
	                        <button class='btn btn-info editable-table-button btn-xs editRental'>View</button>
	                        <?php
	                            if ($uid == 1) {
	                                ?>
	                                    <button class='btn btn-danger editable-table-button btn-xs deleterental'>Del</button>
	                                <?php
	                            }
	                        ?>
	                    </span>
	                </td>
	            </tr>
	        <?php
	    }
	    ?>
	    </tbody></table></div>
	    <div class='row'>
	        <div class='col-sm-12'>
	            <div class='col-sm-7 col-md-offset-2 text-center'>
	    <?php
	    echo paginate_function($item_per_page, $page_number, $get_total_rows, $total_pages);
	    ?>
	        </div></div></div>
	    <?php
	}
}



// load fuel issuance in pagination
function allClaims($page,$user_email,$uid,$userCcenter){
	global $dbc;
	$item_per_page = 20;
	if($user_email && $uid){
	    // $uid = $_SESSION['id'];
	    if($page != ""){
	        $page_number = $page;
	        if(!is_numeric($page_number)){
	            $page_number = 1;
	        } //incase of invalid page number
	    }else{
	        $page_number = 1; //if there's no page number, set it to 1
	    }
	    //get total number of records from database for pagination
	    $results = "SELECT id FROM Claime Where id !='' AND costCenter = '$userCcenter'";
	    $results = mysqli_query($dbc, $results);
	    $get_total_rows = mysqli_num_rows($results);
	    //break records into pages
	    $total_pages = ceil($get_total_rows/$item_per_page);

	    //get starting position to fetch the records
	    $page_position = (($page_number-1) * $item_per_page);

	    $q = "SELECT c.id AS 'id',
	    		c.invoice AS 'invoice',
                c.vehicle AS 'vehicle',
                c.department AS 'department',
                c.destination AS 'destination',
                c.startdate AS 'startdate',
                c.enddate AS 'enddate',
                c.claimer AS 'claimer',
                c.claimdate AS 'claimdate',
                c.kilometers AS 'kilometers',
                c.remarks AS 'remarks',
                c.addedby AS 'addedby',
                c.dateadded AS 'dateadded',
                c.costCenter AS 'costCenter',
                (r.rateperkm * c.kilometers) + c.amount AS 'amount'
                FROM Claime AS c
                JOIN Contracted AS r ON c.vehicle = r.vehicle
                WHERE c.costCenter = '$userCcenter' ORDER BY c.claimdate DESC LIMIT $page_position, $item_per_page";
	    // $q = "SELECT * FROM Claime WHERE costCenter = '$userCcenter' ORDER BY claimdate DESC LIMIT $page_position, $item_per_page";
	    $r = mysqli_query($dbc, $q);
	    ?>
	<div class="table-responsive">
	    <table class="table table-hover mails m-0 table table-actions-bar">
	        <thead>
	            <tr>
	                <th>Vehicle</th>
                    <th>Department</th>
                    <th>Claim Date</th>
                    <th>Claimer</th>
                    <th>Invoice</th>
                    <th>KMS</th>
                    <th>Start Date</th>
                    <th>End Date</th>
                    <th>Amount</th>
                    <th class="text-right">Action</th>
	            </tr>
	        </thead>
	        <tbody>
	<?php
	    while( $obj = mysqli_fetch_assoc( $r )) {
	        $id = $obj['id'];
	        $vehicle = $obj['vehicle'];
	        $department = $obj['department'];
	        $department1 = getDepartmentName($department, $userCcenter);
	        $claimdate = $obj['claimdate'];
	        $claimer = $obj['claimer'];
	        $invoice = $obj['invoice'];
	        $kilometers = $obj['kilometers'];
	        $amount = $obj['amount'];

	        $startdate = $obj['startdate'];
	        $enddate = $obj['enddate'];

	        if (isset($claimdate) && !empty($claimdate)) {
	        	$x = strtotime($claimdate);
			    $claimdate1 = date('d M Y', $x);
	        }
	        else{
	        	$claimdate1 = "";
	        }

	        if (isset($startdate) && !empty($startdate)) {
	        	$x = strtotime($startdate);
			    $startdate1 = date('d M Y', $x);
	        }
	        else{
	        	$startdate1 = "";
	        }

	        if (isset($enddate) && !empty($enddate)) {
	        	$x = strtotime($enddate);
			    $enddate1 = date('d M Y', $x);
	        }
	        else{
	        	$enddate1 = "";
	        }

	        ?>
	            <tr id='<?php echo $id; ?>'>
	                <td style='width: 8%; text-transform: uppercase;'><?php echo $vehicle; ?></td>
	                <td style='width: 12%;'><?php echo $department1; ?></td>
	                <td style='width: 10%;'><?php echo $claimdate1; ?></td>
	                <td style='width: 15%;'><?php echo $claimer; ?></td>
	                <td style='width: 10%;'><?php echo $invoice ?></td>
	                <td style='width: 8%;'><?php echo $kilometers ?></td>
	                <td style='width: 10%;'><?php echo $startdate1 ?></td>
	                <td style='width: 10%;'><?php echo $enddate1 ?></td>
	                <td style='width: 8%;'><?php echo number_format($amount, 2) ?></td>
	                <td class="text-right" style='width: 13%;'>
	                    <span class='claimID' data='<?php echo $vehicle; ?>' id='<?php echo $id; ?>' page='<?php echo $gg; ?>' type='<?php echo $uid; ?>'>
	                        <button class='btn btn-info editable-table-button btn-xs editClaim'>Edit</button>
	                        <?php
	                            if ($uid == 1) {
	                                ?>
	                                    <button class='btn btn-danger editable-table-button btn-xs deleteClaim'>Del</button>
	                                <?php
	                            }
	                        ?>
	                    </span>
	                </td>
	            </tr>
	        <?php
	    }
	    ?>
	    </tbody></table></div>
	    <div class='row'>
	        <div class='col-sm-12'>
	            <div class='col-sm-7 col-md-offset-2 text-center'>
	    <?php
	    echo paginate_function($item_per_page, $page_number, $get_total_rows, $total_pages);
	    ?>
	        </div></div></div>
	    <?php
	}
}

// load fuel issuance in pagination
function allContracted($page,$user_email,$uid,$center){
	global $dbc;
	$item_per_page = 20;
	if($user_email && $uid){
	    // $uid = $_SESSION['id'];
	    if($page != ""){
	        $page_number = $page;
	        if(!is_numeric($page_number)){
	            $page_number = 1;
	        } //incase of invalid page number
	    }else{
	        $page_number = 1; //if there's no page number, set it to 1
	    }
	    //get total number of records from database for pagination
	    $results = "SELECT id FROM Contracted Where id !='' AND costCenter = '$center'";
	    $results = mysqli_query($dbc, $results);
	    $get_total_rows = mysqli_num_rows($results);
	    //break records into pages
	    $total_pages = ceil($get_total_rows/$item_per_page);

	    //get starting position to fetch the records
	    $page_position = (($page_number-1) * $item_per_page);

	    $q = "SELECT * FROM Contracted WHERE costCenter = '$center' ORDER BY id DESC LIMIT $page_position, $item_per_page";
	    $r = mysqli_query($dbc, $q);
	    ?>
	<div class="table-responsive">
	    <table class="table table-hover mails m-0 table table-actions-bar">
	        <thead>
	            <tr>
	                <th>vehicle</th>
                    <th>make</th>
                    <th>model</th>
                    <th>Owner</th>
                    <th>Date added</th>
                    <th class="text-right">Action</th>
	            </tr>
	        </thead>
	        <tbody>
	<?php
	    while( $obj = mysqli_fetch_assoc( $r )) {
	    	date_default_timezone_set("Africa/Nairobi");
	        $id = $obj['id'];
	        $vehicle = $obj['vehicle'];
	        $make = $obj['make'];
	        $model = $obj['model'];
	        $owner = $obj['owner'];

	        $datecreated = $obj['datecreated'];

	        if (isset($datecreated) && !empty($datecreated)) {
	        	$x = strtotime($datecreated);
			    $datefrom1 = date('d M Y', $x);
	        }
	        else{
	        	$datefrom1 = "";
	        }

	        ?>
	            <tr id='<?php echo $id; ?>'>
	                <td style='width: 10%; text-transform: uppercase;'><?php echo $vehicle; ?></td>
	                <td style='width: 10%;'><?php echo $make; ?></td>
	                <td style='width: 10%;'><?php echo $model; ?></td>
	                <td style='width: 20%;'><?php echo $owner; ?></td>
	                <td style='width: 20%;'><?php echo $datefrom1 ?></td>
	                <td class="text-right" style='width: 11%;'>
	                    <span class='rentalID' data='<?php echo $vehicle; ?>' id='<?php echo $id; ?>' page='<?php echo $gg; ?>' type='<?php echo $uid; ?>'>
	                        <button class='btn btn-info editable-table-button btn-xs editContracted'>Edit</button>
	                        <?php
	                            if ($uid == 1) {
	                                ?>
	                                    <button class='btn btn-danger editable-table-button btn-xs deleterental'>Del</button>
	                                <?php
	                            }
	                        ?>
	                    </span>
	                </td>
	            </tr>
	        <?php
	    }
	    ?>
	    </tbody></table></div>
	    <div class='row'>
	        <div class='col-sm-12'>
	            <div class='col-sm-7 col-md-offset-2 text-center'>
	    <?php
	    echo paginate_function($item_per_page, $page_number, $get_total_rows, $total_pages);
	    ?>
	        </div></div></div>
	    <?php
	}
}

function allStockReturns($page,$user_email,$uid,$user_center){
	global $dbc;
	$item_per_page = 20;
	if($user_email && $uid){
	    // $uid = $_SESSION['id'];
	    if($page != ""){
	        $page_number = $page;
	        if(!is_numeric($page_number)){
	            $page_number = 1;
	        } //incase of invalid page number
	    }else{
	        $page_number = 1; //if there's no page number, set it to 1
	    }
	    //get total number of records from database for pagination
	    $results = "SELECT id FROM StockReturn Where id !='' AND CostCenter = '$user_center'";
	    $results = mysqli_query($dbc, $results);
	    $get_total_rows = mysqli_num_rows($results);
	    //break records into pages
	    $total_pages = ceil($get_total_rows/$item_per_page);

	    //get starting position to fetch the records
	    $page_position = (($page_number-1) * $item_per_page);

	    $q = "SELECT * FROM StockReturn WHERE CostCenter = '$user_center' ORDER BY id DESC LIMIT $page_position, $item_per_page";
	    $r = mysqli_query($dbc, $q);
	    ?>
	<div class="table-responsive">
	    <table class="table table-hover mails m-0 table table-actions-bar">
	        <thead>
	            <tr>
	                <th>vehicle</th>
                    <th>Date returned</th>
                    <th>Job No.</th>
                    <th>Prev Cost</th>
                    <th>New Cost</th>
                    <th>Part</th>
                    <th>Prev Qty</th>
                    <th>New Qty</th>
	            </tr>
	        </thead>
	        <tbody>
	<?php
	    while( $obj = mysqli_fetch_assoc( $r )) {
	        $id = $obj['id'];
	        $vehicle = $obj['VID'];
	        $datefrom = $obj['dateAdjusted'];
	        $JobNumber = $obj['JobNumber'];
	        $priviousCost = $obj['priviousCost'];
	        $NewCost = $obj['NewCost'];
	        $department1 = getDepartmentName($Department, $user_center);
	        $PartNumber = $obj['PartNumber'];
	        $NewQty = $obj['NewQty'];
	        $previousQty = $obj['previousQty'];

	        if (isset($datefrom) && !empty($datefrom)) {
	        	$x = strtotime($datefrom);
			    $datefrom1 = date('d M Y', $x);
	        }
	        else{
	        	$datefrom1 = "";
	        }

	        ?>
	            <tr id='<?php echo $id; ?>'>
	                <td style='width: 10%; text-transform: uppercase;'><?php echo $vehicle; ?></td>
	                <td style='width: 15%;'><?php echo $datefrom1; ?></td>
	                <td style='width: 10%;'><?php echo $JobNumber; ?></td>
	                <td style='width: 10%;'><?php echo number_format($priviousCost, 2); ?></td>
	                <td style='width: 10%;'><?php echo number_format($NewCost, 2); ?></td>
	                <td style='width: 20%;'><?php echo $PartNumber; ?></td>
	                <td style='width: 11%;'><?php echo $previousQty; ?></td>
	                <td style='width: 11%;'><?php echo $NewQty; ?></td>
	            </tr>
	        <?php
	    }
	    ?>
	    </tbody></table></div>
	    <div class='row'>
	        <div class='col-sm-12'>
	            <div class='col-sm-7 col-md-offset-2 text-center'>
	    <?php
	    echo paginate_function($item_per_page, $page_number, $get_total_rows, $total_pages);
	    ?>
	        </div></div></div>
	    <?php
	}
}


// load fuel issuance in pagination
function allReturns($page,$user_email,$uid,$user_center){
	global $dbc;
	$item_per_page = 20;
	if($user_email && $uid){
	    // $uid = $_SESSION['id'];
	    if($page != ""){
	        $page_number = $page;
	        if(!is_numeric($page_number)){
	            $page_number = 1;
	        } //incase of invalid page number
	    }else{
	        $page_number = 1; //if there's no page number, set it to 1
	    }
	    //get total number of records from database for pagination
	    $results = "SELECT id FROM Returns Where id !='' AND costCenter = '$user_center'";
	    $results = mysqli_query($dbc, $results);
	    $get_total_rows = mysqli_num_rows($results);
	    //break records into pages
	    $total_pages = ceil($get_total_rows/$item_per_page);

	    //get starting position to fetch the records
	    $page_position = (($page_number-1) * $item_per_page);

	    $q = "SELECT * FROM Returns WHERE costCenter = '$user_center' ORDER BY id DESC LIMIT $page_position, $item_per_page";
	    $r = mysqli_query($dbc, $q);
	    ?>
	<div class="table-responsive">
	    <table class="table table-hover mails m-0 table table-actions-bar">
	        <thead>
	            <tr>
	                <th>vehicle</th>
                    <th>Date from</th>
                    <th>department</th>
                    <th>Amount</th>
                    <th>Kilometers</th>
                    <th class="text-right">Action</th>
	            </tr>
	        </thead>
	        <tbody>
	<?php
	    while( $obj = mysqli_fetch_assoc( $r )) {
	        $id = $obj['id'];
	        $vehicle = $obj['Vehicle'];
	        $datefrom = $obj['Datefrom'];
	        $Department = $obj['Department'];
	        $Amount = $obj['Amount'];
	        $department1 = getDepartmentName($Department, $user_center);
	        $Kilometersdone = $obj['Kilometersdone'];

	        if (isset($datefrom) && !empty($datefrom)) {
	        	$x = strtotime($datefrom);
			    $datefrom1 = date('d M Y', $x);
	        }
	        else{
	        	$datefrom1 = "";
	        }

	        ?>
	            <tr id='<?php echo $id; ?>'>
	                <td style='width: 10%; text-transform: uppercase;'><?php echo $vehicle; ?></td>
	                <td style='width: 10%;'><?php echo $datefrom1; ?></td>
	                <td style='width: 10%;'><?php echo $department1; ?></td>
	                <td style='width: 20%;'><?php echo number_format($Amount, 2); ?></td>
	                <td style='width: 20%;'><?php echo number_format($Kilometersdone, 2); ?></td>
	                <td class="text-right" style='width: 11%;'>
	                    <span class='rerurnID' data='<?php echo $vehicle; ?>' id='<?php echo $id; ?>' page='<?php echo $gg; ?>' type='<?php echo $uid; ?>'>
	                        <button class='btn btn-info editable-table-button btn-xs editReturn'>Edit</button>
	                        <?php
	                            if ($uid == 1) {
	                                ?>
	                                    <button class='btn btn-danger editable-table-button btn-xs deleteReturn'>Del</button>
	                                <?php
	                            }
	                        ?>
	                    </span>
	                </td>
	            </tr>
	        <?php
	    }
	    ?>
	    </tbody></table></div>
	    <div class='row'>
	        <div class='col-sm-12'>
	            <div class='col-sm-7 col-md-offset-2 text-center'>
	    <?php
	    echo paginate_function($item_per_page, $page_number, $get_total_rows, $total_pages);
	    ?>
	        </div></div></div>
	    <?php
	}
}


// load fuel issuance in pagination
function next_vehicle_issues($vehicle,$page,$uid,$user_center){
	global $dbc;
	$item_per_page = 5;
	if($uid){
	    // $uid = $_SESSION['id'];
	    if($page != ""){
	        $page_number = $page;
	        if(!is_numeric($page_number)){
	            $page_number = 1;
	        } //incase of invalid page number
	    }else{
	        $page_number = 1; //if there's no page number, set it to 1
	    }
	    //get total number of records from database for pagination
	    $results = "SELECT id FROM ISSUANCE WHERE vehicle = '$vehicle' AND costcenter = '$user_center'";
	    $results = mysqli_query($dbc, $results);
	    $get_total_rows = mysqli_num_rows($results);
	    //break records into pages
	    $total_pages = ceil($get_total_rows/$item_per_page);

	    //get starting position to fetch the records
	    $page_position = (($page_number-1) * $item_per_page);

	    $q = "SELECT id, vehicle, location, fuel_type, dates, odometer, quantity, totalcost FROM ISSUANCE WHERE vehicle = '$vehicle' AND costcenter = '$user_center' ORDER BY dates DESC LIMIT $page_position, $item_per_page";
	    $r = mysqli_query($dbc, $q);
	    ?>
	<div class="table-responsive">
	    <table class="table table-hover mails m-0 table table-actions-bar">
	        <thead>
	            <tr>
	                <th>Vehicle</th>
                    <th>Date</th>
                    <th>Odometer</th>
                    <th>Location</th>
                    <th>Fuel type</th>
                    <th>Amount</th>
                    <th>Total</th>
	            </tr>
	        </thead>
	        <tbody>
	<?php
	    while( $obj = mysqli_fetch_assoc( $r )) {
	        $id = $obj['id'];
	        $vehicle = $obj['vehicle'];
	        $location = $obj['location'];
	        $location1 = getLocationName1($location, $user_center);
	        $fuel_type = $obj['fuel_type'];
	        $fuel_type1 = getFuelName($fuel_type, $user_center);
	        $dates = $obj['dates'];
	        $odometer = $obj['odometer'];
	        $quantity = $obj['quantity'];
	        $totalcost = $obj['totalcost'];
	        ?>
	            <tr id='<?php echo $id; ?>'>
	                <td style='width: 13%; text-transform: uppercase;'><?php echo $vehicle; ?></td>
	                <td style='width: 13%;'><?php echo $dates; ?></td>
	                <td style='width: 13%;'><?php echo $odometer; ?></td>
	                <td style='width: 17%;'><?php echo $location1; ?></td>
	                <td style='width: 15%;'><?php echo $fuel_type1; ?></td>
	                <td style='width: 15%;'><?php echo $quantity; ?></td>
	                <td style='width: 15%;'><?php echo $totalcost; ?></td>
	            </tr>
	        <?php
	    }
	    ?>
	    </tbody></table></div>
	    <div class='row'>
	        <div class='col-sm-12'>
	            <div class='col-sm-7 col-md-offset-2 text-center'>
	    <?php
	    echo paginate_results_function($item_per_page, $page_number, $get_total_rows, $total_pages);
	    ?>
	        </div></div></div>
	    <?php
	}
}

// load fuel issuance in pagination
function current_driver_assignment($uid, $driver, $center){
	global $dbc;
	if($uid){
	    //get total number of records from database for pagination
	    $q = "SELECT a.vehicle AS 'vehicle',
            a.date_asigned AS 'dateass',
            a.id AS 'ass_id',
            v.model AS 'model',
            v.vtype AS 'vtype',
            v.base AS 'base'
            FROM VEHICLE_ASIGN AS a
            LEFT JOIN VEHICLES AS v ON a.employee_number = v.driver
            WHERE a.employee_number = '$driver' AND a.costCenter = '$center'
            GROUP BY a.id
            ORDER BY a.id DESC";
	    $r = mysqli_query($dbc, $q);
	    ?>
	<div class="table-responsive">
	    <table class="table table-hover mails m-0 table table-actions-bar">
	        <thead>
	            <tr>
	                <th>Vehicle</th>
                    <th>Date asigned</th>
                    <th>Vehicle base</th>
                    <th>Vehicle model</th>
                    <th>Vehicle type</th>
	            </tr>
	        </thead>
	        <tbody>
	<?php
	    while( $obj = mysqli_fetch_assoc( $r )) {
	        $id = $obj['ass_id'];
	        $vehicle = $obj['vehicle'];
	        $dateass = $obj['dateass'];
	        $base = $obj['base'];
	        $base1 = getBaseName($base, $center);
	        $vtype = $obj['vtype'];
	        $vtype1 = getVehicleTypeName($vtype);
	        $model = $obj['model'];
	        $model1 = getModelName($model);
	        ?>
	            <tr id='<?php echo $id; ?>'>
	                <td style='width: 15%; text-transform: uppercase;'><?php echo $vehicle; ?></td>
	                <td style='width: 15%;'><?php echo $dateass; ?></td>
	                <td style='width: 20%;'><?php echo $base1; ?></td>
	                <td style='width: 20%;'><?php echo $model1; ?></td>
	                <td style='width: 20%;'><?php echo $vtype1; ?></td>
	            </tr>
	        <?php
	    }
	    ?>
	    </tbody></table></div>
	    <div class='row'>
	        <div class='col-sm-12'>
	            <div class='col-sm-7 col-md-offset-2 text-center'>
	        </div></div></div>
	    <?php
	}
}


// load fuel issuance in pagination
function current_vehicle_issues($vehicle,$uid,$page,$center){
	global $dbc;
	// $page = "";
	$item_per_page = 5;
	if($uid){
	    // $uid = $_SESSION['id'];
	    if($page != ""){
	        $page_number = $page;
	        if(!is_numeric($page_number)){
	            $page_number = 1;
	        } //incase of invalid page number
	    }else{
	        $page_number = 1; //if there's no page number, set it to 1
	    }
	    //get total number of records from database for pagination
	    $results = "SELECT id FROM ISSUANCE WHERE vehicle = '$vehicle' AND costcenter = '$center'";
	    $results = mysqli_query($dbc, $results);
	    $get_total_rows = mysqli_num_rows($results);
	    //break records into pages
	    $total_pages = ceil($get_total_rows/$item_per_page);

	    //get starting position to fetch the records
	    $page_position = (($page_number-1) * $item_per_page);

	    $q = "SELECT id, vehicle, location, fuel_type, dates, odometer, quantity, totalcost FROM ISSUANCE WHERE vehicle = '$vehicle' AND costcenter = '$center' ORDER BY dates DESC LIMIT $page_position, $item_per_page";
	    $r = mysqli_query($dbc, $q);
	    ?>
	<div class="table-responsive">
	    <table class="table table-hover mails m-0 table table-actions-bar">
	        <thead>
	            <tr>
	                <th>Vehicle</th>
                    <th>Date</th>
                    <th>Odometer</th>
                    <th>Location</th>
                    <th>Fuel type</th>
                    <th>Amount</th>
                    <th>Total</th>
	            </tr>
	        </thead>
	        <tbody>
	<?php
	    while( $obj = mysqli_fetch_assoc( $r )) {
	    	date_default_timezone_set("Africa/Nairobi");
	        $id = $obj['id'];
	        $vehicle = $obj['vehicle'];
	        $location = $obj['location'];
	        $location1 = getLocationName1($location, $center);
	        $fuel_type = $obj['fuel_type'];
	        $fuel_type1 = getFuelName($fuel_type, $center);
	        // $dates = $obj['dates'];

	        if (isset($obj['dates']) && !empty($obj['dates'])) {
                $month = strtotime($obj['dates']);
                $dates = date('d M Y', $month);
            }
            else{
            	$dates = "";
            }

	        $odometer = $obj['odometer'];
	        $quantity = $obj['quantity'];
	        $totalcost = $obj['totalcost'];
	        ?>
	            <tr id='<?php echo $id; ?>'>
	                <td style='width: 13%; text-transform: uppercase;'><?php echo $vehicle; ?></td>
	                <td style='width: 13%;'><?php echo $dates; ?></td>
	                <td style='width: 13%;'><?php echo $odometer; ?></td>
	                <td style='width: 17%;'><?php echo $location1; ?></td>
	                <td style='width: 15%;'><?php echo $fuel_type1; ?></td>
	                <td style='width: 15%;'><?php echo $quantity; ?></td>
	                <td style='width: 15%;'><?php echo $totalcost; ?></td>
	            </tr>
	        <?php
	    }
	    ?>
	    </tbody></table></div>
	    <div class='row'>
	        <div class='col-sm-12'>
	            <div class='col-sm-7 col-md-offset-2 text-center'>
	    <?php
	    echo paginate_results_function($item_per_page, $page_number, $get_total_rows, $total_pages);
	    ?>
	        </div></div></div>
	    <?php
	}
}


function paginate_results_function($item_per_page, $current_page, $total_records, $total_pages)
{
    $pagination = '';
    if($total_pages > 0 && $total_pages != 1 && $current_page <= $total_pages){ //verify total pages and current page number
        $pagination .= '<ul class="pagination">';

        $right_links    = $current_page + 3;
        $previous       = $current_page - 1; //previous link
        $next           = $current_page + 1; //next link
        $first_link     = true; //boolean var to decide our first link

        if($current_page > 1){
            $previous_link = ($previous==0)? 1: $previous;
            //first link
            $pagination .= '<li class="paginate_button previous pagedata" data="1" aria-controls="datatable-buttons" tabindex="0" id="datatable-buttons_previous"><a class="nextRsultvhl">First</a></li>';
            //previous link
            $pagination .= '<li><span class="pagedata" data="'.$previous_link.'"><a class="nextRsultvhl">Previous</a></span></li>';

                for($i = ($current_page-2); $i < $current_page; $i++){ //Create left-hand side links
                    if($i > 0){
                        $pagination .= '<li><span class="pagedata" id="'.$i.'" data="'.$i.'"><a class="nextRsultvhl" data-page="'.$i.'" title="Page '.$i.'">'.$i.'</a></span></li>';
                    }
                }
            $first_link = false; //set first link to false
        }

        if($first_link){ //if current active page is first link
            $pagination .= '<li class="paginate_button previous disabled" aria-controls="datatable-buttons" tabindex="0" id="datatable-buttons_previous"><a>First</a></li>
            ';
        }elseif($current_page == $total_pages){ //if it's the last active link
            $pagination .= '<li class="paginate_button next disabled" aria-controls="datatable-buttons" tabindex="0" id="datatable-buttons_next"><a>End</a></li>';
        }else{ //regular current link
            $pagination .= '<li class="paginate_button active" aria-controls="datatable-buttons" tabindex="0"><a>'.$current_page.'</a></li>
            ';
        }

        for($i = $current_page+1; $i < $right_links ; $i++){ //create right-hand side links
            if($i<=$total_pages){
                $pagination .= '<li class="paginate_button pagedata" data="'.$i.'" aria-controls="datatable-buttons"><a class="nextRsultvhl">'.$i.'</a></li>';
            }
        }

        if($current_page < $total_pages){
                $next_link = ($next > $total_pages)? $total_pages: $next;
                //next link
                $pagination .= '<li class="paginate_button pagedata" data="'.$next_link.'" aria-controls="datatable-buttons"><a class="nextRsultvhl">Next</a></li>';
                //last link
                $pagination .= '<li class="paginate_button next pagedata" data="'.$total_pages.'" id="datatable-buttons_next"><a class="nextRsultvhl">Last</a></li>';
        }

        $pagination .= '</ul>';
    }
    return $pagination; //return pagination links
}

function vehicle_exists($reg1,$cost_cnt){
	global $dbc;
	$reg1 = sanitize($reg1);
	$q = "SELECT vid FROM VEHICLES WHERE vid = '$reg1' AND costcentre = '$cost_cnt'";
	$r = mysqli_query($dbc, $q);
	$query_num_rows = mysqli_num_rows($r);
	if($query_num_rows >= 1){
		return true;
	}
	elseif($query_num_rows == 0){
		return false;
	}
}

function update_vehicle_KM($vhl_numb, $new_kilo, $ccenter){
	global $dbc;
	$q = "UPDATE VEHICLES SET odometer = '$new_kilo' WHERE vid = '$vhl_numb' AND costcentre = '$ccenter'";
	$r = mysqli_query($dbc, $q);
	if($r){
		return true;
	}
	else{
		return false;
	}
}


function update_vehicle_diver($vehicle, $name, $ccenter){
	global $dbc;
	$vehicle = sanitize($vehicle);
	$name = sanitize($name);
	$q = "UPDATE VEHICLES SET driver = '$name' WHERE vid = '$vehicle' AND costcentre = '$ccenter'";
	$r = mysqli_query($dbc, $q);
	if($r){
		return true;
	}
	else{
		return false;
	}
}

function Driver_exists($emp_number, $idn, $licencenum){
	global $dbc;
	$reg1 = sanitize($reg1);
	$q = "SELECT * FROM DRIVER WHERE id_num = '$idn' OR employ_number = '$emp_number' OR licence_number = '$licencenum'";
	$r = mysqli_query($dbc, $q);
	$query_num_rows = mysqli_num_rows($r);
	if($query_num_rows >= 1){
		return true;
	}
	elseif($query_num_rows == 0){
		return false;
	}
}


function getUserNames($user_id){
	global $dbc;
	$q = "SELECT firstname, lastname FROM users WHERE id = '$user_id'";
	$data = mysqli_query($dbc, $q);
	while ($row = mysqli_fetch_assoc($data)):
		$name = $row['firstname'] ." ". $row['lastname'];
		return $name;
	endwhile;
}

function getCostingCodeName($code){
	global $dbc;
	$q = "SELECT description FROM cost_code WHERE code = '$code'";
	$data = mysqli_query($dbc, $q);
	while ($row = mysqli_fetch_assoc($data)):
		$name = $row['description'];
		return $name;
	endwhile;
}

function getStockCategoryName($code, $center){
	global $dbc;
	$q = "SELECT description FROM stockCategory WHERE code = '$code' AND costCenter = '$center'";
	$data = mysqli_query($dbc, $q);
	while ($row = mysqli_fetch_assoc($data)):
		$name = $row['description'];
		return $name;
	endwhile;
}


function getVehicleDepartment($vehicle, $costcenter){
	global $dbc;
	$q = "SELECT department FROM VEHICLES WHERE vid = '$vehicle' AND costcentre = '$costcenter'";
	$data = mysqli_query($dbc, $q);
	while ($row = mysqli_fetch_assoc($data)):
		$name = $row['department'];
		return $name;
	endwhile;
}

function getVehicleLocation($location, $costcenter){
	global $dbc;
	$q = "SELECT location FROM LOCATION WHERE code = '$location' AND costCenter = '$costcenter'";
	$data = mysqli_query($dbc, $q);
	while ($row = mysqli_fetch_assoc($data)):
		$name = $row['location'];
		return $name;
	endwhile;
}

function getModelServiceMilage($model){
	global $dbc;
	$q = "SELECT serviceInter FROM MODELS WHERE model = '$model' LIMIT 1";
	$data = mysqli_query($dbc, $q);
	while ($row = mysqli_fetch_assoc($data)):
		$serviceInter = $row['serviceInter'];
		return $serviceInter;
	endwhile;
}

function getVehicleCostCenter($vehicle){
	global $dbc;
	$q = "SELECT costcentre FROM VEHICLES WHERE vid = '$vehicle'";
	$data = mysqli_query($dbc, $q);
	while ($row = mysqli_fetch_assoc($data)):
		$name = $row['costcentre'];
		return $name;
	endwhile;
}

function getFuelName($fuel, $centre){
	global $dbc;
	$q = "SELECT fuel_type FROM FUEL_TYPE WHERE code = '$fuel' AND costCenter = '$centre'";
	$data = mysqli_query($dbc, $q);
	while ($row = mysqli_fetch_assoc($data)):
		$name = $row['fuel_type'];
		return $name;
	endwhile;
}

function getFuelUnitCost($fuel, $center){
	global $dbc;
	$q = "SELECT unit_cost FROM FUEL_TYPE WHERE code = '$fuel' AND costCenter = '$center'";
	$data = mysqli_query($dbc, $q);
	while ($row = mysqli_fetch_assoc($data)):
		$cost = $row['unit_cost'];
		return $cost;
	endwhile;
}

function getCardNumber($vehicle, $center){
	global $dbc;
	$q = "SELECT cardnumber FROM VEHICLES WHERE vid = '$vehicle' AND costcentre = '$center'";
	$data = mysqli_query($dbc, $q);
	while ($row = mysqli_fetch_assoc($data)):
		$card = $row['cardnumber'];
		return $card;
	endwhile;
}

function update_vhl_odo($vehicles, $odometer, $cost_center){
	global $dbc;
	$q = "SELECT odometer FROM VEHICLES WHERE vid = '$vehicles' AND costcentre = '$cost_center'";
	$data = mysqli_query($dbc, $q);
	while ($row = mysqli_fetch_assoc($data)):
		$Oldodo = $row['odometer'];
		if (($odometer > $Oldodo) || ($Oldodo == "")) {
			$sql = "UPDATE VEHICLES SET odometer = '$odometer' WHERE vid = '$vehicles' AND costcentre = '$cost_center'";
			$result = mysqli_query($dbc, $sql);
			if ($result) {
				return true;
			}
		}else{
			return true;
		}
	endwhile;
}

function update_workticket_odo($worknumber, $odoin, $ccenter){
	global $dbc;
	$q = "SELECT closeOdo FROM Workheader WHERE worknumber = '$worknumber' AND costCenter = '$ccenter'";
	$data = mysqli_query($dbc, $q);
	while ($row = mysqli_fetch_assoc($data)):
		$closeOdo = $row['closeOdo'];
		if ($odoin > $closeOdo) {
			$sql = "UPDATE Workheader SET closeOdo = '$odoin' WHERE worknumber = '$worknumber' AND costCenter = '$ccenter'";
			$result = mysqli_query($dbc, $sql);
			if ($result) {
				return true;
			}
		}else{
			return true;
		}
	endwhile;
}

function DifferentCardNumber($vehicles, $driver, $cost_center, $location, $fuel_type, $datefrom1, $odometer, $prevodo, $quantiy, $unit_cost, $total, $reference, $int_ext, $carnumber, $fueltime, $uid, $lastid){
	global $dbc;
	$q = "INSERT INTO Diferent_CardNumb (vehicle, driver, costcenter, location, fuel_type, dates, odometer, prev_odo, quantity, unit_cost, totalcost, reference, card_numebr, fueltime, int_ext, IssuanceId, added_by) VALUES ('$vehicles', '$driver', '$cost_center', '$location', '$fuel_type', '$datefrom1', '$odometer', '$prevodo', '$quantiy', '$unit_cost', '$total', '$reference', '$carnumber', '$fueltime', '$int_ext', '$lastid', '$uid')";
	$r = mysqli_query($dbc, $q);
	if($r){
		return true;
	}
}

function UpdateDifferentCardNumber($driver, $ccenter, $location, $fuel_type, $datefrom1, $odometer, $prevodo, $quantiy, $unit_cost, $total, $reference, $int_ext, $carnumber, $fueltime, $id){
	global $dbc;
	$q = "UPDATE Diferent_CardNumb SET driver = '$driver', costcenter = '$ccenter', location = '$location', fuel_type = '$fuel_type', dates = '$datefrom1', odometer = '$odometer', prev_odo = '$prevodo', quantity = '$quantiy', unit_cost = '$unit_cost', totalcost = '$total', reference = '$reference', int_ext = '$int_ext', card_numebr = '$carnumber', fueltime = '$fueltime' WHERE IssuanceId = '$id'";
	$r = mysqli_query($dbc, $q);
	if($r){
		return true;
	}
}

function checkPartNumber($partnumber){
	$search = "SELECT * FROM StockHeader WHERE partcode = '$partnumber' LIMIT 1";
	if(($result = mysqli_query($dbc, $search)) !== false){
		$count = mysqli_num_rows($result);
		if ($count>=1) {
			return true;
		}else{
			return false;
		}
	}
}

function DeleteDifferentCardNumber($id){
	global $dbc;
	$q = "DELETE FROM Diferent_CardNumb WHERE IssuanceId = '$id'";
	$r = mysqli_query($dbc, $q);
	if($r){
		return true;
	}
}

function screen_locker($uid,$current_page){
	global $dbc;
	$q = "UPDATE ACTIVE SET user_status = 0 WHERE user_id = '$uid'";
	$r = mysqli_query($dbc, $q);
	if ($r) {
		return true;
	}else {
		return false;
	}
}

function state_updater($user_id){
	global $dbc;
	$q = "UPDATE ACTIVE SET user_status = 1 WHERE user_id = '$user_id'";
	$r = mysqli_query($dbc, $q);
	if ($r) {
		header('Location: /start');
	}
}

function getLocationName1($location, $centre){
	global $dbc;
	$q = "SELECT description FROM LOCATION WHERE location = '$location' AND costCenter = '$centre'";
	$data = mysqli_query($dbc, $q);
	while ($row = mysqli_fetch_assoc($data)):
		$name = $row['description'];
		return $name;
	endwhile;
}

function getVehicleID($vhl, $userCcenter){
	global $dbc;
	$q = "SELECT id FROM VEHICLES WHERE vid = '$vhl' AND costcentre = '$userCcenter'";
	$data = mysqli_query($dbc, $q);
	while ($row = mysqli_fetch_assoc($data)):
		$id = $row['id'];
		return $id;
	endwhile;
}

function getVehicleReg($vhl, $userCcenter){
	global $dbc;
	$q = "SELECT vid2 FROM VEHICLES WHERE vid = '$vhl' AND costcentre = '$userCcenter'";
	$data = mysqli_query($dbc, $q);
	while ($row = mysqli_fetch_assoc($data)):
		$vid2 = $row['vid2'];
		return $vid2;
	endwhile;
}

function getCarNumb($vehicle, $CostCenter){
	global $dbc;
	$q = "SELECT vid2 FROM VEHICLES WHERE vid = '$vehicle' AND costcentre = '$CostCenter'";
	$data = mysqli_query($dbc, $q);
	while ($row = mysqli_fetch_assoc($data)):
		$vid2 = $row['vid2'];
		return $vid2;
	endwhile;
}

function getCarMake($vehicle, $CostCenter){
	global $dbc;
	$q = "SELECT v.make AS 'make',
				m.description AS 'desc'
				FROM VEHICLES AS v
				JOIN MAKES AS m ON v.make = m.make AND v.costcentre = m.costCenter
				WHERE v.vid = '$vehicle' AND v.costcentre = '$CostCenter'";
	$data = mysqli_query($dbc, $q);
	while ($row = mysqli_fetch_assoc($data)):
		$desc = $row['desc'];
		return $desc;
	endwhile;
}

function getModelName($model){
	global $dbc;
	$q = "SELECT description FROM MODELS WHERE model = '$model'";
	$data = mysqli_query($dbc, $q);
	while ($row = mysqli_fetch_assoc($data)):
		$name = $row['description'];
		return $name;
	endwhile;
}

function getMaintenanceName($mname){
	global $dbc;
	$q = "SELECT description FROM maintenType WHERE code = '$mname'";
	$data = mysqli_query($dbc, $q);
	while ($row = mysqli_fetch_assoc($data)):
		$name = $row['description'];
		return $name;
	endwhile;
}

function getServiceName($sname){
	global $dbc;
	$q = "SELECT description FROM serviceType WHERE code = '$sname'";
	$data = mysqli_query($dbc, $q);
	while ($row = mysqli_fetch_assoc($data)):
		$name = $row['description'];
		return $name;
	endwhile;
}

function getBaseName($base, $centre){
	global $dbc;
	$q = "SELECT description FROM BASE WHERE base = '$base' AND costCenter = '$centre'";
	$data = mysqli_query($dbc, $q);
	while ($row = mysqli_fetch_assoc($data)):
		$name = $row['description'];
		return $name;
	endwhile;
}


function getDepartmentName($department, $centre){
	global $dbc;
	$q = "SELECT description FROM DEPARTMENT WHERE department = '$department' AND costCenter = '$centre'";
	$data = mysqli_query($dbc, $q);
	while ($row = mysqli_fetch_assoc($data)):
		$name = $row['description'];
		return $name;
	endwhile;
}

function getDepartmentNameFromVhl($vehicle, $centre){
	global $dbc;
	$q = "SELECT department FROM VEHICLES WHERE vid = '$vehicle'";
	$data = mysqli_query($dbc, $q);
	while ($row = mysqli_fetch_assoc($data)):
		$dpt = $row['department'];
		$name = getDepartmentName($dpt, $centre);
		return $name;
	endwhile;
}

function getServiceTypeName($serciceCode){
	global $dbc;
	$q = "SELECT description FROM serviceType WHERE code = '$serciceCode'";
	$data = mysqli_query($dbc, $q);
	while ($row = mysqli_fetch_assoc($data)):
		$name = $row['description'];
		return $name;
	endwhile;
}


function getVehicleTypeName($type){
	global $dbc;
	$q = "SELECT description FROM VHL_TYPE WHERE vhl_type = '$type'";
	$data = mysqli_query($dbc, $q);
	while ($row = mysqli_fetch_assoc($data)):
		$name = $row['description'];
		return $name;
	endwhile;
}

function getVehicleTypeNameFromVhl($vhl, $center){
	global $dbc;
	$q = "SELECT vtype FROM VEHICLES WHERE vid = '$vhl' AND costcentre = '$center'";
	$data = mysqli_query($dbc, $q);
	while ($row = mysqli_fetch_assoc($data)):
		$name = $row['vtype'];
		return $name;
	endwhile;
}


function getBodyTypeName($bodytype){
	global $dbc;
	$q = "SELECT description FROM BODY_TYPE WHERE body_type = '$bodytype'";
	$data = mysqli_query($dbc, $q);
	while ($row = mysqli_fetch_assoc($data)):
		$name = $row['description'];
		return $name;
	endwhile;
}

function getCostCenterName($center){
	global $dbc;
	$q = "SELECT company FROM COST_CENTER WHERE code = '$center'";
	$data = mysqli_query($dbc, $q);
	while ($row = mysqli_fetch_assoc($data)):
		$name = $row['company'];
		return $name;
	endwhile;
}

function getCostCenterName1($center){
	global $dbc;
	$q = "SELECT company FROM COST_CENTER WHERE code = '$center'";
	$data = mysqli_query($dbc, $q);
	while ($row = mysqli_fetch_assoc($data)):
		$name = $row['company'];
		echo $name;
	endwhile;
}


function getCostCenter($vehicle, $userCenter){
	global $dbc;
	$q = "SELECT costcentre FROM VEHICLES WHERE vid = '$vehicle' AND costcentre = '$userCenter'";
	$data = mysqli_query($dbc, $q);
	while ($row = mysqli_fetch_assoc($data)):
		$name = $row['costcentre'];
		return $name;
	endwhile;
}

function getLocationName2($location, $userCenter){
	global $dbc;
	$q = "SELECT description FROM LOCATION WHERE location = '$location' AND costCenter = '$userCenter'";
	$data = mysqli_query($dbc, $q);
	while ($row = mysqli_fetch_assoc($data)):
		$name = $row['description'];
		return $name;
	endwhile;
}

function getVendorName($code, $center){
	global $dbc;
	$q = "SELECT description FROM Vendors WHERE code = '$code' AND costCenter = '$center'";
	$data = mysqli_query($dbc, $q);
	while ($row = mysqli_fetch_assoc($data)):
		$name = $row['description'];
		return $name;
	endwhile;
}


function getJobCardState($code, $center){
	global $dbc;
	$q = "SELECT JobStatus FROM jobcardhead WHERE JobNumber = '$code' AND CostCenter = '$center'";
	$data = mysqli_query($dbc, $q);
	while ($row = mysqli_fetch_assoc($data)):
		$status = $row['JobStatus'];
		return $status;
	endwhile;
}

function getMakeName($make, $centre){
	global $dbc;
	$q = "SELECT description FROM MAKES WHERE make = '$make' AND costCenter = '$centre'";
	$data = mysqli_query($dbc, $q);
	while ($row = mysqli_fetch_assoc($data)):
		$name = sanitize($row['description']);
		return $name;
	endwhile;
}

function current_make($make, $centre){
	global $dbc;
	$q = "SELECT make, description FROM MAKES WHERE make != '$make' AND costCenter = '$centre'";
	$data = mysqli_query($dbc, $q);
	$actual_name = getMakeName($make, $centre);
	$results .= "<option value='".$make."'>".$actual_name."</option>";
	while ($row = mysqli_fetch_assoc($data)):
		$make1 = $row['make'];
		$desc = $row['description'];
		$results .= "<option value='".$make1."'>".$desc."</option>";
	endwhile;
	return $results;
}

function getCostingName($code){
	global $dbc;
	$q = "SELECT description FROM cost_code WHERE code = '$code'";
	$data = mysqli_query($dbc, $q);
	while ($row = mysqli_fetch_assoc($data)):
		$name = $row['description'];
		return $name;
	endwhile;
}

function costing_code($code){
	global $dbc;
	$q = "SELECT code, description FROM cost_code WHERE code != '$code'";
	$data = mysqli_query($dbc, $q);
	$actual_name = getCostingName($code);
	// $results .= "<option value='".$code."'>".$actual_name."</option>";
	while ($row = mysqli_fetch_assoc($data)):
		$code = $row['code'];
		$desc = $row['description'];

		$results .= "<div class='radio radio-info radio-inline'>
            <input type='radio' id='".$code."' class='radiopick' value='".$code."' name='radioInline'>
            <label for='".$code."'>".$desc."</label>
        </div>";
	endwhile;
	return $results;
}

function getCategoryName($code, $center){
	global $dbc;
	$q = "SELECT description FROM stockCategory WHERE code = '$code' AND costCenter = '$center'";
	$data = mysqli_query($dbc, $q);
	while ($row = mysqli_fetch_assoc($data)):
		$name = $row['description'];
		return $name;
	endwhile;
}

function current_stock_category($code, $center){
	global $dbc;
	$q = "SELECT code, description FROM stockCategory WHERE code != '$code' AND costCenter = '$center'";
	$data = mysqli_query($dbc, $q);
	$actual_name = getCategoryName($code, $center);
	$results .= "<option value='".$code."'>".$actual_name."</option>";
	while ($row = mysqli_fetch_assoc($data)):
		$code = $row['code'];
		$desc = $row['description'];
		$results .= "<option value='".$code."'>".$desc."</option>";
	endwhile;
	return $results;
}

function getPartName($code, $center){
	global $dbc;
	$q = "SELECT partcode FROM StockHeader WHERE partcode = '$code' AND costCenter = '$center'";
	$data = mysqli_query($dbc, $q);
	while ($row = mysqli_fetch_assoc($data)):
		$name = $row['partcode'];
		return $name;
	endwhile;
}


function current_job_part_code($code, $center){
	global $dbc;
	$q = "SELECT PartNumber, partDescription FROM JobDetail WHERE JobNumber = '$code' AND costCenter = '$center'";
	// $q = "SELECT partcode, partDesc FROM StockHeader WHERE partcode != '$code' AND costCenter = '$center'";
	$data = mysqli_query($dbc, $q);
	$actual_name = getPartName($code, $center);
	while ($row = mysqli_fetch_assoc($data)):
		$pcode = $row['PartNumber'];
		$partDesc = $row['partDescription'];
		$results .= "<option value='".$pcode."'>".$partDesc." -- ".$pcode."</option>";
	endwhile;
	return $results;
}


function current_stock_part_code($code, $center){
	global $dbc;
	$q = "SELECT partcode, partDesc, manuCode FROM StockHeader WHERE partcode != '$code' AND costCenter = '$center'";
	$data = mysqli_query($dbc, $q);
	$actual_name = getPartName($code, $center);
	$results .= "<option value='".$code."'>".$actual_name."</option>";
	while ($row = mysqli_fetch_assoc($data)):
		$pcode = $row['partcode'];
		$manuCode = $row['manuCode'];
		$partDesc = $row['partDesc'];
		$results .= "<option value='".$pcode."'>".$partDesc." -- ".$manuCode." -- ".$pcode."</option>";
	endwhile;
	return $results;
}

function checkifUsed($partcode, $center){
	global $dbc;
	$partcode = sanitize($partcode);
	$q = "SELECT partcode FROM StockHeader WHERE partcode = '$partcode' AND costCenter = '$center'";
	$data = mysqli_query($dbc, $q);
	$count = mysqli_num_rows($data);
	if ($count >= 1) {
		return true;
	}
	else{
		return false;
	}
}

function getSupplierName($code ,$center){
	global $dbc;
	$q = "SELECT name FROM Suppliers WHERE code = '$code' AND costCenter = '$center'";
	$data = mysqli_query($dbc, $q);
	while ($row = mysqli_fetch_assoc($data)):
		$name = $row['name'];
		return $name;
	endwhile;
}

function current_jobcard($center){
	global $dbc;
	$q = "SELECT JobNumber, VID FROM jobcardhead WHERE JobStatus != 'Closed' AND CostCenter = '$center' ORDER BY id DESC";
	$data = mysqli_query($dbc, $q);
	while ($row = mysqli_fetch_assoc($data)):
		$JobNumber = $row['JobNumber'];
		$VID = $row['VID'];
		$results .= "<option value='".$JobNumber."'>".$JobNumber."</option>";
	endwhile;
	return $results;
}

function current_supplier($code, $center){
	global $dbc;
	$q = "SELECT code, name FROM Suppliers WHERE code != '$code' AND costCenter = '$center'";
	$data = mysqli_query($dbc, $q);
	$actual_name = getSupplierName($code, $center);
	$results .= "<option value='".$code."'>".$actual_name."</option>";
	while ($row = mysqli_fetch_assoc($data)):
		$code = $row['code'];
		$desc = $row['name'];
		$results .= "<option value='".$code."'>".$desc."</option>";
	endwhile;
	return $results;
}

function current_model($model){
	global $dbc;
	$q = "SELECT model, description FROM MODELS WHERE model != '$model'";
	$data = mysqli_query($dbc, $q);
	$actual_name = getModelName($model);
	$results .= "<option value='".$model."'>".$actual_name."</option>";
	while ($row = mysqli_fetch_assoc($data)):
		$model1 = $row['model'];
		$desc = $row['description'];
		$results .= "<option value='".$model1."'>".$desc."</option>";
	endwhile;
	return $results;
}

function service_list($model){
	global $dbc;
	$q = "SELECT code, description FROM serviceType WHERE code != '$model'";
	$data = mysqli_query($dbc, $q);
	$actual_name = getServiceName($model);
	$results .= "<option value='".$model."'>".$actual_name."</option>";
	while ($row = mysqli_fetch_assoc($data)):
		$model1 = $row['code'];
		$desc = $row['description'];
		$results .= "<option value='".$model1."'>".$desc."</option>";
	endwhile;
	return $results;
}

function vendor_list($model, $center){
	global $dbc;
	$q = "SELECT code, description FROM Vendors WHERE code != '$model' AND costCenter = '$center'";
	$data = mysqli_query($dbc, $q);
	$actual_name = getVendorName($model, $center);
	$results .= "<option value='".$model."'>".$actual_name."</option>";
	while ($row = mysqli_fetch_assoc($data)):
		$code = $row['code'];
		$desc = $row['description'];
		$results .= "<option value='".$code."'>".$desc."</option>";
	endwhile;
	return $results;
}

function maintenance_list($model){
	global $dbc;
	$q = "SELECT code, description FROM maintenType WHERE code != '$model'";
	$data = mysqli_query($dbc, $q);
	$actual_name = getMaintenanceName($model);
	$results .= "<option value='".$model."'>".$actual_name."</option>";
	while ($row = mysqli_fetch_assoc($data)):
		$model1 = $row['code'];
		$desc = $row['description'];
		$results .= "<option value='".$model1."'>".$desc."</option>";
	endwhile;
	return $results;
}

function current_ftype($fuel, $centre){
	global $dbc;
	$q = "SELECT fuel_type, code FROM FUEL_TYPE WHERE code != '$fuel' AND costCenter = '$centre'";
	$data = mysqli_query($dbc, $q);
	$actual_name = getFuelName($fuel, $centre);
	$results .= "<option value='".$fuel."'>".$actual_name."</option>";
	while ($row = mysqli_fetch_assoc($data)):
		$fuel1 = $row['code'];
		$desc = $row['fuel_type'];
		$results .= "<option value='".$fuel1."'>".$desc."</option>";
	endwhile;
	return $results;
}

function getLocationDesc($location, $userCenter){
	global $dbc;
	$q = "SELECT description FROM LOCATION WHERE location = '$location' AND costCenter = '$userCenter'";
	$data = mysqli_query($dbc, $q);
	while ($row = mysqli_fetch_assoc($data)):
		$desc = $row['description'];
		return $desc;
	endwhile;
}

function current_location($location, $userCenter){
	global $dbc;
	$q = "SELECT location, description FROM LOCATION WHERE location != '$location' AND costCenter = '$userCenter'";
	$data = mysqli_query($dbc, $q);
	$actual_name = getLocationName2($location, $userCenter);
	$results .= "<option value='".$location."'>".$actual_name."</option>";
	while ($row = mysqli_fetch_assoc($data)):
		$fuel1 = $row['location'];
		$desc = $row['description'];
		$results .= "<option value='".$fuel1."'>".$desc."</option>";
	endwhile;
	return $results;
}

function current_vhltype($vhltype){
	global $dbc;
	$q = "SELECT vhl_type, description FROM VHL_TYPE WHERE vhl_type != '$vhltype'";
	$data = mysqli_query($dbc, $q);
	$actual_name = getVehicleTypeName($vhltype);
	$results .= "<option value='".$vhltype."'>".$actual_name."</option>";
	while ($row = mysqli_fetch_assoc($data)):
		$vhlty = $row['vhl_type'];
		$desc = $row['description'];
		$results .= "<option value='".$vhlty."'>".$desc."</option>";
	endwhile;
	return $results;
}



function current_fuelType($code, $center){
	global $dbc;
	$q = "SELECT fuel_type, code FROM FUEL_TYPE WHERE code != '$code' AND costCenter = '$center'";
	$data = mysqli_query($dbc, $q);
	$actual_name = getFuelName($code, $center);
	$results .= "<option value='".$code."'>".$actual_name."</option>";
	while ($row = mysqli_fetch_assoc($data)):
		$fuel_type = $row['fuel_type'];
		$code = $row['code'];
		$results .= "<option value='".$code."'>".$fuel_type."</option>";
	endwhile;
	return $results;
}

function getTaskDesc($code, $centre){
	global $dbc;
	$q = "SELECT description FROM Task_Code WHERE code = '$code' AND costCenter = '$centre'";
	$data = mysqli_query($dbc, $q);
	while ($row = mysqli_fetch_assoc($data)):
		$name = $row['description'];
		return $name;
	endwhile;
}

function current_task_code($code, $centre){
	global $dbc;
	$q = "SELECT code, description FROM Task_Code WHERE code != '$code' AND costCenter = '$centre'";
	$data = mysqli_query($dbc, $q);
	$actual_name = getTaskDesc($code, $centre);
	$results .= "<option value='".$code."'>".$actual_name."</option>";
	while ($row = mysqli_fetch_assoc($data)):
		$code = $row['code'];
		$desc = $row['description'];
		$results .= "<option value='".$code."'>".$desc."</option>";
	endwhile;
	return $results;
}

function current_vehicle($car, $centre){
	global $dbc;
	$q = "SELECT vid FROM VEHICLES WHERE status != '0' AND vid !='$car' AND costcentre = '$centre'";
	$data = mysqli_query($dbc, $q);
	$results .= "<option value='".$car."'>".$car."</option>";
	while ($row = mysqli_fetch_assoc($data)):
		$vid = $row['vid'];
		$results .= "<option value='".$vid."'>".$vid."</option>";
	endwhile;
	return $results;
}

function all_center_vehicle($car){
	global $dbc;
	$q = "SELECT vid FROM VEHICLES WHERE status != '0' AND vid !='$car'";
	$data = mysqli_query($dbc, $q);
	$results .= "<option value='".$car."'>".$car."</option>";
	while ($row = mysqli_fetch_assoc($data)):
		$vid = $row['vid'];
		$results .= "<option value='".$vid."'>".$vid."</option>";
	endwhile;
	return $results;
}

function current_contact_vehicle($center){
	global $dbc;
	$q = "SELECT vehicle FROM Contracted WHERE costCenter = '$center'";
	$data = mysqli_query($dbc, $q);
	while ($row = mysqli_fetch_assoc($data)):
		$vid = $row['vehicle'];
		$results .= "<option value='".$vid."'>".$vid."</option>";
	endwhile;
	return $results;
}

function current_driver_detail($center){
	global $dbc;
	$q = "SELECT first_name, other_name, employ_number FROM DRIVER WHERE costCenter = '$center'";
	$data = mysqli_query($dbc, $q);
	while ($row = mysqli_fetch_assoc($data)):
		$first_name = $row['first_name'];
		$other_name = $row['other_name'];
		$employ_number = $row['employ_number'];
		$results .= "<option value='".$employ_number."'>".$first_name . ' ' . $other_name . ' -- ' . $employ_number. "</option>";
	endwhile;
	return $results;
}

function current_department($center){
	global $dbc;
	$q = "SELECT department, description FROM DEPARTMENT WHERE costCenter = '$center'";
	$data = mysqli_query($dbc, $q);
	while ($row = mysqli_fetch_assoc($data)):
		$department = $row['department'];
		$description = $row['description'];
		$results .= "<option value='".$department."'>".$description."</option>";
	endwhile;
	return $results;
}

function current_costcenter(){
	global $dbc;
	$q = "SELECT code, customer FROM COST_CENTER";
	$data = mysqli_query($dbc, $q);
	while ($row = mysqli_fetch_assoc($data)):
		$code = $row['code'];
		$customer = $row['customer'];
		$results .= "<option value='".$code."'>".$customer."</option>";
	endwhile;
	return $results;
}

function current_userdetail(){
	global $dbc;
	$q = "SELECT id, firstname, lastname FROM users";
	$data = mysqli_query($dbc, $q);
	while ($row = mysqli_fetch_assoc($data)):
		$id = $row['id'];
		$firstname = $row['firstname'];
		$lastname = $row['lastname'];
		$results .= "<option value='".$id."'>".$firstname.' '.$lastname."</option>";
	endwhile;
	return $results;
}


function current_bodytype($bodytype){
	global $dbc;
	$q = "SELECT body_type, description FROM BODY_TYPE WHERE body_type != '$bodytype'";
	$data = mysqli_query($dbc, $q);
	$actual_name = getBodyTypeName($bodytype);
	$results .= "<option value='".$bodytype."'>".$actual_name."</option>";
	while ($row = mysqli_fetch_assoc($data)):
		$vhlty = $row['body_type'];
		$desc = $row['description'];
		$results .= "<option value='".$vhlty."'>".$desc."</option>";
	endwhile;
	return $results;
}


function checkifbooked($vehicle,$combinefrom,$combineto,$ccenter){
	global $dbc;
	$q = "SELECT * FROM Rentals WHERE status = '1' AND (vehicle = '$vehicle' AND costCenter = '$ccenter' AND ('$combinefrom' BETWEEN datefrom AND dateto) OR vehicle = '$vehicle' AND ('$combineto' BETWEEN datefrom AND dateto))";
	$data = mysqli_query($dbc, $q);
	$count = mysqli_num_rows($data);
	if ($count >= 1) {
		return true;
	}
	else{
		return false;
	}
}

// function checkifbookedUpdate($vehicle,$combinefrom,$combineto){
// 	global $dbc;
// 	$q = "SELECT * FROM Rentals WHERE vehicle = '$vehicle' AND ('$combinefrom' BETWEEN datefrom AND dateto) OR vehicle = '$vehicle' AND ('$combineto' BETWEEN datefrom AND dateto) ";
// 	$data = mysqli_query($dbc, $q);
// 	$count = mysqli_num_rows($data);
// 	if ($count >= 1) {
// 		return true;
// 	}
// 	else{
// 		return false;
// 	}
// }

function checkifclaimed($vehicle,$startdate,$enddate,$department,$ccenter){
	global $dbc;
	$q = "SELECT * FROM Claime WHERE vehicle = '$vehicle' AND startdate = '$startdate' AND enddate = '$enddate' AND department = '$department' AND costCenter = '$ccenter'";
	$data = mysqli_query($dbc, $q);
	$count = mysqli_num_rows($data);
	if ($count >= 1) {
		return true;
	}
	else{
		return false;
	}
}

function getDepartmentAccount($department,$ccenter){
	global $dbc;
	$search = "SELECT account FROM DEPARTMENT WHERE department = '$department' AND costCenter = '$ccenter' LIMIT 1";
	if(($result = mysqli_query($dbc, $search)) !== false){
		while( $obj = mysqli_fetch_assoc( $result )) {
			$account = $obj['account'];
			return $account;
        }
    }
}

function getRateperKM($vehicle, $ccenter){
	global $dbc;
	$search = "SELECT * FROM VEHICLES WHERE vid = '$vehicle' AND costcentre = '$ccenter' LIMIT 1";
	if(($result = mysqli_query($dbc, $search)) !== false){
		while( $obj = mysqli_fetch_assoc( $result )) {
			$rateperkm 	= $obj['rateperkm'];
			return $rateperkm;
        }
    }
}


function current_departmet($department, $centre){
	global $dbc;
	$q = "SELECT department, description FROM DEPARTMENT WHERE department != '$department' AND costCenter = '$centre'";
	$data = mysqli_query($dbc, $q);
	$actual_name = getDepartmentName($department, $centre);
	$results .= "<option value='".$department."'>".$actual_name."</option>";
	while ($row = mysqli_fetch_assoc($data)):
		$vhlty = $row['department'];
		$desc = $row['description'];
		$results .= "<option value='".$vhlty."'>".$desc."</option>";
	endwhile;
	return $results;
}

function getMechanicName($staff, $centre){
	global $dbc;
	$q = "SELECT fname, lname FROM Mechanics WHERE staffNumber = '$staff' AND costCenter = '$centre'";
	$data = mysqli_query($dbc, $q);
	while ($row = mysqli_fetch_assoc($data)):
		$name = $row['fname'];
		$sname = $row['lname'];
		$all = $name." ".$sname;
		return $all;
	endwhile;
}

function MechanicExists($staff, $centre){
	global $dbc;
	$q = "SELECT staffNumber FROM Mechanics WHERE staffNumber = '$staff' AND costCenter = '$centre'";
	$data = mysqli_query($dbc, $q);
	$get = mysqli_num_rows($data);
	if ($get >= 1) {
		return true;
	}else{
		return false;
	}
}

function MechanicExistsUpdate($staff, $centre, $id){
	global $dbc;
	$q = "SELECT staffNumber FROM Mechanics WHERE staffNumber = '$staff' AND id != '$id' AND costCenter = '$centre'";
	$data = mysqli_query($dbc, $q);
	$get = mysqli_num_rows($data);
	if ($get >= 1) {
		return true;
	}
	else{
		return false;
	}
}

function current_mechanics($staff, $centre){
	global $dbc;
	$q = "SELECT staffNumber, fname, lname FROM Mechanics WHERE staffNumber != '$staff' AND costCenter = '$centre'";
	$data = mysqli_query($dbc, $q);
	$actual_name = getMechanicName($staff, $centre);
	$results .= "<option value='".$staff."'>".$actual_name.' -- '.$staff."</option>";
	while ($row = mysqli_fetch_assoc($data)):
		$staffNumber = $row['staffNumber'];
		$fname = $row['fname'];
		$lname = $row['lname'];
		$results .= "<option value='".$staffNumber."'>".$fname .' '.$lname.' -- '.$staffNumber."</option>";
	endwhile;
	return $results;
}

function current_base($base, $centre){
	global $dbc;
	$q = "SELECT base, description FROM BASE WHERE base != '$base' AND costCenter = '$centre'";
	$data = mysqli_query($dbc, $q);
	$actual_name = getBaseName($base, $centre);
	$results .= "<option value='".$base."'>".$actual_name."</option>";
	while ($row = mysqli_fetch_assoc($data)):
		$base = $row['base'];
		$desc = $row['description'];
		$results .= "<option value='".$base."'>".$desc."</option>";
	endwhile;
	return $results;
}

function current_ccenter($center){
	global $dbc;
	$q = "SELECT code, company FROM COST_CENTER WHERE code != '$center'";
	$data = mysqli_query($dbc, $q);
	$actual_name = getCostCenterName($center);
	$results .= "<option value='".$center."'>".$actual_name."</option>";
	while ($row = mysqli_fetch_assoc($data)):
		$code = $row['code'];
		$company = $row['company'];
		$results .= "<option value='".$code."'>".$company."</option>";
	endwhile;
	return $results;
}

function getDriverName($emp_numb, $centre){
	global $dbc;
	$q = "SELECT first_name, other_name FROM DRIVER WHERE employ_number = '$emp_numb' AND costCenter = '$centre'";
	$data = mysqli_query($dbc, $q);
	while ($row = mysqli_fetch_assoc($data)):
		$name = $row['first_name'];
		$sname = $row['other_name'];
		$all = $name." ".$sname;
		return $all;
	endwhile;
}

function getDriverNumber($emp_numb){
	global $dbc;
	$q = "SELECT mobile FROM DRIVER WHERE employ_number = '$emp_numb'";
	$data = mysqli_query($dbc, $q);
	while ($row = mysqli_fetch_assoc($data)):
		$mobile = $row['mobile'];
		return $mobile;
	endwhile;
}

function current_driver($enumber, $centre){
	global $dbc;
	$q = "SELECT first_name, other_name, employ_number FROM DRIVER WHERE employ_number != '$enumber' AND costCenter = '$centre'";
	$data = mysqli_query($dbc, $q);
	$actual_name = getDriverName($enumber, $centre);
	$results .= "<option value='".$enumber."'>".$actual_name." - ".$enumber."</option>";
	while ($row = mysqli_fetch_assoc($data)):
		$code = $row['employ_number'];
		$first_name = $row['first_name'];
		$other_name = $row['other_name'];
		$fullname = $first_name." ".$other_name;
		$results .= "<option value='".$code."'>".$fullname." - ".$code." </option>";
	endwhile;
	return $results;
}

function add_driver_assignment($driver, $reg1, $uid, $today, $cost_cnt){
	global $dbc;
	$q = "SELECT vehicle, employee_number FROM VEHICLE_ASIGN WHERE vehicle = '$reg1' AND employee_number != '$driver' AND costCenter = '$cost_cnt'";
	$data = mysqli_query($dbc, $q);
	$count = mysqli_num_rows($data);
	if ($count <= 0) {
		$sql = "INSERT INTO VEHICLE_ASIGN (vehicle, date_asigned, employee_number, added_by, costCenter) VALUES ('$reg1', '$today', '$driver', '$uid', '$cost_cnt')";
		$stmt = mysqli_query($dbc, $sql);
		if ($stmt) {
			return true;
		}
	}
	else{
		$sql = "DELETE FROM VEHICLE_ASIGN WHERE vehicle = '$reg1' AND costCenter = '$cost_cnt'";
		$stmt = mysqli_query($dbc, $sql);
		if ($stmt) {
			$query = "INSERT INTO VEHICLE_ASIGN (vehicle, date_asigned, employee_number, added_by, costCenter) VALUES ('$reg1', '$today', '$driver', '$uid', '$cost_cnt')";
			$result = mysqli_query($dbc, $query);
			if ($result) {
				return true;
			}
		}
	}
	return true;
}
// load vehicles in pagination
function all_assigns($page,$user_email,$uid,$user_center){
	global $dbc;
	$item_per_page = 20;
	if($user_email && $uid){
	    // $uid = $_SESSION['id'];
	    if($page != ""){
	        $page_number = $page;
	        if(!is_numeric($page_number)){
	            $page_number = 1;
	        } //incase of invalid page number
	    }else{
	        $page_number = 1; //if there's no page number, set it to 1
	    }
	    //get total number of records from database for pagination
	    $results = "SELECT id FROM VEHICLE_ASIGN Where id !='' AND costCenter = '$user_center'";
	    $results = mysqli_query($dbc, $results);
	    $get_total_rows = mysqli_num_rows($results);
	    //break records into pages
	    $total_pages = ceil($get_total_rows/$item_per_page);

	    //get starting position to fetch the records
	    $page_position = (($page_number-1) * $item_per_page);

	    $q = "SELECT * FROM VEHICLE_ASIGN WHERE costCenter = '$user_center' ORDER BY id DESC LIMIT $page_position, $item_per_page";
	    $r = mysqli_query($dbc, $q);
	    ?>
	<div class="table-responsive">
	    <table class="table table-hover mails m-0 table table-actions-bar">
	        <thead>
	            <tr>
	                <th>Vehicle</th>
	                <th>Period from</th>
	                <th>Period to</th>
	                <th>Employee number</th>
	                <th>Driver name</th>
	                <th class="text-right">Action</th>
	            </tr>
	        </thead>
	        <tbody>
	<?php
	    while( $obj = mysqli_fetch_assoc( $r )) {
	    	date_default_timezone_set("Africa/Nairobi");
	        $id = $obj['id'];
	        $vehicle = $obj['vehicle'];
	        $date_asigned = $obj['date_asigned'];
	        $period_to = $obj['period_to'];

	        if (isset($date_asigned) && !empty($date_asigned)) {
	        	$from = strtotime($date_asigned);
				$from1 = date('d M Y', $from);
	        }
	        else{
	        	$from1 = "";
	        }

	        if (isset($period_to) && !empty($period_to)) {
	        	$to = strtotime($period_to);
				$to1 = date('d M Y', $to);
	        }
	        else{
	        	$to1 = "";
	        }


	        $employee_number = $obj['employee_number'];
	        $Dname = getDriverName($employee_number, $user_center);
	        ?>
	            <tr id='<?php echo $id; ?>'>
	                <td style='width: 13%; text-transform: uppercase;'><?php echo $vehicle; ?></td>
	                <td style='width: 11%;'><?php echo $from1; ?></td>
	                <td style='width: 13%;'><?php echo $to1; ?></td>
	                <td style='width: 15%;'><?php echo $employee_number; ?></td>
	                <td style='width: 15%;'><?php echo $Dname; ?></td>

	                <td class="text-right" style='width: 11%;'>
	                    <span class='allocate' data='<?php echo $vehicle; ?>' id='<?php echo $id; ?>' type='<?php echo $Dname; ?>'>
	                        <button class="btn btn-success waves-effect waves-light btn-xs editAssign" data-toggle="modal" data-target="#edit_allocation_details">Edit</button>
	                        <?php
	                            if ($uid == 1) {
	                                ?>
	                                    <button class='btn btn-danger editable-table-button btn-xs deleteAssign'>Del</button>
	                                <?php
	                            }
	                        ?>
	                    </span>
	                </td>
	            </tr>
	        <?php
	    }
	    ?>
	    </tbody></table></div>
	    <div class='row'>
	        <div class='col-sm-12'>
	            <div class='col-sm-7 col-md-offset-2 text-center'>
	    <?php
	    echo paginate_function($item_per_page, $page_number, $get_total_rows, $total_pages);
	    ?>
	        </div></div></div>
	    <?php
	}
}

function getTotalCost($JobNumber, $centre){
	global $dbc;
	$q = "SELECT * FROM JobDetail WHERE JobNumber = '$JobNumber' AND CostCenter = '$centre'";
	$r = mysqli_query($dbc, $q);
	$total = 0;
	while( $obj = mysqli_fetch_assoc( $r )) {
		$JobTotalCost = $obj['JobTotalCost'];
		$total = $total + $JobTotalCost;
		$tttax = number_format($total);
	}
	echo 'Total job cost: '.$tttax;
}

function getTotalCostDownTime($JobNumber, $centre){
	global $dbc;
	$q = "SELECT SUM(JobTotalCost) AS tts FROM JobDetail WHERE JobNumber = '$JobNumber' AND CostCenter = '$centre'";
	$r = mysqli_query($dbc, $q);
	$total = 0;
	while( $obj = mysqli_fetch_assoc( $r )) {
		$JobTotalCost = $obj['tts'];
	}
	return $JobTotalCost;
}


function getTotalFuel($worknumber, $userCenter){
	global $dbc;
	$q = "SELECT * FROM Workdetails WHERE workticketnuber = '$worknumber' AND costCenter = '$userCenter'";
	$r = mysqli_query($dbc, $q);
	$total = 0;
	while( $obj = mysqli_fetch_assoc( $r )) {
		$fuel = $obj['fuel'];
		$total = $total + $fuel;
		$tttax = number_format($total);
	}
	echo 'Total fuel: '.$tttax;
}

// load vehicles in pagination
function StockDetailSelect($stocknumb,$uid,$page,$user_center){
	global $dbc;
	$item_per_page = 5;
	if($uid){
	    // $uid = $_SESSION['id'];
	    if($page != ""){
	        $page_number = $page;
	        if(!is_numeric($page_number)){
	            $page_number = 1;
	        } //incase of invalid page number
	    }else{
	        $page_number = 1; //if there's no page number, set it to 1
	    }
	    //get total number of records from database for pagination
	    $results = "SELECT id FROM StockDetails WHERE stockNumber = '$stocknumb' AND costCenter = '$user_center'";
	    $results = mysqli_query($dbc, $results);
	    $get_total_rows = mysqli_num_rows($results);
	    //break records into pages
	    $total_pages = ceil($get_total_rows/$item_per_page);

	    //get starting position to fetch the records
	    $page_position = (($page_number-1) * $item_per_page);

	    $q = "SELECT * FROM StockDetails WHERE stockNumber = '$stocknumb' AND costCenter = '$user_center' ORDER BY id DESC LIMIT $page_position, $item_per_page";
	    $r = mysqli_query($dbc, $q);
	    ?>
	<div class="table-responsive">
	    <table class="table table-hover mails m-0 table table-actions-bar">
	        <thead>
	            <tr>
	                <th>Date</th>
	                <th>Qty</th>
                    <th>Unit Cost</th>
                    <th>Total</th>
                    <th>Advice Note</th>
                    <th>Supplier</th>
                    <th>LPO No.</th>
                    <th>Added By</th>
                    <th class="text-right">Action</th>
	            </tr>
	        </thead>
	        <tbody>
	<?php
	    while( $obj = mysqli_fetch_assoc( $r )) {
	    	date_default_timezone_set("Africa/Nairobi");
	        $id = $obj['id'];
	        $stockNumber = $obj['stockNumber'];
	        $quantity = $obj['quantity'];
	        $datepick = $obj['datepick'];
	        $unitCost = $obj['unitCost'];
	        $total = $obj['total'];
	        $advicenote = $obj['advicenote'];
	        $supplire = $obj['supplire'];
	        // $supplire = getSupplierName($obj['supplire']);
	        $orderNumber = $obj['orderNumber'];
	        $dateAdded = $obj['dateAdded'];

	        if (isset($datepick) && !empty($datepick)) {
				$x = strtotime($datepick);
				$dateout1 = date('d M Y', $x);
			}
			else{
				$dateout1 = "";
			}

			if (isset($dateAdded) && !empty($dateAdded)) {
				$x = strtotime($dateAdded);
				$dateAdded1 = date('d M Y', $x);
			}
			else{
				$dateAdded1 = "";
			}

	        $added_by = $obj['added_by'];
	        $initials = getUserInitials($added_by);
	        ?>
	            <tr id='<?php echo $id; ?>'>
	                <td style='width: 11%;'><?php echo $dateout1; ?></td>
	                <td style='width: 10%;'><?php echo $quantity; ?></td>
	                <td style='width: 10%;'><?php echo $unitCost; ?></td>
	                <td style='width: 10%;'><?php echo $total; ?></td>
	                <td style='width: 10%;'><?php echo $advicenote; ?></td>
	                <td style='width: 18%;'><?php echo $supplire; ?></td>
	                <td style='width: 10%;'><?php echo $orderNumber; ?></td>
	                <td style='width: 9%;'><?php echo $initials; ?></td>
	                <td class="text-right" style='width: 11%;'>
	                    <span class='StockID' data='<?php echo $stockNumber; ?>' id='<?php echo $id; ?>' type='<?php echo $uid; ?>'>
	                        <button class='btn btn-info editable-table-button btn-xs editworkdetails' data-toggle="modal" data-target="#edit-stock-dtail">Edit</button>
	                        <?php
	                            if ($uid == 1) {
	                                ?>
	                                    <button class='btn btn-danger editable-table-button btn-xs deleteStockdetail'>Del</button>
	                                <?php
	                            }
	                        ?>
	                    </span>
	                </td>
	            </tr>
	        <?php
	    }
	    ?>
	    </tbody></table></div>
	    <div class='row'>
	        <div class='col-sm-12'>
	            <div class='col-sm-7 col-md-offset-2 text-center'>
	    <?php
	    echo paginate_results_function($item_per_page, $page_number, $get_total_rows, $total_pages);
	    ?>
	        </div></div></div>
	    <?php
	}
}

// load vehicles in pagination
function OrderDetailSelect($ordernumber,$uid,$page,$user_center){
	global $dbc;
	$item_per_page = 5;
	if($uid){
	    // $uid = $_SESSION['id'];
	    if($page != ""){
	        $page_number = $page;
	        if(!is_numeric($page_number)){
	            $page_number = 1;
	        } //incase of invalid page number
	    }else{
	        $page_number = 1; //if there's no page number, set it to 1
	    }
	    //get total number of records from database for pagination
	    $results = "SELECT id FROM OrderDetail WHERE ordernumber = '$ordernumber' AND costCenter = '$user_center'";
	    $results = mysqli_query($dbc, $results);
	    $get_total_rows = mysqli_num_rows($results);
	    //break records into pages
	    $total_pages = ceil($get_total_rows/$item_per_page);

	    //get starting position to fetch the records
	    $page_position = (($page_number-1) * $item_per_page);

	    $q = "SELECT * FROM OrderDetail WHERE ordernumber = '$ordernumber' AND costCenter = '$user_center' ORDER BY id DESC LIMIT $page_position, $item_per_page";
	    $r = mysqli_query($dbc, $q);
	    ?>
	<div class="table-responsive">
	    <table class="table table-hover mails m-0 table table-actions-bar">
	        <thead>
	            <tr>
	                <th>Decription.</th>
	                <th>Part No.</th>
                    <th class="text-center">Qty</th>
                    <th>Units</th>
                    <th>Unit Cost</th>
                    <th>Date Rec.</th>
                    <th>Advice Note</th>
                    <th>Innitials</th>
                    <th class="text-right">Action</th>
	            </tr>
	        </thead>
	        <tbody>
	<?php
	    while( $obj = mysqli_fetch_assoc( $r )) {
	    	date_default_timezone_set("Africa/Nairobi");
	        $id = $obj['id'];
	        $ordernumber = $obj['ordernumber'];
	        $partnumber = $obj['partnumber'];
	        $quantity = $obj['quantity'];
	        $description = $obj['description'];
	        $unitcost = $obj['unitcost'];
	        $units = $obj['units'];
	        $daterecieved = $obj['daterecieved'];
	        $quantityRecieved = $obj['quantityRecieved'];
	        $adviceNote = $obj['adviceNote'];


	        if (isset($daterecieved) && !empty($daterecieved)) {
				$x = strtotime($daterecieved);
				$daterecieved1 = date('d M Y', $x);
			}
			else{
				$daterecieved1 = "";
			}

	        $added_by = $obj['added_by'];
	        $initials = getUserInitials($added_by);
	        ?>
	            <tr id='<?php echo $id; ?>'>
	                <td style='width: 15%;'><?php echo $description; ?></td>
	                <td style='width: 10%;'><?php echo $partnumber; ?></td>
	                <td class="text-center" style='width: 10%;'><?php echo $quantity; ?></td>
	                <td style='width: 10%;'><?php echo $units; ?></td>
	                <td style='width: 10%;'><?php echo $unitcost; ?></td>
	                <td style='width: 12%;'><?php echo $daterecieved1; ?></td>
	                <td style='width: 10%;'><?php echo $adviceNote; ?></td>
	                <td style='width: 7%;'><?php echo $initials; ?></td>
	                <td class="text-right" style='width: 5%;'>
	                    <span class='OrderID' data='<?php echo $ordernumber; ?>' id='<?php echo $id; ?>' type='<?php echo $uid; ?>'>
	                        <button class='btn btn-info editable-table-button btn-xs editOrderdetails' data-toggle="modal" data-target="#edit-stock-dtail">Edit</button>
	                        <?php
	                            if ($uid == 1) {
	                                ?>
	                                    <!-- <button class='btn btn-danger editable-table-button btn-xs deleteOrderdetail'>Del</button> -->
	                                <?php
	                            }
	                        ?>
	                    </span>
	                </td>
	            </tr>
	        <?php
	    }
	    ?>
	    </tbody></table></div>
	    <div class='row'>
	        <div class='col-sm-12'>
	            <div class='col-sm-7 col-md-offset-2 text-center'>
	    <?php
	    echo paginate_results_function($item_per_page, $page_number, $get_total_rows, $total_pages);
	    ?>
	        </div></div></div>
	    <?php
	}
}

// load vehicles in pagination
function WorkTicketSelect($worknumber,$uid,$page,$user_center){
	global $dbc;
	$item_per_page = 5;
	if($uid){
	    // $uid = $_SESSION['id'];
	    if($page != ""){
	        $page_number = $page;
	        if(!is_numeric($page_number)){
	            $page_number = 1;
	        } //incase of invalid page number
	    }else{
	        $page_number = 1; //if there's no page number, set it to 1
	    }
	    //get total number of records from database for pagination
	    $results = "SELECT id FROM Workdetails WHERE workticketnuber = '$worknumber' AND costCenter = '$user_center'";
	    $results = mysqli_query($dbc, $results);
	    $get_total_rows = mysqli_num_rows($results);
	    //break records into pages
	    $total_pages = ceil($get_total_rows/$item_per_page);

	    //get starting position to fetch the records
	    $page_position = (($page_number-1) * $item_per_page);

	    $q = "SELECT * FROM Workdetails WHERE workticketnuber = '$worknumber' AND costCenter = '$user_center' ORDER BY id DESC LIMIT $page_position, $item_per_page";
	    $r = mysqli_query($dbc, $q);
	    ?>
	<div class="table-responsive">
	    <table class="table table-hover mails m-0 table table-actions-bar">
	        <thead>
	            <tr>
	                <th>Date Out</th>
                    <th>Date In.</th>
                    <th>Destination.</th>
                    <th>Oil</th>
                    <th>Fuel</th>
                    <th>Odo Out</th>
                    <th>Odo In</th>
                    <th>Added By</th>
                    <th class="text-right">Action</th>
	            </tr>
	        </thead>
	        <tbody>
	<?php
	    while( $obj = mysqli_fetch_assoc( $r )) {
	    	date_default_timezone_set("Africa/Nairobi");
	        $id = $obj['id'];
	        $workID = $obj['workticketnuber'];
	        $dateout = $obj['dateout'];
	        $datein = $obj['datein'];
	        $destination = $obj['destination'];
	        $oil = $obj['oil'];
	        $fuel = $obj['fuel'];
	        $odoOut = $obj['odoOut'];
	        $odoIn = $obj['odoIn'];

	        if (isset($dateout) && !empty($dateout)) {
				$x = strtotime($dateout);
				$dateout1 = date('d M Y', $x);
			}
			else{
				$dateout1 = "";
			}

			if (isset($datein) && !empty($datein)) {
				$x = strtotime($datein);
				$datein1 = date('d M Y', $x);
			}
			else{
				$datein1 = "";
			}

	        $added_by = $obj['added_by'];
	        $initials = getUserInitials($added_by);
	        ?>
	            <tr id='<?php echo $id; ?>'>
	                <td style='width: 15%;'><?php echo $dateout1; ?></td>
	                <td style='width: 10%;'><?php echo $datein1; ?></td>
	                <td style='width: 20%;'><?php echo $destination; ?></td>
	                <td style='width: 7%;'><?php echo $oil; ?></td>
	                <td style='width: 10%;'><?php echo $fuel; ?></td>
	                <td style='width: 10%;'><?php echo $odoOut; ?></td>
	                <td style='width: 10%;'><?php echo $odoIn; ?></td>
	                <td style='width: 10%;'><?php echo $initials; ?></td>
	                <td class="text-right" style='width: 11%;'>
	                    <span class='WDID' data='<?php echo $workID; ?>' id='<?php echo $id; ?>' type='<?php echo $uid; ?>'>
	                        <button class='btn btn-info editable-table-button btn-xs editworkdetails' data-toggle="modal" data-target="#edit-maintenance-dtail">Edit</button>
	                        <?php
	                            if ($uid == 1) {
	                                ?>
	                                    <button class='btn btn-danger editable-table-button btn-xs deleteticketdetail'>Del</button>
	                                <?php
	                            }
	                        ?>
	                    </span>
	                </td>
	            </tr>
	        <?php
	    }
	    ?>
	    </tbody></table></div>
	    <div class='row'>
	        <div class='col-sm-12'>
	            <div class='col-sm-7 col-md-offset-2 text-center'>
	    <?php
	    echo paginate_results_function($item_per_page, $page_number, $get_total_rows, $total_pages);
	    ?>
	        </div></div></div>
	    <?php
	}
}

// load vehicles in pagination
 function JobCardSelectOld($user_pid,$JobNumber,$uid,$page,$user_center){
 	global $dbc;
 	$item_per_page = 5;
 	if($uid){
 	    // $uid = $_SESSION['id'];
 	    if($page != ""){
 	        $page_number = $page;
 	        if(!is_numeric($page_number)){
 	            $page_number = 1;
 	        } //incase of invalid page number
 	    }else{
 	        $page_number = 1; //if there's no page number, set it to 1
 	    }
 	    //get total number of records from database for pagination
 	    $results = "SELECT id FROM JobDetail WHERE JobNumber = '$JobNumber' AND CostCenter = '$user_center'";
 	    $results = mysqli_query($dbc, $results);
 	    $get_total_rows = mysqli_num_rows($results);
 	    //break records into pages
 	    $total_pages = ceil($get_total_rows/$item_per_page);

 	    //get starting position to fetch the records
 	    $page_position = (($page_number-1) * $item_per_page);

 	    $q = "SELECT * FROM JobDetail WHERE JobNumber = '$JobNumber' AND CostCenter = '$user_center' ORDER BY id DESC LIMIT $page_position, $item_per_page";
 	    $r = mysqli_query($dbc, $q);
 	    ?>
 	<div class="table-responsive">
 	    <table class="table table-hover mails m-0 table table-actions-bar">
 	        <thead>
 	            <tr>
 	                <th>Service type</th>
                     <th>Part No.</th>
                     <th>Description.</th>
                     <th>Qty</th>
                     <th>Total cost</th>
                     <th>Vendor</th>
                     <th>Issue date</th>
                     <th>Initials</th>
                     <th class="text-right">Action</th>
 	            </tr>
 	        </thead>
 	        <tbody>
 	<?php
 	    while( $obj = mysqli_fetch_assoc( $r )) {
 	    	date_default_timezone_set("Africa/Nairobi");
 	        $id = $obj['id'];
 	        $RepairType = $obj['RepairType'];
 	        $RepairType1 = getMaintenanceName($RepairType);
 	        $partDescription = $obj['partDescription'];
 	        $PartNumber = $obj['PartNumber'];
 	        $Quantity = $obj['Quantity'];
 	        $JobTotalCost = $obj['JobTotalCost'];
 	        $VendorName = $obj['VendorName'];
 	        $VendorName1 = getVendorName($VendorName, $user_center);
 	        $JobStartDate1 = $obj['JobStartDate'];
 	        if (isset($JobStartDate1) && !empty($JobStartDate1)) {
 				$x = strtotime($JobStartDate1);
 				$JobStartDate = date('d M Y', $x);
 			}
 			else{
 				$JobStartDate = "";
 			}

 	        $added_by = $obj['added_by'];
 	        $initials = getUserInitials($added_by);
 	        ?>
 	            <tr id='<?php echo $id; ?>'>
 	                <td style='width: 15%;'><?php echo $RepairType1; ?></td>
 	                <td style='width: 10%;'><?php echo $PartNumber; ?></td>
 	                <td style='width: 20%;'><?php echo $partDescription; ?></td>
 	                <td style='width: 7%;'><?php echo $Quantity; ?></td>
 	                <td style='width: 10%;'><?php echo $JobTotalCost; ?></td>
 	                <td style='width: 15%;'><?php echo $VendorName1; ?></td>
 	                <td style='width: 10%;'><?php echo $JobStartDate; ?></td>
 	                <td style='width: 5%; text-align: center;'><?php echo $initials; ?></td>
 	                <td class="text-right" style='width: 11%;'>
 	                    <span class='JBcardID' user='<?php echo $user_pid; ?>' data='<?php echo $JobNumber; ?>' id='<?php echo $id; ?>' type='<?php echo $uid; ?>'>
 	                        <button class='btn btn-info editable-table-button btn-xs editJob' data-toggle="modal" data-target="#edit-maintenance-dtail">Edit</button>
 	                    </span>
 	                </td>
 	            </tr>
 	        <?php
 	    }
 	    ?>
 	    </tbody></table></div>
 	    <div class='row'>
 	        <div class='col-sm-12'>
	            <div class='col-sm-7 col-md-offset-2 text-center'>
 	    <?php
	    echo paginate_results_function($item_per_page, $page_number, $get_total_rows, $total_pages);
	    ?>
	        </div></div></div>
 	    <?php
	}
}

// load vehicles in pagination
function JobCardSelect($user_pid,$JobNumber,$uid,$page,$user_center){
	global $dbc;
	$item_per_page = 5;
	if($uid){
	    // $uid = $_SESSION['id'];
	    if($page != ""){
	        $page_number = $page;
	        if(!is_numeric($page_number)){
	            $page_number = 1;
	        } //incase of invalid page number
	    }else{
	        $page_number = 1; //if there's no page number, set it to 1
	    }
	    //get total number of records from database for pagination
	    $results = "SELECT id FROM JobDetail WHERE JobNumber = '$JobNumber' AND CostCenter = '$user_center'";
	    $results = mysqli_query($dbc, $results);
	    $get_total_rows = mysqli_num_rows($results);
	    //break records into pages
	    $total_pages = ceil($get_total_rows/$item_per_page);

	    //get starting position to fetch the records
	    $page_position = (($page_number-1) * $item_per_page);

	    $q = "SELECT * FROM JobDetail WHERE JobNumber = '$JobNumber' AND CostCenter = '$user_center' ORDER BY id DESC LIMIT $page_position, $item_per_page";
	    $r = mysqli_query($dbc, $q);
	    ?>
	<div class="table-responsive">
	    <table class="table table-hover mails m-0 table table-actions-bar">
	        <thead>
	            <tr>
	                <th>Service type</th>
                    <th>Part No.</th>
                    <th>Description.</th>
                    <th>Qty</th>
                    <th>Total cost</th>
                    <th>Vendor</th>
                    <th>Issue date</th>
                    <th>Initials</th>
                    <th class="text-right">Action</th>
	            </tr>
	        </thead>
	        <tbody>
	<?php
	    while( $obj = mysqli_fetch_assoc( $r )) {
	    	date_default_timezone_set("Africa/Nairobi");
	        $id = $obj['id'];
	        $RepairType = $obj['RepairType'];
	        $RepairType1 = getMaintenanceName($RepairType);
	        $partDescription = $obj['partDescription'];
	        $PartNumber = $obj['PartNumber'];
	        $Quantity = $obj['Quantity'];
	        $JobTotalCost = $obj['JobTotalCost'];
	        $VendorName = $obj['VendorName'];
	        $VendorName1 = getVendorName($VendorName, $user_center);
	        $JobStartDate1 = $obj['JobStartDate'];
	        if (isset($JobStartDate1) && !empty($JobStartDate1)) {
				$x = strtotime($JobStartDate1);
				$JobStartDate = date('d M Y', $x);
			}
			else{
				$JobStartDate = "";
			}

	        $added_by = $obj['added_by'];
	        $initials = getUserInitials($added_by);
	        ?>
	            <tr id='<?php echo $id; ?>'>
	                <td style='width: 15%;'><?php echo $RepairType1; ?></td>
	                <td style='width: 10%;'><?php echo $PartNumber; ?></td>
	                <td style='width: 20%;'><?php echo $partDescription; ?></td>
	                <td style='width: 7%;'><?php echo $Quantity; ?></td>
	                <td style='width: 10%;'><?php echo $JobTotalCost; ?></td>
	                <td style='width: 15%;'><?php echo $VendorName1; ?></td>
	                <td style='width: 10%;'><?php echo $JobStartDate; ?></td>
	                <td style='width: 5%; text-align: center;'><?php echo $initials; ?></td>
	                <td class="text-right" style='width: 11%;'>
	                    <span class='JBcardID' user='<?php echo $user_pid; ?>' data='<?php echo $JobNumber; ?>' id='<?php echo $id; ?>' type='<?php echo $uid; ?>'>
                            <button class='btn btn-info editable-table-button btn-xs editJob' data-toggle="modal" data-target="#returnPart_<?php echo $id; ?>">Edit</button>
	                        <?php
	                            if ($uid == 1) {
	                                ?>
	                                    <!-- <button class='btn btn-danger editable-table-button btn-xs deletejbdetails'>Del</button> -->
	                                <?php
	                            }
	                        ?>
	                    </span>
	                </td>
	                <div id="returnPart_<?php echo $id; ?>" data="<?php echo $id; ?>" class="modal FleetJob fade" tabindex="-1" role="dialog" aria-labelledby="custom-width-modalLabel" aria-hidden="true" style="display: none;">
                    	<form class="edit_<?php echo $id;?>" id="edit_<?php echo $id;?>">
	                        <div class="modal-dialog modal-lg">
	                            <div class="modal-content">
	                                <div class="modal-header">
	                                	<div class="row">
	                                		<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
			                                    <h4 class="modal-title" id="custom-width-modalLabel">JOBCARD EDITING FOR <?php echo $obj['JobNumber'];?></h4>
	                                		</div>
	                                		<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 text-right">
			                                	<button type="submit" class="btn btn-xs btn-success waves-effect saveJobEdits">Save</button>
			                                	<button type="button" class="btn btn-xs btn-danger waves-effect" data-dismiss="modal">Close</button>
	                                		</div>
	                                	</div>
	                                </div>
	                                <div class="modal-body">
	                                    <div class="row m-b-5 m-t-10">
						                    <div class="col-md-6">
						                        <div class="form-group">
						                            <label for="field-1" class="control-label">Job Number: <span class="text-danger">*</span></label>
						                            <input type="text" class="form-control col-md-12 jobnumber_<?php echo $id;?>" name="jobnumber1" readonly value="<?php echo $obj['JobNumber']; ?>">
						                            <input type="hidden" name="user_center" class="user_center" value="<?php echo $user['center'];?>">
						                            <input type="hidden" name="vehicle" value="<?php echo $obj['VID']; ?>">
						                            <input type="hidden" class="recordId" value="<?php echo $id;?>">
						                        </div>
						                    </div>

						                    <div class="col-md-6">
						                        <div class="form-group">
						                        	<div class="form-group">
							                            <label for="field-2" class="control-label">Part Number:</label>
							                            <input type="text" class="form-control col-md-12 part_desc" name="part_desc" value="<?php echo getPartName($obj['PartNumber'], $obj['CostCenter']); ?>">
							                        </div>
						                        </div>
						                    </div>
						                </div>
						                <div class="row m-b-10">
						                    <div class="col-md-6">
						                        <div class="form-group">
						                            <label for="field-2" class="control-label">Part Description: <span class="text-danger">*</span></label>
						                            <input type="text" class="form-control col-md-12 partDesc_<?php echo $id;?>" name="partDesc" value="<?php echo $obj['partDescription']; ?>">
						                        </div>
						                    </div>
						                    <div class="col-md-6">
						                        <div class="form-group">
						                            <label for="field-2" class="control-label">Issue date:</label>
						                            <?php
			                                            date_default_timezone_set("Africa/Nairobi");
			                                            if ($obj['JobStartDate']) {
			                                                $JobStartDate = strtotime($obj['JobStartDate']);
			                                                $CMonth = date('d-m-Y', $JobStartDate);
			                                            }
			                                        ?>
						                            <input type="text" class="form-control col-md-12 IssueDate" name="stockIssueDate" id="datepicker" value="<?php echo $CMonth; ?>">
						                        </div>
						                    </div>
						                </div>
						                <div class="row m-b-10">
						                    <div class="col-md-4">
						                        <div class="form-group">
						                            <label for="field-1" class="control-label">Quantity: <span class="text-danger">*</span></label>
						                            <input type="text" name="quantity" autocomplete="off" class="form-control quantity_<?php echo $id; ?>" id="field-1" value="<?php echo $obj['Quantity']; ?>">
						                        </div>
						                    </div>
						                    <div class="col-md-4">
						                        <div class="form-group">
						                            <label for="field-2" class="control-label">Unit Price: <span class="text-danger">*</span></label>
						                            <input type="text" class="form-control col-md-12 unit_price_<?php echo $id; ?>" name="unit_price" value="<?php echo $obj['UnitPrice']; ?>">
						                        </div>
						                    </div>
						                    <div class="col-md-4">
						                        <div class="form-group">
						                            <label for="field-2" class="control-label">Job Total Cost: <span class="text-danger">*</span></label>
						                            <input type="text" class="form-control col-md-12 totalCost_<?php echo $id; ?>" name="totalCost" value="<?php echo $obj['JobTotalCost']; ?>">
						                        </div>
						                    </div>
						                </div>
						                <div class="row m-b-10">
						                    <div class="col-md-4">
						                        <div class="form-group">
						                            <label for="field-2" class="control-label">Vender / Service Center:</label>
						                            <input type="text" class="form-control col-md-12 services" name="services" value="<?php echo getVendorName($obj['VendorName'], $obj['CostCenter']); ?>">
						                        </div>
						                    </div>
						                    <div class="col-md-4">
						                        <div class="form-group">
						                            <label for="field-2" class="control-label">Reciept number:</label>
						                            <input type="text" name="reciept_numb" class="form-control col-md-12 reciept_numb" value="<?php echo $obj['ReceiptNo']; ?>">
						                        </div>
						                    </div>
						                    <div class="col-md-4">
						                        <div class="form-group">
						                            <label for="field-2" class="control-label">LPO Number:</label>
						                            <input type="text" name="lpo_numb" class="form-control col-md-12 lpo_numb" value="<?php echo $obj['POnumber']; ?>">
						                        </div>
						                    </div>
						                </div>
						                <div class="row m-b-10">
						                    <div class="col-md-4">
						                    	<div class="form-group">
						                            <label for="field-2" class="control-label">Work done / Repaire type:</label>
						                            <input type="text" name="lpo_numb" class="form-control col-md-12 lpo_numb" value="<?php echo getMaintenanceName($obj['RepairType']); ?>">
						                        </div>
						                    </div>
						                    <div class="col-md-4">
						                        <div class="form-group">
						                            <label for="field-1" class="control-label">Waranty Date:</label>
						                            <input type="text" class="form-control col-md-12 waranty_date" name="waranty_date" id="datepicker" value="<?php echo $obj['WarantyDate']; ?>">
						                        </div>
						                    </div>
						                    <div class="col-md-4">
						                        <div class="form-group">
						                            <label for="field-2" class="control-label">Warranty Description:</label>
						                            <input type="text" class="form-control col-md-12 waranty_desc" name="waranty_desc" value="<?php echo $obj['WarrantyDescription']; ?>">
						                        </div>
						                    </div>
						                </div>
						                <div class="row m-b-15">
						                    <div class="col-md-12">
						                        <div class="form-group">
						                            <label for="field-2" class="control-label">Work Done Description:</label>
						                            <input type="text" class="form-control col-md-12 remarks" name="remarks" value="<?php echo $obj['JobDescription']; ?>">
						                        </div>
						                    </div>
						                </div>
	                                </div>
	                            </div>
	                        </div>
                        </form>
                    </div>
	            </tr>
	        <?php
	    }
	    ?>
	    </tbody></table></div>
	    <div class='row'>
	        <div class='col-sm-12'>
	            <div class='col-sm-7 col-md-offset-2 text-center'>
	    <?php
	    echo paginate_results_function($item_per_page, $page_number, $get_total_rows, $total_pages);
	    ?>
	        </div></div></div>
	    <?php
	}
}

// load vehicles in pagination
function FuelDetails($user_pid,$FillNumber,$uid,$page,$user_center){
	global $dbc;
	$item_per_page = 5;
	if($uid){
	    // $uid = $_SESSION['id'];
	    if($page != ""){
	        $page_number = $page;
	        if(!is_numeric($page_number)){
	            $page_number = 1;
	        } //incase of invalid page number
	    }else{
	        $page_number = 1; //if there's no page number, set it to 1
	    }
	    //get total number of records from database for pagination
	    $results = "SELECT id FROM Tank_Details WHERE FillNumber = '$FillNumber' AND costCenter = '$user_center'";
	    $results = mysqli_query($dbc, $results);
	    $get_total_rows = mysqli_num_rows($results);
	    //break records into pages
	    $total_pages = ceil($get_total_rows/$item_per_page);

	    //get starting position to fetch the records
	    $page_position = (($page_number-1) * $item_per_page);

	    $q = "SELECT v.FillNumber AS 'FillNumber',
		            v.recievedLitres AS 'recievedLitres',
		            v.id AS 'id',
		            v.dateRecieved AS 'dateRecieved',
		            v.costPerLitre AS 'costPerLitre',
		            v.supplire AS 'supplire',
		            v.newBalance AS 'newBalance',
		            v.Location AS 'Location',
		            v.Issued AS 'Issued',
		            v.costCenter AS 'costCenter',
		            f.fuel_type AS 'fuel_type',
		            v.fuelType AS 'fuelType',
		            t.capacity AS 'capacity',
		            t.description AS 'description',
		            u.firstname AS 'firstname',
		            u.lastname AS 'lastname',
		            u.avatar AS 'avatar',
		            v.date_added AS 'date_added',
		            (v.newBalance - v.Issued) AS 'remainder'
		            FROM Tank_Details AS v
		            JOIN FUEL_TYPE AS f ON v.fuelType = f.code AND v.costCenter = f.costCenter
		            JOIN Tanks AS t ON v.Location = t.code AND v.costCenter = t.costCenter
		            JOIN users AS u ON v.adde_by = u.id
		            WHERE v.FillNumber = '$FillNumber' AND v.costCenter = '$user_center' ORDER BY v.id DESC LIMIT $page_position, $item_per_page";
	    // $q = "SELECT * FROM Tank_Details WHERE FillNumber = '$FillNumber' AND costCenter = '$user_center' ORDER BY id DESC LIMIT $page_position, $item_per_page";
	    $r = mysqli_query($dbc, $q);
	    ?>
	<div class="table-responsive">
	    <table class="table table-hover mails m-0 table table-actions-bar">
	        <thead>
	            <tr>
	                <th>Date Rec</th>
                    <th>Supplier</th>
                    <th>Litres Rec</th>
                    <th>New Qty</th>
                    <th>Cost/ltr</th>
                    <th class="text-right">Action</th>
	            </tr>
	        </thead>
	        <tbody>
	<?php
	    while( $obj = mysqli_fetch_assoc( $r )) {
	    	date_default_timezone_set("Africa/Nairobi");
	        $id = $obj['id'];
	        $costPerLitre = $obj['costPerLitre'];
	        $supplire = $obj['supplire'];
	        $supplire1 = getSupplierName($supplire, $user_center);
	        $recievedLitres = $obj['recievedLitres'];
	        $adde_by = $obj['adde_by'];
	        $FillNumber = $obj['FillNumber'];
	        $Location = $obj['Location'];
	        $description = $obj['description'];
	        $firstname = $obj['firstname'];
	        $lastname = $obj['lastname'];
	        $BothNames = $firstname." ".$lastname;
	        $avatar = $obj['avatar'];
	        $fuelType = $obj['fuel_type'];
	        $newBalance = $obj['newBalance'];
	        $adde_by = $obj['adde_by'];
	        $remainder = $obj['remainder'];
	        $Issued = $obj['Issued'];

	        $date_added = $obj['date_added'];

	        if (isset($date_added) && !empty($date_added)) {
				$y = strtotime($date_added);
				$Date = date('d M Y', $y);
				$Time = date('H : i', $y);
			}

	        $JobStartDate1 = $obj['dateRecieved'];
	        if (isset($JobStartDate1) && !empty($JobStartDate1)) {
				$x = strtotime($JobStartDate1);
				$dateRecieved = date('d M Y', $x);
			}
			else{
				$dateRecieved = "";
			}
	        ?>
	            <tr id='<?php echo $id; ?>'>
	                <td style='width: 10%;'><?php echo $dateRecieved; ?></td>
	                <td style='width: 30%;'><?php echo $supplire1; ?></td>
	                <td style='width: 15%;'><?php echo $recievedLitres; ?></td>
	                <td style='width: 10%;'><?php echo $newBalance; ?></td>
	                <td style='width: 10%;'><?php echo $costPerLitre; ?></td>
	                <td class="text-right" style='width: 11%;'>
	                    <span class='JBcardID' user='<?php echo $user_pid; ?>' data='<?php echo $FillNumber; ?>' id='<?php echo $id; ?>' type='<?php echo $uid; ?>'>
	                        <button class='btn btn-info editable-table-button btn-xs editJob' data-toggle="modal" data-target="#fuelDetail_<?php echo $id; ?>">View</button>
	                    </span>
	                </td>

	                <div id="fuelDetail_<?php echo $id; ?>" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none; padding-left: 0px;">
                        <div class="modal-dialog">
                            <div class="modal-content pickedPard">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h4 class="modal-title"><?php echo $description; ?>, TRANSACTION NO: <?php echo $FillNumber; ?> </h4>

                                </div>
                                <div class="modal-body" style="color: #000;">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="field-1" class="control-label">Received Litres:</label>
                                                <input type="text" class="form-control" value="<?php echo $recievedLitres; ?>">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="field-2" class="control-label">Date Recieved:</label>
                                                <input type="text" class="form-control" value="<?php echo $dateRecieved; ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="field-3" class="control-label">Cost per litre:</label>
                                                <input type="text" class="form-control" value="<?php echo $costPerLitre; ?>">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="field-3" class="control-label">Supplier:</label>
                                                <input type="text" class="form-control" value="<?php echo $supplire1; ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="field-3" class="control-label">New Balance:</label>
                                                <input type="text" class="form-control" value="<?php echo $newBalance; ?>">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="field-3" class="control-label">Fuel type:</label>
                                                <input type="text" class="form-control" value="<?php echo $fuelType; ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="field-3" class="control-label">Issued:</label>
                                                <input type="text" class="form-control" value="<?php echo $Issued; ?>">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="field-3" class="control-label">Rmaining fuel:</label>
                                                <input type="text" class="form-control" value="<?php echo $remainder; ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row inbox-widget p-x-10">
                                    	<div class="col-md-12">
	                                    	<div class="inbox-item">
	                                            <div class="inbox-item-img"><img src="/images/users/<?php echo $avatar; ?>" class="img-circle" alt=""></div>

	                                            <p class="inbox-item-author">Added By: <?php echo $BothNames; ?></p>
	                                            <p class="inbox-item-text">On: <?php echo $Date; ?></p>
	                                            <p class="inbox-item-date">At: <?php echo $Time; ?></p>
	                                        </div>
                                    	</div>
                                    </div>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-sm btn-danger waves-effect" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
	            </tr>
	        <?php
	    }
	    ?>
	    </tbody></table></div>
	    <div class='row'>
	        <div class='col-sm-12'>
	            <div class='col-sm-7 col-md-offset-2 text-center'>
	    <?php
	    echo paginate_results_function($item_per_page, $page_number, $get_total_rows, $total_pages);
	    ?>
	        </div></div></div>
	    <?php
	}
}

// load vehicles in pagination
function load_all_locations($page,$user_email,$uid,$user_center){
	global $dbc;
	$item_per_page = 20;
	if($user_email && $uid){
	    // $uid = $_SESSION['id'];
	    if($page != ""){
	        $page_number = $page;
	        if(!is_numeric($page_number)){
	            $page_number = 1;
	        } //incase of invalid page number
	    }else{
	        $page_number = 1; //if there's no page number, set it to 1
	    }
	    //get total number of records from database for pagination
	    $results = "SELECT id FROM LOCATION Where id !='' AND costCenter = '$user_center'";
	    $results = mysqli_query($dbc, $results);
	    $get_total_rows = mysqli_num_rows($results);
	    //break records into pages
	    $total_pages = ceil($get_total_rows/$item_per_page);

	    //get starting position to fetch the records
	    $page_position = (($page_number-1) * $item_per_page);

	    $q = "SELECT * FROM LOCATION WHERE costCenter = '$user_center' ORDER BY id DESC LIMIT $page_position, $item_per_page";
	    $r = mysqli_query($dbc, $q);
	    ?>
	<div class="table-responsive">
	    <table class="table table-hover mails m-0 table table-actions-bar">
	        <thead>
	            <tr>
	                <th>Location</th>
	                <th>Description.</th>
	                <th class="text-right">Action</th>
	            </tr>
	        </thead>
	        <tbody>
	<?php
	    while( $obj = mysqli_fetch_assoc( $r )) {
	        $id = $obj['id'];
	        $location = $obj['location'];
	        $description = $obj['description'];
	        $added_by = $obj['added_by'];
	        ?>
	            <tr id='<?php echo $id; ?>'>
	                <td style='width: 35%;'><?php echo $location; ?></td>
	                <td style='width: 50%;'><?php echo $description; ?></td>
	                <td class="text-right" style='width: 11%;'>
	                    <span class='LocationID' data='<?php echo $description; ?>' id='<?php echo $id; ?>' page='<?php echo $gg; ?>' type='<?php echo $uid; ?>'>
	                        <button class='btn btn-info editable-table-button btn-xs editLocation'>Edit</button>
	                        <?php
	                            if ($uid == 1) {
	                                ?>
	                                    <button class='btn btn-danger editable-table-button btn-xs deleteLocation'>Del</button>
	                                <?php
	                            }
	                        ?>
	                    </span>
	                </td>
	            </tr>
	        <?php
	    }
	    ?>
	    </tbody></table></div>
	    <div class='row'>
	        <div class='col-sm-12'>
	            <div class='col-sm-7 col-md-offset-2 text-center'>
	    <?php
	    echo paginate_function($item_per_page, $page_number, $get_total_rows, $total_pages);
	    ?>
	        </div></div></div>
	    <?php
	}
}


// load vehicles in pagination
function global_seached_vehicle($page,$srcval){
	global $dbc;
	$item_per_page = 20;
	if($srcval && strlen($srcval) >= 4){

	    if($page != ""){
	        $page_number = $page;
	        if(!is_numeric($page_number)){
	            $page_number = 1;
	        }
	    }else{
	        $page_number = 1;
	    }

    	$sql = "SELECT v.id AS 'id'
		        FROM VEHICLES AS v
		        WHERE (v.vid like '%$srcval%' OR v.vid2 like '%$srcval%' OR v.base like '%$srcval%')";
	    

	    //get total number of records from database for pagination
	    // $results = "SELECT id FROM VEHICLES Where id !='' AND costcentre = '$center'";
	    $results = mysqli_query($dbc, $sql);
	    $get_total_rows = mysqli_num_rows($results);
	    //break records into pages
	    $total_pages = ceil($get_total_rows/$item_per_page);
	    //get starting position to fetch the records
	    $page_position = (($page_number-1) * $item_per_page);
	    	$q = "SELECT v.id AS 'id',
	        v.vid AS 'vid',
	        v.vid2 AS 'vid2',
	        b.description AS 'BaseDescription',
	        v.odometer AS 'odometer',
	        ma.description AS 'MakeDescription',
	        mo.description AS 'ModelDescription',
	        vt.description AS 'VhlTypeDescription'
	        FROM VEHICLES AS v
	        LEFT JOIN MODELS AS mo ON v.model = mo.model
	        LEFT JOIN VHL_TYPE AS vt ON v.vtype = vt.vhl_type
	        LEFT JOIN BASE AS b ON v.base = b.base AND v.costcentre = b.costCenter
	        LEFT JOIN MAKES AS ma ON v.make = ma.make AND v.costcentre = ma.costCenter
	        WHERE (v.vid like '%$srcval%' OR v.vid2 like '%$srcval%') ORDER BY v.vid ASC LIMIT $page_position, $item_per_page";

	    $r = mysqli_query($dbc, $q);
	    ?>
<div class="col-md-12">
	    <div class="panel panel-blur with-scroll animated zoomIn" style="background-size: 1273px 716px; background-position: 0px -128px;">
	<div class="panel-body">
	<div class="table-responsive">
	    <table class="table table-hover mails m-0 table table-actions-bar">
	        <thead>
	            <tr>
	                <th>Vehicle</th>
	                <th>Fleet No.</th>
	                <th>Make</th>
	                <th>Model</th>
	                <th>Vehicle type</th>
	                <th>Base</th>
	                <th>Odometer</th>
	            </tr>
	        </thead>
	        <tbody>
	<?php
	    while( $obj = mysqli_fetch_assoc( $r )) {
	        $id = $obj['id'];
	        $vid = $obj['vid'];
	        $make = $obj['MakeDescription'];
	        $model = $obj['ModelDescription'];
	        $vtype = $obj['VhlTypeDescription'];
	        $vid2 = $obj['vid2'];
	        $base = $obj['BaseDescription'];
	        $odometer = $obj['odometer'];
	        ?>
	            <tr id='<?php echo $id; ?>'>
	                <td style='width: 13%; text-transform: uppercase;'><?php echo $vid; ?></td>
	                <td style='width: 11%; text-transform: uppercase;'><?php echo $vid2; ?></td>
	                <td style='width: 13%;'><?php echo $make; ?></td>
	                <td style='width: 15%;'><?php echo $model; ?></td>
	                <td style='width: 15%;'><?php echo $vtype; ?></td>
	                <td style='width: 17%;'><?php echo $base; ?></td>
	                <td class="text-center" style='width: 5%;'><?php echo $odometer; ?></td>
	            </tr>
	        <?php
	    }
	    ?>
	    </tbody></table></div>
	    <div class='row'>
	        <div class='col-sm-12'>
	            <div class='col-sm-7 col-md-offset-2 text-center'>
	    <?php
	    echo paginate_function($item_per_page, $page_number, $get_total_rows, $total_pages);
	    ?>
	        </div>
	        <div class="col-sm-3 text-right m-t-15">
	        	<button type="button" style="margin-right: -8px;" class="btn btn-sm btn-github waves-effect waves-light">
                    <span class="btn-label"><i class="fa fa-car"></i> </span><?php echo number_format($get_total_rows); ?>
                </button>
	        	<!-- <button class="btn btn-github editable-table-button btn-xs" style="margin-right: -8px;"></button> -->
	        </div>
	    </div>
	</div>
</div>
</div>
</div>
	    <?php
	}
}


// load vehicles in pagination
function leads_by_current_day($page,$user_email,$uid,$center,$search){
	global $dbc;
	$item_per_page = 20;
	if($user_email && $uid){

		if (strlen($search) >= 4) {
		 	$postedVal = $search;
		}else{
			$postedVal = "noSearchedCar";
		}

	    if($page != ""){
	        $page_number = $page;
	        if(!is_numeric($page_number)){
	            $page_number = 1;
	        }
	    }else{
	        $page_number = 1;
	    }

	    if ($postedVal == "noSearchedCar") {
		    $sql = "SELECT id FROM VEHICLES Where id !='' AND costcentre = '$center'";
	    }else{
	    	$sql = "SELECT v.id AS 'id'
		        FROM VEHICLES AS v
		        WHERE (v.vid like '%$postedVal%' OR v.vid2 like '%$postedVal%' OR v.base like '%$postedVal%') AND v.costcentre = '$center'";
	    }

	    //get total number of records from database for pagination
	    // $results = "SELECT id FROM VEHICLES Where id !='' AND costcentre = '$center'";
	    $results = mysqli_query($dbc, $sql);
	    $get_total_rows = mysqli_num_rows($results);
	    //break records into pages
	    $total_pages = ceil($get_total_rows/$item_per_page);
	    //get starting position to fetch the records
	    $page_position = (($page_number-1) * $item_per_page);

	    if ($postedVal == "noSearchedCar") {
		    $q = "SELECT v.id AS 'id',
			v.vid AS 'vid',
			v.vid2 AS 'vid2',
			b.description AS 'BaseDescription',
			v.odometer AS 'odometer',
			ma.description AS 'MakeDescription',
	        mo.description AS 'ModelDescription',
	        vt.description AS 'VhlTypeDescription'
	        FROM VEHICLES AS v
	        LEFT JOIN MODELS AS mo ON v.model = mo.model
	        LEFT JOIN VHL_TYPE AS vt ON v.vtype = vt.vhl_type
	        LEFT JOIN BASE AS b ON v.base = b.base AND v.costcentre = b.costCenter
	        LEFT JOIN MAKES AS ma ON v.make = ma.make AND v.costcentre = ma.costCenter
	        WHERE v.costcentre = '$center' ORDER BY v.vid ASC LIMIT $page_position, $item_per_page";
	    }else{
	    	$q = "SELECT v.id AS 'id',
	        v.vid AS 'vid',
	        v.vid2 AS 'vid2',
	        b.description AS 'BaseDescription',
	        v.odometer AS 'odometer',
	        ma.description AS 'MakeDescription',
	        mo.description AS 'ModelDescription',
	        vt.description AS 'VhlTypeDescription'
	        FROM VEHICLES AS v
	        LEFT JOIN MODELS AS mo ON v.model = mo.model
	        LEFT JOIN VHL_TYPE AS vt ON v.vtype = vt.vhl_type
	        LEFT JOIN BASE AS b ON v.base = b.base AND v.costcentre = b.costCenter
	        LEFT JOIN MAKES AS ma ON v.make = ma.make AND v.costcentre = ma.costCenter
	        WHERE (v.vid like '%$postedVal%' OR v.vid2 like '%$postedVal%' OR v.base like '%$postedVal%' OR b.description like '%$postedVal%') AND v.costcentre = '$center' ORDER BY v.vid ASC LIMIT $page_position, $item_per_page";
	    }

	    $r = mysqli_query($dbc, $q);
	    ?>
	<div class="table-responsive">
	    <table class="table table-hover mails m-0 table table-actions-bar">
	        <thead>
	            <tr>
	                <th>Vehicle</th>
	                <th>Fleet No.</th>
	                <th>Make</th>
	                <th>Model</th>
	                <th>Vehicle type</th>
	                <th>Base</th>
	                <th>Odometer</th>
	                <th class="text-right">Action</th>
	            </tr>
	        </thead>
	        <tbody>
	<?php
	    while( $obj = mysqli_fetch_assoc( $r )) {
	        $id = $obj['id'];
	        $vid = $obj['vid'];
	        $make = $obj['MakeDescription'];
	        $model = $obj['ModelDescription'];
	        $vtype = $obj['VhlTypeDescription'];
	        $vid2 = $obj['vid2'];
	        $base = $obj['BaseDescription'];
	        $odometer = $obj['odometer'];
	        ?>
	            <tr id='<?php echo $id; ?>'>
	                <td style='width: 13%; text-transform: uppercase;'><?php echo $vid; ?></td>
	                <td style='width: 11%; text-transform: uppercase;'><?php echo $vid2; ?></td>
	                <td style='width: 13%;'><?php echo $make; ?></td>
	                <td style='width: 15%;'><?php echo $model; ?></td>
	                <td style='width: 15%;'><?php echo $vtype; ?></td>
	                <td style='width: 17%;'><?php echo $base; ?></td>
	                <td class="text-center" style='width: 5%;'><?php echo $odometer; ?></td>
	                <td class="text-right" style='width: 11%;'>
	                    <span class='vhlID' data='<?php echo $vid; ?>' id='<?php echo $id; ?>' page='<?php echo $gg; ?>' type='<?php echo $uid; ?>'>
	                        <button class='btn btn-info editable-table-button btn-xs editVhl'>Edit</button>
	                        <?php
	                            if ($uid == 1) {
	                                ?>
	                                    <!-- <button class='btn btn-danger editable-table-button btn-xs deleteVhl'>Del</button> -->
	                                <?php
	                            }
	                        ?>
	                    </span>
	                </td>
	            </tr>
	        <?php
	    }
	    ?>
	    </tbody></table></div>
	    <div class='row'>
	        <div class='col-sm-12'>
	            <div class='col-sm-7 col-md-offset-2 text-center'>
	    <?php
	    echo paginate_function($item_per_page, $page_number, $get_total_rows, $total_pages);
	    ?>
	        </div>
	        <div class="col-sm-3 text-right m-t-15">
	        	<button type="button" style="margin-right: -8px;" class="btn btn-sm btn-github waves-effect waves-light">
                    <span class="btn-label"><i class="fa fa-car"></i> </span><?php echo number_format($get_total_rows); ?>
                </button>
	        	<!-- <button class="btn btn-github editable-table-button btn-xs" style="margin-right: -8px;"></button> -->
	        </div>
	    </div>
	</div>
	    <?php
	}
}

// load vehicles in pagination
function Load_drivers($page,$user_email,$uid,$user_center){
	global $dbc;
	$item_per_page = 20;
	if($user_email && $uid){
	    // $uid = $_SESSION['id'];
	    if($page != ""){
	        $page_number = $page;
	        if(!is_numeric($page_number)){
	            $page_number = 1;
	        } //incase of invalid page number
	    }else{
	        $page_number = 1; //if there's no page number, set it to 1
	    }
	    //get total number of records from database for pagination
	    $results = "SELECT id FROM DRIVER Where id !='' AND costCenter = '$user_center'";
	    $results = mysqli_query($dbc, $results);
	    $get_total_rows = mysqli_num_rows($results);
	    //break records into pages
	    $total_pages = ceil($get_total_rows/$item_per_page);

	    //get starting position to fetch the records
	    $page_position = (($page_number-1) * $item_per_page);

	    $q = "SELECT id, first_name, other_name, mobile, employ_number, Department, licence_number, title, licence_expirery FROM DRIVER WHERE costCenter = '$user_center' ORDER BY employ_number ASC LIMIT $page_position, $item_per_page";
	    $r = mysqli_query($dbc, $q);
	    ?>
	<div class="table-responsive">
	    <table class="table table-hover mails m-0 table table-actions-bar">
	        <thead>
	            <tr>
	                <th>Name</th>
	                <th>Emp No.</th>
	                <th>Dept.</th>
	                <th>Licence</th>
	                <th>DL Expiry</th>
	                <th class="text-center">Title</th>
	                <th class="text-right">Action</th>
	            </tr>
	        </thead>
	        <tbody>
	<?php
	    while( $obj = mysqli_fetch_assoc( $r )) {
	        date_default_timezone_set("Africa/Nairobi");
	        $id = $obj['id'];
	        $first_name = $obj['first_name'];
	        $other_name = $obj['other_name'];
	        $employ_number = $obj['employ_number'];
	        $licence_expirery = $obj['licence_expirery'];
	        $title = $obj['title'];
	        $Department = getDepartmentName($obj['Department'], $user_center);


            if (isset($licence_expirery) && !empty($licence_expirery)) {
                $month = strtotime($licence_expirery);
                $fullMonth = date('d M Y', $month);
            }
            else{
            	$fullMonth = "";
            }

	        $mobile = $obj['mobile'];
	        $licence_number = $obj['licence_number'];
	        ?>
	            <tr id='<?php echo $id; ?>'>
	                <td style='width: 27%;'><?php echo $first_name ." ". $other_name; ?></td>
	                <td style='width: 11%;'><?php echo $employ_number; ?></td>
	                <td style='width: 16%;'><?php echo $Department; ?></td>
	                <td style='width: 16%;'><?php echo $licence_number; ?></td>
	                <td style='width: 10%;'><?php echo $fullMonth; ?></td>
	                <td class="text-right" style='width: 9%;'><?php echo $title; ?></td>
	                <td class="text-right" style='width: 10%;'>
	                    <span class='vhlID' data='<?php echo $first_name; ?>' id='<?php echo $id; ?>' type='<?php echo $uid; ?>'>
	                        <button class='btn btn-info editable-table-button btn-xs editVhl'>Edit</button>
	                        <?php
	                            if ($uid == 1) {
	                                ?>
	                                    <button class='btn btn-danger editable-table-button btn-xs deleteVhl'>Del</button>
	                                <?php
	                            }
	                        ?>
	                    </span>
	                </td>
	            </tr>
	        <?php
	    }
	    ?>
	    </tbody></table></div>
	    <div class='row'>
	        <div class='col-sm-12'>
	            <div class='col-sm-7 col-md-offset-2 text-center'>
	    <?php
	    echo paginate_function($item_per_page, $page_number, $get_total_rows, $total_pages);
	    ?>
	        </div></div></div>
	    <?php
	}
}

function paginate_function($item_per_page, $current_page, $total_records, $total_pages)
{
    $pagination = '';
    if($total_pages > 0 && $total_pages != 1 && $current_page <= $total_pages){ //verify total pages and current page number
        $pagination .= '<ul class="pagination pagination-split">';

        $right_links    = $current_page + 3;
        $previous       = $current_page - 1; //previous link
        $next           = $current_page + 1; //next link
        $first_link     = true; //boolean var to decide our first link

        if($current_page > 1){
            $previous_link = ($previous==0)? 1: $previous;
            //first link
            $pagination .= '<li class="footable-page-arrow pagedata" data="1"><a class="pagePrevNext">«</a></li>';
            //previous link
            $pagination .= '<li class="footable-page-arrow pagedata" data="'.$previous_link.'"><a class="pagePrevNext">‹</a></li>';

                for($i = ($current_page-2); $i < $current_page; $i++){ //Create left-hand side links
                    if($i > 0){
                        $pagination .= '<li class="footable-page-arrow pagedata" id="'.$i.'" data="'.$i.'"><a class="pagePrevNext" data-page="'.$i.'" title="Page '.$i.'">'.$i.'</a></li>';
                    }
                }
            $first_link = false; //set first link to false
        }

        if($first_link){ //if current active page is first link
            $pagination .= '<li class="paginate_button disabled" aria-controls="datatable-buttons" tabindex="0" id="datatable-buttons_previous"><a>«</a></li>
            ';
        }elseif($current_page == $total_pages){ //if it's the last active link
            $pagination .= '<li class="paginate_button next disabled" aria-controls="datatable-buttons" tabindex="0" id="datatable-buttons_next"><a>End</a></li>';
        }else{ //regular current link
            $pagination .= '<li class="paginate_button active" aria-controls="datatable-buttons" tabindex="0"><a>'.$current_page.'</a></li>
            ';
        }

        for($i = $current_page+1; $i < $right_links ; $i++){ //create right-hand side links
            if($i<=$total_pages){
                $pagination .= '<li class="paginate_button pagedata" data="'.$i.'" aria-controls="datatable-buttons"><a class="pagePrevNext">'.$i.'</a></li>';
            }
        }

        if($current_page < $total_pages){
                $next_link = ($next > $total_pages)? $total_pages: $next;
                //next link
                $pagination .= '<li class="paginate_button pagedata" data="'.$next_link.'" aria-controls="datatable-buttons"><a class="pagePrevNext">›</a></li>';
                //last link
                $pagination .= '<li class="paginate_button next pagedata" data="'.$total_pages.'" id="datatable-buttons_next"><a class="pagePrevNext">»</a></li>';
        }

        $pagination .= '</ul>';
    }
    return $pagination; //return pagination links
}

function get_user_avatars($user_id){
	global $dbc;
	$q = "SELECT * FROM users WHERE id = '$user_id'";
	$data = mysqli_query($dbc, $q);
	while ($row = mysqli_fetch_assoc($data)):
		$avatar = $row['avatar'];
		$firstname = $row['firstname'];
		$lastname = $row['lastname'];
		$names = $firstname ." ". $lastname;
		$results .= "<td style='width: 6%;'><img src='../images/users/".$avatar."' class='img-circle thumb-sm' /></td>";
		$results .= "<td style='width: 20%;'>".$names."</td>";
		echo $results;
	endwhile;
}

// load audit logs
function logs_by_current_page($page,$user_email,$uid){
	global $dbc;
	$item_per_page = 20;
	if($user_email && $uid){
	    // $uid = $_SESSION['id'];
	    if($page != ""){
	        $page_number = $page;
	        if(!is_numeric($page_number)){
	            $page_number = 1;
	        } //incase of invalid page number
	    }else{
	        $page_number = 1; //if there's no page number, set it to 1
	    }
	    //get total number of records from database for pagination
	    $results = "SELECT id FROM audit_log Where id !='' ";
	    $results = mysqli_query($dbc, $results);
	    $get_total_rows = mysqli_num_rows($results);
	    //break records into pages
	    $total_pages = ceil($get_total_rows/$item_per_page);

	    //get starting position to fetch the records
	    $page_position = (($page_number-1) * $item_per_page);

	    $q = "SELECT * FROM audit_log ORDER BY id DESC LIMIT $page_position, $item_per_page";
	    $r = mysqli_query($dbc, $q);
	    ?>
	<div class="table-responsive">
	    <table class="table table-hover mails m-0 table table-actions-bar">
	        <thead>
	            <tr>
	                <th>User</th>
	                <th>User</th>
	                <th>Action</th>
	                <th>Date Added</th>
	                <th>Public IP</th>
	            </tr>
	        </thead>
	        <tbody>
	<?php
	    while( $obj = mysqli_fetch_assoc( $r )) {
	    	date_default_timezone_set("Africa/Nairobi");

	        $user_id = $obj['user_id'];
	        $type = $obj['type'];
	        $action = $obj['action'];
	        $ip = $obj['ip'];
	        $date_added = $obj['date_added'];

	        $MonthYear = strtotime($date_added);
	        $final = date('d M Y, g : i A', $MonthYear);

	        ?>
	            <tr>

	                <?php get_user_avatars($user_id) ?>
	                <td style='width: 40%;'><?php echo $action; ?></td>
	                <td style='width: 20%;'><?php echo $final; ?></td>
	                <td style='width: 20%;'><?php echo $ip; ?></td>
	            </tr>
	        <?php
	    }
	    ?>
	    </tbody></table></div>
	    <div class='row'>
	        <div class='col-sm-12'>
	            <div class='col-sm-7 col-md-offset-2 text-center'>
	    <?php
	    echo paginate_function($item_per_page, $page_number, $get_total_rows, $total_pages);
	    ?>
	        </div></div></div>
	    <?php
	}
}


// load fuel issuance in pagination
function getAllBases($page,$user_email,$uid,$ccenter,$search){
	global $dbc;
	$item_per_page = 20;
	if($user_email && $uid){
	    
	    if (strlen($search) >= 3) {
		 	$postedVal = $search;
		}else{
			$postedVal = "noSearchedCar";
		}

	    if($page != ""){
	        $page_number = $page;
	        if(!is_numeric($page_number)){
	            $page_number = 1;
	        }
	    }else{
	        $page_number = 1;
	    }

	    if ($postedVal == "noSearchedCar") {
		    $sql = "SELECT id FROM BASE Where id !='' AND costCenter = '$ccenter'";
	    }else{
	    	$sql = "SELECT id FROM BASE WHERE (base like '%$postedVal%' OR description like '%$postedVal%') AND costCenter = '$ccenter'";
	    }

	    //get total number of records from database for pagination
	    $results = mysqli_query($dbc, $sql);
	    $get_total_rows = mysqli_num_rows($results);
	    //break records into pages
	    $total_pages = ceil($get_total_rows/$item_per_page);

	    //get starting position to fetch the records
	    $page_position = (($page_number-1) * $item_per_page);

	    if ($postedVal == "noSearchedCar") {
		    $q = "SELECT * FROM BASE WHERE costCenter = '$ccenter' ORDER BY id DESC LIMIT $page_position, $item_per_page";
	    }
	    else{
	    	$q = "SELECT * FROM BASE WHERE (base like '%$postedVal%' OR description like '%$postedVal%') AND costCenter = '$ccenter' ORDER BY base ASC LIMIT $page_position, $item_per_page";
	    }
	    $r = mysqli_query($dbc, $q);
	    ?>
	<div class="table-responsive">
	    <table class="table table-hover mails m-0 table table-actions-bar">
	        <thead>
	            <tr>
	                <th>Code</th>
	                <th>Description</th>
	                <th class="text-right">Action</th>
	            </tr>
	        </thead>
	        <tbody>
	<?php
	    while( $obj = mysqli_fetch_assoc( $r )) {
	        $id = $obj['id'];
	        $base = $obj['base'];
	        $description = $obj['description'];
	        ?>
	            <tr class="<?php echo $id; ?>">
	                <td style='width: 20%;'><?php echo $base; ?></td>
	                <td style='width: 69%;'><?php echo $description; ?></td>
	                <td class="text-right" style='width: 11%;'>
	                    <span class='incidentID' data='<?php echo $page; ?>' id='<?php echo $id; ?>' type='<?php echo $base; ?>'>
	                        <button class='btn btn-info editable-table-button btn-xs editBase'>Edit</button>
	                        <?php
	                            if ($uid == 1) {
	                                ?>
	                                    <button class='btn btn-danger editable-table-button btn-xs deleteBase'>Del</button>
	                                <?php
	                            }
	                        ?>
	                    </span>
	                </td>
	            </tr>
	        <?php
	    }
	    ?>
	    </tbody></table></div>
	    <div class='row'>
	        <div class='col-sm-12'>
	            <div class='col-sm-7 col-md-offset-2 text-center'>
				    <?php
				    echo paginate_function($item_per_page, $page_number, $get_total_rows, $total_pages);
				    ?>
		        </div>
		        <div class="col-sm-3 text-right m-t-15">
		        	<button type="button" style="margin-right: -8px;" class="btn btn-sm btn-github waves-effect waves-light">
	                    <span class="btn-label"><i class="ti-paint-bucket"></i> </span><?php echo number_format($get_total_rows); ?>
	                </button>
		        </div>
		    </div>
		</div>
	    <?php
	}
}

// get all bases
function getAllBasesOld($page,$user_email,$uid,$ccenter){
	global $dbc;
	$item_per_page = 20;
	if($user_email && $uid){
	    // $uid = $_SESSION['id'];
	    if($page != ""){
	        $page_number = $page;
	        if(!is_numeric($page_number)){
	            $page_number = 1;
	        } //incase of invalid page number
	    }else{
	        $page_number = 1; //if there's no page number, set it to 1
	    }
	    //get total number of records from database for pagination
	    $results = "SELECT id FROM BASE Where id !='' AND costCenter = '$ccenter'";
	    $results = mysqli_query($dbc, $results);
	    $get_total_rows = mysqli_num_rows($results);
	    //break records into pages
	    $total_pages = ceil($get_total_rows/$item_per_page);

	    //get starting position to fetch the records
	    $page_position = (($page_number-1) * $item_per_page);

	    $q = "SELECT * FROM BASE WHERE costCenter = '$ccenter' ORDER BY id DESC LIMIT $page_position, $item_per_page";
	    $r = mysqli_query($dbc, $q);
	    ?>
	<div class="table-responsive">
	    <table class="table table-hover mails m-0 table table-actions-bar">
	        <thead>
	            <tr>
	                <th>Code</th>
	                <th>Description</th>
	                <th class="text-right">Action</th>
	            </tr>
	        </thead>
	        <tbody>
	<?php
	    while( $obj = mysqli_fetch_assoc( $r )) {
	        $id = $obj['id'];
	        $base = $obj['base'];
	        $description = $obj['description'];
	        ?>
	            <tr class="<?php echo $id; ?>">
	                <td style='width: 20%;'><?php echo $base; ?></td>
	                <td style='width: 69%;'><?php echo $description; ?></td>
	                <td class="text-right" style='width: 11%;'>
	                    <span class='incidentID' data='<?php echo $page; ?>' id='<?php echo $id; ?>' type='<?php echo $base; ?>'>
	                        <button class='btn btn-info editable-table-button btn-xs editBase'>Edit</button>
	                        <?php
	                            if ($uid == 1) {
	                                ?>
	                                    <button class='btn btn-danger editable-table-button btn-xs deleteBase'>Del</button>
	                                <?php
	                            }
	                        ?>
	                    </span>
	                </td>
	            </tr>
	        <?php
	    }
	    ?>
	    </tbody></table></div>
	    <div class='row'>
	        <div class='col-sm-12'>
	            <div class='col-sm-7 col-md-offset-2 text-center'>
	    <?php
	    echo paginate_function($item_per_page, $page_number, $get_total_rows, $total_pages);
	    ?>
	        </div></div></div>
	    <?php
	}
}


// get all bases
function getAllTanks($page,$user_email,$uid,$ccenter){
	global $dbc;
	$item_per_page = 20;
	if($user_email && $uid){
	    // $uid = $_SESSION['id'];
	    if($page != ""){
	        $page_number = $page;
	        if(!is_numeric($page_number)){
	            $page_number = 1;
	        } //incase of invalid page number
	    }else{
	        $page_number = 1; //if there's no page number, set it to 1
	    }
	    //get total number of records from database for pagination
	    $results = "SELECT id FROM Tank_Header Where id !='' AND costCenter = '$ccenter'";
	    $results = mysqli_query($dbc, $results);
	    $get_total_rows = mysqli_num_rows($results);
	    //break records into pages
	    $total_pages = ceil($get_total_rows/$item_per_page);

	    //get starting position to fetch the records
	    $page_position = (($page_number-1) * $item_per_page);

	    $q = "SELECT c.id AS 'id',
	    		c.FillNumber AS 'FillNumber',
                c.fillDate AS 'fillDate',
                l.description AS 'description',
                c.lastLitres AS 'lastLitres',
                f.fuel_type AS 'fuel_type',
                c.costPerLitre AS 'costPerLitre',
                c.Supplier AS 'Supplier',
                c.costCenter AS 'costCenter',
                td.recievedLitres AS 'newBalance'
                FROM Tank_Header AS c
                JOIN Tank_Details AS td ON c.FillNumber = td.FillNumber AND c.costCenter = td.costCenter
                JOIN FUEL_TYPE AS f ON c.fuelType = f.code AND c.costCenter = f.costCenter
                JOIN LOCATION AS l ON c.location = l.location AND c.costCenter = l.costCenter
                WHERE c.costCenter = '$ccenter' ORDER BY c.id DESC LIMIT $page_position, $item_per_page";
	    $r = mysqli_query($dbc, $q);
	    ?>
	<div class="table-responsive">
	    <table class="table table-hover mails m-0 table table-actions-bar">
	        <thead>
	            <tr>
	                <th>Transaction No.</th>
	                <th>Fill Date</th>
	                <th>Pump</th>
	                <th>Litres</th>
	                <th>Fuel Type</th>
	                <th class="text-right">Action</th>
	            </tr>
	        </thead>
	        <tbody>
	<?php
		if (mysqli_num_rows($r) >= 1) {
		    while( $obj = mysqli_fetch_assoc( $r )) {
		        $id = $obj['id'];
		        $FillNumber = $obj['FillNumber'];
		        $fillDate = $obj['fillDate'];
		        $location = $obj['description'];
		        $lastLitres = $obj['newBalance'];
		        $fuelType = $obj['fuel_type'];
		        $costPerLitre = $obj['costPerLitre'];
		        $Supplier = $obj['Supplier'];
		        $added_by = $obj['added_by'];
		        $date_added = $obj['date_added'];
		        $costCenter = $obj['costCenter'];
		        if (!empty($fillDate) && $fillDate != "0000-00-00") {
			        $y = strtotime($fillDate);
				    $fillDate1 = date('d M Y', $y);
		        }else{
		        	$fillDate1 = "";
		        }
		        ?>
		            <tr class="<?php echo $id; ?>">
		                <td style='width: 15%;'><?php echo $FillNumber; ?></td>
		                <td style='width: 15%;'><?php echo $fillDate1; ?></td>
		                <td style='width: 25%;'><?php echo $location; ?></td>
		                <td style='width: 15%;'><?php echo $lastLitres; ?></td>
		                <td style='width: 10%;'><?php echo $fuelType; ?></td>
		                <td class="text-right" style='width: 11%;'>
		                    <span class='TankID' data='<?php echo $page; ?>' id='<?php echo $id; ?>' type='<?php echo $FillNumber; ?>'>
		                        <button class='btn btn-info editable-table-button btn-xs editTank'>Edit</button>
		                        <?php
		                            if ($uid == 1) {
		                                ?>
		                                    <button class='btn btn-danger editable-table-button btn-xs deleteTank'>Del</button>
		                                <?php
		                            }
		                        ?>
		                    </span>
		                </td>
		            </tr>
		        <?php
		    }
	    }
	    ?>
	    </tbody></table></div>
	    <div class='row'>
	        <div class='col-sm-12'>
	            <div class='col-sm-7 col-md-offset-2 text-center'>
	    <?php
	    echo paginate_function($item_per_page, $page_number, $get_total_rows, $total_pages);
	    ?>
	        </div></div></div>
	    <?php
	}
}



function getAllMechanics($page,$user_email,$uid,$ccenter){
	global $dbc;
	$item_per_page = 20;
	if($user_email && $uid){
	    // $uid = $_SESSION['id'];
	    if($page != ""){
	        $page_number = $page;
	        if(!is_numeric($page_number)){
	            $page_number = 1;
	        } //incase of invalid page number
	    }else{
	        $page_number = 1; //if there's no page number, set it to 1
	    }
	    //get total number of records from database for pagination
	    $results = "SELECT id FROM Mechanics Where id !='' AND costCenter = '$ccenter'";
	    $results = mysqli_query($dbc, $results);
	    $get_total_rows = mysqli_num_rows($results);
	    //break records into pages
	    $total_pages = ceil($get_total_rows/$item_per_page);

	    //get starting position to fetch the records
	    $page_position = (($page_number-1) * $item_per_page);

	    $q = "SELECT * FROM Mechanics WHERE costCenter = '$ccenter' ORDER BY id DESC LIMIT $page_position, $item_per_page";
	    $r = mysqli_query($dbc, $q);
	    ?>
	<div class="table-responsive">
	    <table class="table table-hover mails m-0 table table-actions-bar">
	        <thead>
	            <tr>
	                <th>Name</th>
	                <th>Staff No.</th>
	                <th>Occupation</th>
	                <th>Labour Rate</th>
	                <th class="text-right">Action</th>
	            </tr>
	        </thead>
	        <tbody>
	<?php
	    while( $obj = mysqli_fetch_assoc( $r )) {
	        $id = $obj['id'];
	        $fname = $obj['fname'];
	        $lname = $obj['lname'];
	        $occupation = $obj['occupation'];
	        $laborRate = $obj['laborRate'];
	        $staffNumber = $obj['staffNumber'];
	        ?>
	            <tr class="<?php echo $id; ?>">
	                <td style='width: 30%;'><?php echo $fname." ".$lname; ?></td>
	                <td style='width: 20%;'><?php echo $staffNumber; ?></td>
	                <td style='width: 20%;'><?php echo $occupation; ?></td>
	                <td style='width: 15%;'><?php echo $laborRate; ?></td>
	                <td class="text-right" style='width: 11%;'>
	                    <span class='MechId' data='<?php echo $page; ?>' id='<?php echo $id; ?>' type='<?php echo $staffNumber; ?>'>
	                        <button class='btn btn-info editable-table-button btn-xs editMech'>Edit</button>
	                        <?php
	                            if ($uid == 1) {
	                                ?>
	                                    <button class='btn btn-danger editable-table-button btn-xs deleteMech'>Del</button>
	                                <?php
	                            }
	                        ?>
	                    </span>
	                </td>
	            </tr>
	        <?php
	    }
	    ?>
	    </tbody></table></div>
	    <div class='row'>
	        <div class='col-sm-12'>
	            <div class='col-sm-7 col-md-offset-2 text-center'>
	    <?php
	    echo paginate_function($item_per_page, $page_number, $get_total_rows, $total_pages);
	    ?>
	        </div></div></div>
	    <?php
	}
}



// get all violations
function getAllViolations($page,$user_email,$uid,$user_center){
	global $dbc;
	$item_per_page = 20;
	if($user_email && $uid){
	    // $uid = $_SESSION['id'];
	    if($page != ""){
	        $page_number = $page;
	        if(!is_numeric($page_number)){
	            $page_number = 1;
	        } //incase of invalid page number
	    }else{
	        $page_number = 1; //if there's no page number, set it to 1
	    }
	    //get total number of records from database for pagination
	    $results = "SELECT id FROM INCIDENTS Where id !='' AND costCenter = '$user_center'";
	    $results = mysqli_query($dbc, $results);
	    $get_total_rows = mysqli_num_rows($results);
	    //break records into pages
	    $total_pages = ceil($get_total_rows/$item_per_page);

	    //get starting position to fetch the records
	    $page_position = (($page_number-1) * $item_per_page);

	    $q = "SELECT * FROM INCIDENTS WHERE costCenter = '$user_center' ORDER BY id DESC LIMIT $page_position, $item_per_page";
	    $r = mysqli_query($dbc, $q);
	    ?>
	<div class="table-responsive">
	    <table class="table table-hover mails m-0 table table-actions-bar">
	        <thead>
	            <tr>
	                <th>Incident No.</th>
	                <th>Fleet No</th>
	                <th>Staff Name</th>
	                <th>Date of Incident</th>
	                <th>Offence</th>
	                <th>Police station</th>
	                <th class="text-right">Action</th>
	            </tr>
	        </thead>
	        <tbody>
	<?php
	    while( $obj = mysqli_fetch_assoc( $r )) {
	    	date_default_timezone_set("Africa/Nairobi");
	        $id = $obj['id'];
	        $inci_number = $obj['inci_number'];
	        $fleet_numb = $obj['fleet_numb'];
	        $driver_name = getDriverName($obj['emp_number'], $user_center);
	        $incident_date = $obj['incident_dte'];
	        $inc_offence = $obj['inc_offence'];
	        $police_station = $obj['police_station'];

	        if (isset($incident_date) && !empty($incident_date)) {
	        	$month = strtotime($incident_date);
	            $incident_dte = date('d M Y', $month);
	        }else{
	        	$incident_dte = "";
	        }

	        ?>
	            <tr>
	                <td style='width: 10%;'><?php echo $inci_number; ?></td>
	                <td style='width: 11%;'><?php echo $fleet_numb; ?></td>
	                <td style='width: 20%;'><?php echo $driver_name; ?></td>
	                <td style='width: 13%;'><?php echo $incident_dte; ?></td>
	                <td style='width: 20%;'><?php echo $inc_offence; ?></td>
	                <td style='width: 15%;'><?php echo $police_station; ?></td>
	                <td class="text-right" style='width: 11%;'>
	                    <span class='incidentID' data='<?php echo $inci_number; ?>' id='<?php echo $id; ?>' type='<?php echo $uid; ?>'>
	                        <button class='btn btn-info editable-table-button btn-xs editIncident'>Edit</button>
	                        <?php
	                            if ($uid == 1) {
	                                ?>
	                                    <button class='btn btn-danger editable-table-button btn-xs deleteIncident'>Del</button>
	                                <?php
	                            }
	                        ?>
	                    </span>
	                </td>
	            </tr>
	        <?php
	    }
	    ?>
	    </tbody></table></div>
	    <div class='row'>
	        <div class='col-sm-12'>
	            <div class='col-sm-7 col-md-offset-2 text-center'>
	    <?php
	    echo paginate_function($item_per_page, $page_number, $get_total_rows, $total_pages);
	    ?>
	        </div></div></div>
	    <?php
	}
}

function getBadgeDates($bed, $today){
	global $dbc;
	date_default_timezone_set("Africa/Nairobi");
	$record3 = strtotime($bed);
	$result3 = $record3 - $today;
    $result3 = floor($result3 / (60 * 60 * 24));
    return $result3;
}

function days_of_week($year, $week){
	// $week_start = new DateTime();
	$week_start = date_create();
	$week_start->setISODate($year, $week);
	$monday = $week_start->format('Y-m-d');
	$tuesday = $week_start->modify('+1 day');
	$tuesday = $tuesday->format('Y-m-d');
	$wednesday = $week_start->modify('+1 day');
	$wednesday = $wednesday->format('Y-m-d');
	$thursday = $week_start->modify('+1 day');
	$thursday = $thursday->format('Y-m-d');
	$friday = $week_start->modify('+1 day');
	$friday = $friday->format('Y-m-d');
	$saturday = $week_start->modify('+1 day');
	$saturday = $saturday->format('Y-m-d');
	$sunday = $week_start->modify('+1 day');
	$sunday = $sunday->format('Y-m-d');

	$days = [
		'monday'  => $monday,
		'tuesday' => $tuesday,
		'wednesday' => $wednesday,
		'thursday' => $thursday,
		'friday' => $friday,
		'saturday' => $saturday,
		'sunday' => $sunday
	];
	return $days;
}

function muted_call($satus){
	if($satus == 1){
		echo "display: none;";
	}
}

function data_callbacks_by_date($date, $center){
	global $dbc;
	$q = "SELECT * FROM Rentals WHERE (datefrom LIKE '%$date%') AND status = '1' AND costCenter = '$center' ORDER BY TIME(datefrom) ASC";
	$data = mysqli_query($dbc, $q);
	return $data;
}

function print_leads_by_day($date, $center){
	$dateold = $date;
	$date = strtotime($date);
	$week_num = date('W', $date);
	$day = date('d', $date);
	$dayText = date('l', $date);
	$class = strtolower($dayText);
	?>
		<div class="card-box">
			<div class="bar-widget">
				<div class="table-box">
					<div class="datenday">
						<button style="font-weight: 600;" class="btn bg-<?php echo $class; ?>">
	                        <span class="btn-label"><?php echo $day; ?></i></span><?php echo $dayText; ?>
	                    </button>
					</div>
					<div class="m-t-5 m-b-5">
					<?php $rows = data_callbacks_by_date( $dateold, $center ); ?>
					<?php while( $row = mysqli_fetch_assoc($rows) ):
						if (isset($row['datebooked']) && !empty($row['datebooked'])) {
							$timeago = time_elapsed($row['datebooked']);
						}else{
							$timeago = "";
						}
						$calc = booking_time_difference($row['datefrom'],$row['dateto']);
					?>
					<div class="col-sm-12 m-b-5 p-b-5 card-space" id="<?php echo $row['id']; ?>" data-toggle="modal" data-target="#booking-details">
						<div class="col-md-6 col-sm-6 col-xs-6">
							<h5 class="m-t-0 m-b-0 text-left"><b><?php echo $row['vehicle']; ?></b></h5>
							<h5 class="text-muted text-left m-b-0 m-t-0"><small><?php echo $row['destination']; ?></small></h5>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-6">
							<h5 class="m-t-0 m-b-0 text-right"><b><?php echo $calc; ?></b></h5>
							<h5 class="text-muted text-right m-b-0 m-t-0"><small><?php echo $timeago; ?></small></h5>
						</div>
					</div>
					<?php endwhile;	?>
				</div>
			</div>
		</div>
	</div>
	<?php
}


function getKpiDetails($JobNumber, $CostCenter){
	global $dbc;
	$q = "SELECT sum(w.WorkHours) as 'ttHors'
	FROM MechWokHours AS w
    WHERE w.jobCardNo = '$JobNumber' AND w.costCenter = '$CostCenter'";
	$r = mysqli_query($dbc, $q);
	$data = mysqli_fetch_assoc($r);
	return $data;
}


function getPartCosts($JobNumber, $CostCenter){
	global $dbc;
	$q = "SELECT sum(j.JobTotalCost) as 'PartCost'
	FROM jobDetail AS j
    WHERE j.JobNumber = '$JobNumber' AND j.CostCenter = '$CostCenter'";
	$r = mysqli_query($dbc, $q);
	$data = mysqli_fetch_assoc($r);
	return $data;
}



function getLoborCosts($JobNumber, $CostCenter){
	global $dbc;
	$q = "SELECT SUM(m.laborRate * w.WorkHours) AS 'LabourCost'
	FROM MechWokHours AS w
	JOIN Mechanics AS m ON w.staffNo = m.staffNumber AND w.costCenter = m.costCenter
    WHERE w.jobCardNo = '$JobNumber' AND w.costCenter = '$CostCenter'";
	$r = mysqli_query($dbc, $q);
	$data = mysqli_fetch_assoc($r);
	return $data;
}



function task_by_date($date, $center){
	global $dbc;
	$q = "SELECT * FROM Tasks WHERE (date_scheduled LIKE '%$date%') AND costCenter = '$center' AND status = '1' ORDER BY date_scheduled ASC";
	$data = mysqli_query($dbc, $q);
	return $data;
}

function getAllAssets($center){
	global $dbc;
	$q = "SELECT
	  (SELECT COUNT(*) FROM VEHICLES WHERE id !='' AND costcentre = '$center') as vehicles,
	  (SELECT COUNT(*) FROM MAKES WHERE id !='' AND costCenter = '$center') as makes,
	  (SELECT COUNT(*) FROM BODY_TYPE WHERE id !='') as body_types,
	  (SELECT COUNT(*) FROM VHL_TYPE WHERE id !='') as vehicle_types,
	  (SELECT COUNT(*) FROM MODELS WHERE id !='') as models,
	  (SELECT COUNT(*) FROM DEPARTMENT WHERE id !='' AND costCenter = '$center') as departments,
	  (SELECT COUNT(*) FROM COST_CENTER WHERE id !='') as cost_centers,
	  (SELECT COUNT(*) FROM BASE WHERE id !='' AND costCenter = '$center') as bases";

	$data = mysqli_query($dbc, $q);
	while($row = mysqli_fetch_assoc($data)):
		return $row;
	endwhile;
}

function tasks_by_week($date, $center){
	$dateold = $date;
	$date = strtotime($date);
	$week_num = date('W', $date);
	$day = date('d', $date);
	$dayText = date('l', $date);
	$class = strtolower($dayText);
?>
	<div class="card-box">
		<div class="bar-widget">
			<div class="table-box">
				<!-- date holder -->
				<div class="datenday">
					<button style="font-weight: 600;" class="btn bg-<?php echo $class; ?>">
                        <span class="btn-label"><?php echo $day; ?></i></span><?php echo $dayText; ?>
                    </button>
				</div>
				<!-- bookings -->
				<div class="m-t-5 m-b-5">
				<?php $rows = task_by_date($dateold, $center); ?>
				<?php while($row = mysqli_fetch_assoc($rows)):
					$timeago = time_elapsed($row['date_added']);
					$calc = time_difference($row['date_added'],$row['date_scheduled']);
				?>
				<div class="col-sm-12 m-b-5 p-b-5 card-space" id="<?php echo $row['id']; ?>" data-toggle="modal" data-target="#task-details">
					<div class="col-md-6 col-sm-6 col-xs-6">
						<h5 class="m-t-0 m-b-0 text-left"><b><?php echo $row['vehicle']; ?></b></h5>
						<h5 class="text-muted text-left m-b-0 m-t-0"><small><?php echo $row['task_name']; ?></small></h5>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-6">
						<h5 class="m-t-0 m-b-0 text-right"><b><?php echo $calc; ?></b></h5>
						<h5 class="text-muted text-right m-b-0 m-t-0"><small><?php echo $timeago; ?></small></h5>
					</div>
				</div>
				<?php endwhile;	?>
			</div>
		</div>
	</div>
</div>
<?php
}

function LoadAllUsers($user_email,$uid,$center){
	global $dbc;
	$q = "SELECT id, type_id, avatar, firstname, lastname, username, email, password_recover, status, date_added, center FROM users ORDER BY id DESC";
    $r = mysqli_query($dbc, $q);
    while( $obj = mysqli_fetch_assoc( $r )) {
    	date_default_timezone_set("Africa/Nairobi");
        $id = $obj['id'];
        $type_id = $obj['type_id'];
        $avatar = $obj['avatar'];
        $firstname = $obj['firstname'];
        $lastname = $obj['lastname'];
        $username = $obj['username'];
        $email = $obj['email'];
        $center = $obj['center'];
        $status = $obj['status'];
        $today = date("Y-m-d");
        if ($obj['type_id'] == 1) {
		    $type_data = "Webmaster";
		}elseif ($obj['type_id'] == 2){
		    $type_data = "Administrator";
		}elseif ($obj['type_id'] == 3){
		    $type_data = "Power User";
		}
        ?>
			<div class="col-sm-6 col-lg-4">
        		<div class="card-box">
        			<div class="contact-card">
        				<a class="pull-left" href="/userSelect/view/<?php echo $id; ?>">
                            <img class="img-circle" src="/images/users/<?php echo $avatar; ?>" alt="">
                        </a>
                        <div class="member-info">
                            <h4 class="m-t-0 m-b-5 header-title"><b><?php echo $firstname." ".$lastname; ?></b></h4>
                            <p class="text-muted"><?php echo $type_data; ?></p>
                            <p class="text-dark"><i class="fa fa-fort-awesome"></i> <small><?php echo getCostCenterName($center); ?>.</small></p>
                            <div class="contact-action userProfile" data="<?php echo $id; ?>" type="<?php echo $firstname;?>">
                            	<span>
	                            	<a href="/users/edit/<?php echo $id; ?>" class="btn m-t-20 btn-success btn-sm">Edit Details</a>
                            	</span>
                            	<span id="accountAction_<?php echo $id; ?>">
			                    	<?php if ($status == 1) {
			                    		?>
										<button type="button" class="btn m-t-20 btn-sm btn-danger waves-effect waves-light suspendAcc">
                                           <span class="btn-label"><i class="fa fa-lock"></i></span>Suspend
                                        </button>
			                    		<?php
			                    	}else{
			                    		?>
										<button type="button" class="btn m-t-20 btn-sm btn-success waves-effect waves-light ActivateAcc">
                                           <span class="btn-label"><i class="fa fa-unlock"></i></span>Activate
                                    	</button>
			                    		<?php
			                    	} ?>
			                    </span>
                            </div>
                        </div>
        			</div>
        		</div>
                
            </div>
        <?php
    }
}

// get all tanks
function pumpsGage($user_center){
    global $dbc;
    $q = "SELECT t.id AS 'id',
        t.code AS 'code',
        l.description AS 'description',
        t.costCenter AS 'costCenter',
        t.capacity AS 'capacity',
        f.fuel_type AS 'fuel_type',
        (
			SELECT round(d.costPerLitre, 2)
			FROM Tank_Details d
			WHERE d.Location = t.code ORDER BY d.id DESC LIMIT 1
		) AS 'UnitCost',
		(
			SELECT (d.newBalance - d.Issued)
			FROM Tank_Details d
			WHERE d.Location = t.code ORDER BY d.id DESC LIMIT 1
		) AS 'Remaining',
        t.capacity AS 'capacity'
        FROM Tanks AS t
		JOIN FUEL_TYPE AS f ON t.fuelType = f.code AND t.costCenter = f.costCenter
		JOIN LOCATION AS l ON t.code = l.location AND t.costCenter = l.costCenter
		WHERE t.costCenter = '$user_center'";

	    $r = mysqli_query($dbc, $q);

	    while( $obj = mysqli_fetch_assoc( $r )) {
	        $id = $obj['id'];
	        $code = $obj['code'];
            $description = $obj['description'];
          $costCenter = $obj['costCenter'];
          $capacity = $obj['capacity'];
          $Remaining1 = $obj['Remaining'];
          if ($Remaining1 == "") {
              $Remaining = 0;
          }
          else {
              $Remaining = $obj['Remaining'];
          }
          $fuel_type = sanitize($obj['fuel_type']);

	        ?>
          <script type="text/javascript">
                function gerPumpChart(id, PumpCapacity, Description, fuelType, balance, pumpCode, costCenter) {
                    var Q1 = PumpCapacity * 0.2;
                    var Q2 = PumpCapacity * 0.4;
                    $('#PumpGage_'+id).highcharts({
                        chart: {
                            type: 'gauge',
                            plotBackgroundColor: null,
                            plotBackgroundImage: null,
                            plotBorderWidth: 0,
                            plotShadow: false
                        },
                        title: {
                            text: Description
                        },
                        pane: {
                            startAngle: -150,
                            endAngle: 150,
                            background: [{
                                backgroundColor: {
                                    linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                                    stops: [
                                        [0, '#FFF'],
                                        [1, '#333']
                                    ]
                                },
                                borderWidth: 0,
                                outerRadius: '109%'
                            }, {
                                backgroundColor: {
                                    linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                                    stops: [
                                        [0, '#333'],
                                        [1, '#FFF']
                                    ]
                                },
                                borderWidth: 1,
                                outerRadius: '107%'
                            }, {
                                // default background
                            }, {
                                backgroundColor: '#DDD',
                                borderWidth: 0,
                                outerRadius: '105%',
                                innerRadius: '103%'
                            }]
                        },
                        // the value axis
                        yAxis: {
                            min: 0,
                            max: PumpCapacity,

                            minorTickInterval: 'auto',
                            minorTickWidth: 1,
                            minorTickLength: 10,
                            minorTickPosition: 'inside',
                            minorTickColor: '#666',

                            tickPixelInterval: 30,
                            tickWidth: 2,
                            tickPosition: 'inside',
                            tickLength: 10,
                            tickColor: '#666',
                            labels: {
                                step: 2,
                                rotation: 'auto'
                            },
                            title: {
                                text: fuelType
                            },
                            plotBands: [{
                                from: 0,
                                to: Q1,
                                color: '#DF5353' // red
                            }, {
                                from: Q1,
                                to: Q2,
                                color: '#DDDF0D' // yellow
                            }, {
                                from: Q2,
                                to: PumpCapacity,
                                color: '#55BF3B' // green
                            }]
                        },
                        series: [{
                            name: 'Available',
                            data: [balance],
                            tooltip: {
                                valueSuffix: ' Ltr'
                            }
                        }]

                    },
                        // Add some life
                        function (chart) {
                            if (!chart.renderer.forExport) {
                                setInterval(function () {
                                    $.post("/ajax/getCurrentFuelBal.php",{pumpCode: pumpCode, costCenter: costCenter}).success(function(data){
                                        var point = chart.series[0].points[0], newVal, inc = Math.round((Math.random() - 0.5) * 3000);
                                        newVal = parseInt(data.remaining);
                                        point.update(newVal);
                                    });
                                }, 3000);
                            }
                        });
                }
          		</script>
	            <div id="PumpGage_<?php echo $id; ?>" class="col-xs-12 col-sm-6 col-md-4 col-md-offset-1 m-b-20" style="min-width: 310px; height: 300px;">
                <img onload="gerPumpChart(<?php echo $id; ?>, <?php echo $capacity; ?>, '<?php echo $description; ?>', '<?php echo $fuel_type; ?>', <?php echo $Remaining; ?>, '<?php echo $code; ?>', '<?php echo $costCenter; ?>' )" src="assets/images/trans.png" />
              </div>

	        <?php
	    }
}

function getSidebarUser($userId){
	global $dbc;
	$q = "SELECT id, avatar, firstname, lastname, email, center FROM users WHERE status != 2 AND id != '$userId' ORDER BY id DESC";
    $r = mysqli_query($dbc, $q);
    while( $obj = mysqli_fetch_assoc( $r )) {
		$id = $obj['id'];
		$avatar = $obj['avatar'];
		$fname = $obj['firstname'];
		$lname = $obj['lastname'];
		?>

		<script type="text/javascript">
            function getOnlineStatus(id) {
                setInterval(function () {
                    $.post("/ajax/getUserState.php",{id: id}).success(function(data){
                    	if (data.status == 1) {
                    		if ($('.userState_'+id).hasClass('online')) {}
                    		else{$('.userState_'+id).removeClass('offline').removeClass('away').addClass('online');}
                    	}
                    	else if(data.status == 0){
                    		if ($('.userState_'+id).hasClass('away')) {}
                    		else{$('.userState_'+id).removeClass('offline').removeClass('online').addClass('away');}
                    	}
                    	else if(data.status == "none"){
                    		if ($('.userState_'+id).hasClass('offline')) {}
                    		else{$('.userState_'+id).removeClass('away').removeClass('offline').addClass('online');}
                    	}
                    	else {
                    		if ($('.userState_'+id).hasClass('offline')) {}
                    		else{$('.userState_'+id).removeClass('away').removeClass('online').addClass('online');}
                    	}
                    });
                }, 6000);
            }
  		</script>
			<li class="list-group-item" ng-click="selectUserChats(<?php echo $id;?>, '<?php echo $avatar;?>', '<?php echo $fname;?>', '<?php echo $lname;?>', <?php echo $userId;?>)" style="padding: 10px 15px;">
		        <div>
		        	<div class="avatar">
		                <img onload="getOnlineStatus(<?php echo $id;?>)" src="<?php echo getUrl().$avatar; ?>">
		            </div>
		            <span class="name"><?php echo $fname ." ". $lname; ?></span>
		            <div class="status userState_<?php echo $id;?> offline"></div>
			        <span class="clearfix"></span>
			    </div>
			</li>
		<?php
    }
}

function getAllLogs($page,$search,$center){
	global $dbc;

	if (strlen($search) >= 4) {
	 	$postedVal = $search;
	}else{
		$postedVal = "";
	}

	$item_per_page = 20;
	if($page != ""){
        $page_number = $page;
        if(!is_numeric($page_number)){
            $page_number = 1;
        } //incase of invalid page number
    }else{
        $page_number = 1; //if there's no page number, set it to 1
    }
    //get total number of records from database for pagination
    if (empty($postedVal)) {
	    $q = "SELECT id FROM ISSUANCE WHERE id !='' AND costcenter = '$center'";
    }else{
    	$q = "SELECT i.id AS 'id'
        FROM ISSUANCE AS i
        WHERE (i.vehicle like '%$postedVal%' OR l.description like '%$postedVal%' OR i.reference like '%$postedVal%') AND i.costcenter = '$center'";
    }
    $rst = mysqli_query($dbc, $q);

    $get_total_rows = mysqli_num_rows($rst);
    //break records into pages
    $total_pages = ceil($get_total_rows/$item_per_page);
    $page_position = (($page_number-1) * $item_per_page);

    if (empty($postedVal)) {
    	$query = "SELECT i.id AS 'id',
		i.vehicle AS 'vehicle',
		i.dates AS 'dates',
		i.odometer AS 'odometer',
		i.quantity AS 'quantity',
		i.totalcost AS 'totalcost',
		i.reference AS 'reference',
		i.fueltime AS 'fueltime',
		f.fuel_type AS 'FuelDescription',
        i.location AS 'description',
        l.location AS 'location',
        l.description AS 'LocationDescription'
        FROM ISSUANCE AS i
        LEFT JOIN LOCATION AS l ON i.location = l.location
        LEFT JOIN FUEL_TYPE AS f ON i.fuel_type = f.code AND i.costcenter = f.costCenter
        WHERE i.costcenter = '$center' ORDER BY i.vehicle ASC, i.dates DESC LIMIT $page_position, $item_per_page";
    }
    else{
    	$query = "SELECT i.vehicle AS 'vehicle',
        i.dates AS 'dates',
        i.id AS 'id',
        i.fueltime AS 'fueltime',
        i.odometer AS 'odometer',
        i.location AS 'location',
        i.fuel_type AS 'fuel_type',
        i.quantity AS 'quantity',
        i.totalcost AS 'totalcost',
        f.fuel_type AS 'FuelDescription',
        l.location AS 'location',
        l.description AS 'LocationDescription'
        FROM ISSUANCE AS i
        LEFT JOIN LOCATION AS l ON i.location = l.location
        LEFT JOIN FUEL_TYPE AS f ON i.fuel_type = f.code AND i.costcenter = f.costCenter
        WHERE (i.vehicle like '%$postedVal%' OR l.description like '%$postedVal%' OR i.reference like '%$postedVal%') AND i.costcenter = '$center'";
    }

    $r = mysqli_query($dbc, $query);

	if(mysqli_num_rows($r) > 0){
		$result = array();
		while( $row = mysqli_fetch_assoc( $r )) {
			$result[] = $row;
		}
		$check = paginate_function($item_per_page, $page_number, $get_total_rows, $total_pages);
		// $PaginatedData = array('fuelData' => $result, "Pages" => $check);
		$results['fuelData'] = $result;
		$results['Pages'] = $check;
		header('Content-type: application/json');
		echo json_encode($results, JSON_PRETTY_PRINT);
	}
}
?>
