<?php
/**
* Overtime Email
*/
namespace SendGrid;
require_once('../vendor/autoload.php');

function overtimeEmail($requestID)
{
  // $date_culc, $fullname, $supervisor, $timeshift, $department, $reason
   global $user_id,$fullname,$supervisor,$timeshift,$hourfrom,$hourto,$department,$reason;

   $acceptLink = "https://callbacks.sickholiday.com/send_overtime.php?id=".$requestID."&action=Approved";
   $denyLink = "https://callbacks.sickholiday.com/send_overtime.php?id=".$requestID."&action=Rejected";

   $hourto1 = strtotime($hourto);
   $hourfrom1 = strtotime($hourfrom);

   $all = round(($hourto1 - $hourfrom1) / 60);
   $d = floor ($all / 1440);
   $h = floor (($all - $d * 1440) / 60);
   $m = $all - ($d * 1440) - ($h * 60);
   $date_culc = $h.' : ' .$m.' ';

   $mail = new Mail();
   $from = new Email("Sickholiday Team", "hello@sickholiday.com");
   $mail->setFrom($from);
   $mail->setSubject("Overtime request from ". $fullname);
   $personalization = new Personalization();

   $email = new Email("John", "john@sickholiday.com");
   $personalization->addTo($email);
   $email = new Email("Simon", "simon@sickholiday.com");
   $personalization->addTo($email);

   $personalization->addSubstitution("%name%", $fullname);
   $personalization->addSubstitution("%supervisor%", $supervisor);
   $personalization->addSubstitution("%timeshift%", $timeshift);
   $personalization->addSubstitution("%hours%", $date_culc);
   $personalization->addSubstitution("%department%", $department);
   $personalization->addSubstitution("%reason%", $reason);
   $personalization->addSubstitution("%accept%", $acceptLink);
   $personalization->addSubstitution("%reject%", $denyLink);

   $mail->setTemplateId("fa3320ee-ffb3-4461-aad0-101a5ca3bda5");
   $mail->addPersonalization($personalization);
   $reply_to = new ReplyTo("no-reply@sickholiday.com");
   $mail->setReplyTo($reply_to);
   $content = new Content("text/plain", "Hello from the Sickholiday Team.");
   $mail->addContent($content);
   $content = new Content("text/html", "<html><body>Hello from the Sickholiday Team.</body></html>");
   $mail->addContent($content);

   return $mail;
}

function sendOvertimeEmail($requestID)
{
   $sg = new \SendGrid("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");
   $request_body = overtimeEmail($requestID);
   $response = $sg->client->mail()->send()->post($request_body);

   if($response->statusCode() == 202){
     return true;
   }else{
     return false;
   }
}
